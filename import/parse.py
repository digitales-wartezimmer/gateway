# Requirements: Python 3.x
# Dependencies: pip install halo
# Usage: Navigate to the directory that contains TransmittingSites.xml and then execute:
# python parse.py
from xml.dom import minidom
import codecs
from halo import Halo
import json

fieldnames = ['Code', 'Name', 'Department', 'Street', 'Postalcode', 'Place', 'Phone', 'Fax', 'Email']

try:
    spinner_parse = Halo(text='Parsing xml File', spinner='dots')
    spinner_parse.start()
    data = minidom.parse('TransmittingSiteSearchText.xml')
    items = data.getElementsByTagName('TransmittingSite')
    spinner_parse.succeed("Finished Parsing")
except Exception as e:
    spinner_parse.fail("Failed Parsing")
    print(e)

def get_site_data(site):
    result = {}
    for field in fieldnames:
        result[field] = site.attributes[field].value
    return result

try:
    spinner_write = Halo(text='Converting Sites: 0/{}'.format(len(items)), spinner='dots')
    spinner_write.start()
    sites = data.getElementsByTagName('TransmittingSite')
    output_file = codecs.open('sites.json', 'wb', 'utf-8')

    for i, site in enumerate(sites, 1):
        spinner_write.text = 'Converting Sites: {}/{}'.format(i, len(items))
        site_data = get_site_data(site)
        schema_data = {
            "name": site_data.get("Name"),
            "department": site_data.get("Department"),
            "code": site_data.get("Code"),
            "address": {
                "street": site_data.get("Street"),
                "postCode": int(site_data.get("Postalcode")) if site_data.get("Postalcode") != "" else None,
                "place": site_data.get("Place")
            },
            "contact": {
                "phone": site_data.get("Phone"),
                "fax": site_data.get("Fax"),
                "mail": site_data.get("Email")
            },
            "jurisdiction": [],
            "transmissionType": "MAIL",
            "pdfServiceEnabled": False
        }
        searches = site.getElementsByTagName('SearchText')
        for search in searches:
            current_row = site_data
            if search.attributes.length == 0 or not search.attributes['Value'] or not search.attributes['Value'].value:
                continue
            current_search = search.attributes['Value'].value
            if not current_search.isdigit() or len(current_search) == 0:
                continue
            schema_data["jurisdiction"].append(int(current_search))
        output_file.write(json.dumps(schema_data, separators=(',', ':'), ensure_ascii=False))
        output_file.write('\n')

    spinner_write.succeed('Converted {} Sites'.format(len(items)))
    spinner_done = Halo(text='Saved to file sites.json'.format(len(items)), spinner='dots')
    spinner_done.succeed()
except Exception as e:
    spinner_write.fail("Failed Converting")
    output_file.close()
    print(e)
