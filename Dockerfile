FROM node:12.16.1-alpine3.11

WORKDIR /home/node/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "dist/main"]
