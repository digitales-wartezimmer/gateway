import {
  Body,
  Controller,
  Post,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { SentryInterceptor } from '../interceptors/sentry.interceptor';
import {
  ApiHeader,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import * as Sentry from '@sentry/minimal';
import { Connector } from './connector.schema';
import { ConnectorService } from './connector.service';
import { Roles } from '../auth/roles.decorator';
import { Role } from '../auth/roles.enum';

@UseInterceptors(SentryInterceptor)
@Controller('connectors')
@ApiTags('Connectors')
export class ConnectorController {
  constructor(private connectorService: ConnectorService) {}

  @Post()
  @ApiOperation({ summary: 'Get All existing Workflow configurations' })
  @ApiOkResponse({
    description: 'Response contains newly created Connector',
    type: Connector,
  })
  @ApiHeader({
    name: 'X-Custom-Lang',
    description:
      'Set Language of Response or errors by two letter language code',
    example: 'de',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Pass API Token for Authorization with prefix Api-Key',
    example: 'Api-Key 123abc456def',
  })
  @Roles(Role.Admin, Role.HealthOffice)
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  async addConnector(@Body() connector: Connector): Promise<Connector> {
    Sentry.captureMessage('Connector added');
    return this.connectorService.addConnector(connector);
  }
}
