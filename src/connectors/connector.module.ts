import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ConnectorService } from './connector.service';
import { ConnectorController } from './connector.controller';
import ConnectorProvider from './connector.provider';
@Module({
  controllers: [ConnectorController],
  providers: [ConnectorService],
  exports: [ConnectorService, MongooseModule],
  imports: [ConfigModule, MongooseModule.forFeatureAsync([ConnectorProvider])],
})
export class ConnectorModule {}
