import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SERVICE_KEYS } from './connector.constants';
import { Connector, ConnectorDocument } from './connector.schema';

@Injectable()
export class ConnectorService {
  private readonly logger = new Logger(ConnectorService.name);

  public constructor(
    @InjectModel(Connector.name)
    private connectorModel: Model<ConnectorDocument>,
  ) {}

  public async addConnector(connector: Connector): Promise<Connector> {
    const createdConnector = new this.connectorModel(connector);
    return createdConnector.save();
  }

  public async getConnector(
    code: string,
    service: SERVICE_KEYS,
  ): Promise<Connector> {
    const connector = await this.connectorModel.findOne({ code, service });
    if (!connector) {
      throw new NotFoundException(
        `Could not find Connector with code "${code}" for service "${service}"`,
      );
    }
    return connector;
  }
}
