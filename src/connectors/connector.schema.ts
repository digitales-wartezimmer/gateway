import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { SERVICE_KEYS } from './connector.constants';
import { IsEnum, IsString, IsUrl } from 'class-validator';

export type ConnectorDocument = Connector & Document;

@Schema()
export class Connector {
  @ApiProperty({
    description: 'Code of the Health office',
    example: '1.08.4.25.',
  })
  @Prop({ required: true, index: true })
  @IsString()
  code: string;

  @ApiProperty({
    description:
      'Descriptive Name for the Health office for easier recognition',
    example: 'Gesundheitsamt Alb Donau Kreis',
  })
  @Prop()
  @IsString()
  name: string;

  @ApiProperty({
    description:
      'The service name of the connector. A health office can be conencted via multiple services',
    example: 'sormas',
    enum: SERVICE_KEYS,
  })
  @Prop({ type: SERVICE_KEYS, required: true })
  @IsString()
  @IsEnum(SERVICE_KEYS)
  service: SERVICE_KEYS;

  @ApiProperty({
    description: 'The service url',
  })
  @Prop({ required: true })
  @IsUrl()
  url: string;

  @ApiProperty({
    description: 'The username used for authentication',
  })
  @Prop({ required: true })
  @IsString()
  username: string;

  @ApiProperty({
    description: 'The password used for authentication',
  })
  @Prop({ required: true })
  @IsString()
  password: string;

  @ApiProperty({
    description: 'The Uuid of the user used for authentication',
  })
  @Prop({ required: true })
  @IsString()
  userUuid: string;
}

export const ConnectorSchema = SchemaFactory.createForClass(Connector);
