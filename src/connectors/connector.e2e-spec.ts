import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { I18nModule } from '../i18n.module';
import { getModelToken } from '@nestjs/mongoose';
import * as request from 'supertest';
import { ConnectorModule } from '../connectors/connector.module';
import { Connector } from './connector.schema';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.schema';
import { Role } from '../auth/roles.enum';
import { AuthModule } from '../auth/auth.module';

const save = jest.fn();
class MockedConnector {
  data: any;

  constructor(data: any) {
    this.data = data;
  }

  findOne() {
    return this;
  }
  save() {
    save(this.data);
    return this.data;
  }
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('Connector', () => {
  let app: INestApplication;
  const userService = new MockedUserService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [I18nModule, ConnectorModule, AuthModule],
    })
      .overrideProvider(getModelToken(Connector.name))
      .useValue(MockedConnector)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  beforeEach(() => {
    jest
      .spyOn(userService, 'getData')
      .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
    jest.clearAllMocks();
  });

  describe('POST /connectors', () => {
    it('throws an unauthorized error if not authenticated', async () => {
      const data = {
        code: 'demo',
        name: 'demo',
        service: 'sormas',
        url: 'https://sormas.de/sormas-rest',
        username: 'test',
        password: 'test',
        userUuid: '123-abc',
      };
      await request(app.getHttpServer())
        .post('/connectors')
        .send(data)
        .expect(401);
    });
    it('only allows admins and health offices to access this resource', async () => {
      const data = {
        code: 'demo',
        name: 'demo',
        service: 'sormas',
        url: 'https://sormas.de/sormas-rest',
        username: 'test',
        password: 'test',
        userUuid: '123-abc',
      };
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
      await request(app.getHttpServer())
        .post('/connectors')
        .send(data)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'HealthOffice', role: Role.HealthOffice, token: '1' });
      await request(app.getHttpServer())
        .post('/connectors')
        .send(data)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'User', role: Role.User, token: '1' });
      await request(app.getHttpServer())
        .post('/connectors')
        .send(data)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Monitor', role: Role.Monitor, token: '1' });
      await request(app.getHttpServer())
        .post('/connectors')
        .send(data)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .post('/connectors')
        .send(data)
        .set('Authorization', 'Api-Key 1')
        .expect(401);
    });
    it(`saves new connector config`, async () => {
      const data = {
        code: 'demo',
        name: 'demo',
        service: 'sormas',
        url: 'https://sormas.de/sormas-rest',
        username: 'test',
        password: 'test',
        userUuid: '123-abc',
      };
      await request(app.getHttpServer())
        .post('/connectors')
        .set('Authorization', 'Api-Key 1')
        .send(data)
        .expect(201)
        .expect(data);

      expect(save).toHaveBeenCalledWith(data);
    });

    it(`validates data`, async () => {
      const data = {};
      await request(app.getHttpServer())
        .post('/connectors')
        .set('Authorization', 'Api-Key 1')
        .send(data)
        .expect(400);
      expect(save).not.toHaveBeenCalled();
    });
  });
});
