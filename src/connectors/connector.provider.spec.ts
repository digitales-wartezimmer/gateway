import { ConfigService } from '@nestjs/config';
import ConnectorProvider from './connector.provider';
import { Connector, ConnectorSchema } from './connector.schema';

jest.mock('./connector.schema', () => {
  return {
    Connector: {
      name: 'AHA',
    },
    ConnectorSchema: {
      schema: true,
      plugin: jest.fn(),
      index: jest.fn(),
    },
  };
});

describe('Connector', () => {
  const configService = ({
    get: jest.fn().mockReturnValue('abc'),
  } as unknown) as ConfigService;
  describe('Provider', () => {
    it('provides the connector model', () => {
      expect(ConnectorProvider.name).toEqual(Connector.name);
      expect(ConnectorProvider.useFactory(configService)).toEqual(
        ConnectorSchema,
      );
    });
    it('encrypts database fields', () => {
      jest.spyOn(ConnectorSchema, 'plugin');
      ConnectorProvider.useFactory(configService);
      expect(ConnectorSchema.plugin).toHaveBeenCalledWith(expect.anything(), {
        secret: 'abc',
        excludeFromEncryption: ['code', 'name', 'service'],
      });
    });
    it('creates a unique constraint and index for code and service', () => {
      jest.spyOn(ConnectorSchema, 'index');
      ConnectorProvider.useFactory(configService);
      expect(ConnectorSchema.index).toHaveBeenCalledWith(
        { code: 1, service: 1 },
        { unique: true },
      );
    });
  });
});
