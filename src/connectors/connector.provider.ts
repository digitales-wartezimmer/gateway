import { ConfigModule, ConfigService } from '@nestjs/config';
import { Connector, ConnectorSchema } from './connector.schema';
import * as encrypt from 'mongoose-encryption';

export default {
  name: Connector.name,
  imports: [ConfigModule],
  useFactory: (configService: ConfigService) => {
    const schema = ConnectorSchema;
    schema.plugin(encrypt, {
      secret: configService.get('DB_SECRET'),
      excludeFromEncryption: ['code', 'name', 'service'],
    });
    schema.index({ code: 1, service: 1 }, { unique: true });
    return schema;
  },
  inject: [ConfigService],
};
