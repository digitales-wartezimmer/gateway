import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { I18nModule } from '../i18n.module';
import { getModelToken } from '@nestjs/mongoose';
import * as request from 'supertest';
import { ConnectorModule } from '../connectors/connector.module';
import { Connector } from './connector.schema';
import { ConnectorService } from './connector.service';
import { SERVICE_KEYS } from './connector.constants';

const save = jest.fn();
const retrieve = jest.fn();
class MockedConnector {
  data: any;

  constructor(data: any) {
    this.data = data;
  }

  static findOne(code, service): Promise<Connector> {
    return retrieve(code, service);
  }
  save() {
    save(this.data);
    return this.data;
  }
}

describe('ConnectorService', () => {
  let connectorService: ConnectorService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [I18nModule, ConnectorModule],
    })
      .overrideProvider(getModelToken(Connector.name))
      .useValue(MockedConnector)
      .compile();

    connectorService = await moduleRef.resolve<ConnectorService>(
      ConnectorService,
    );
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('getConnector', () => {
    it(`returns connector config`, async () => {
      const data = {
        code: 'demo',
        name: 'demo',
        service: 'sormas',
        url: 'https://sormas.de/sormas-rest',
        username: 'test',
        password: 'test',
        userUuid: '123-abc',
      };
      retrieve.mockResolvedValue(data);
      const result = await connectorService.getConnector(
        'demo',
        SERVICE_KEYS.SORMAS,
      );
      expect(result).toEqual(data);
    });

    it(`throws not found error if none found`, async () => {
      retrieve.mockReturnValue(undefined);
      await expect(
        connectorService.getConnector('demo', SERVICE_KEYS.SORMAS),
      ).rejects.toThrowError(
        'Could not find Connector with code "demo" for service "sormas"',
      );
    });
  });
});
