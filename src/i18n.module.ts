import { HeaderResolver, I18nJsonParser, I18nModule as LocalizationModule } from 'nestjs-i18n';
import * as path from "path";
import { Module } from '@nestjs/common';

@Module({
  imports: [
    LocalizationModule.forRoot({
      fallbackLanguage: 'de',
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'),
      },
      resolvers: [
        new HeaderResolver(['x-custom-lang']),
      ],
    }),
  ],
  controllers: [],
  exports: [],
  providers: [],
})
export class I18nModule {}
