import { Body, CacheInterceptor, Controller, Get, Header, Post, Query, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { HealthOfficeService } from "./healthOffice.service";
import { HealthOffice } from "./interfaces/healthOffice";
import { PostCode } from './interfaces/postCode';
import { ApiCreatedResponse, ApiHeader, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '../interceptors/sentry.interceptor';
import { TTL } from '../util/cache/cache.decorator';
import { Roles } from '../auth/roles.decorator';
import { Role } from '../auth/roles.enum';

@UseInterceptors(SentryInterceptor)
@UseInterceptors(CacheInterceptor)
@ApiTags('HealthOffice')
@Controller('healthOffice')
export class HealthOfficeController {
    constructor(private healthOfficeService: HealthOfficeService) {}

    @Post('')
    @ApiOperation({ summary: 'Get Health Office Information based on postal code', deprecated: true })
    @ApiCreatedResponse({ description: 'Array of Health Offices for the given postal code', type: [HealthOffice] })
    @ApiHeader({
        name: 'X-Custom-Lang',
        description: 'Set Language of Response or errors by two letter language code',
        example: 'de',
    })
    @ApiHeader({
        name: 'Authorization',
        description: 'Pass API Token for Authorization with prefix Api-Key',
        example: 'Api-Key 123abc456def',
      })
    @Roles(Role.User, Role.Admin)
    @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
    async postHealthOfficeForPostcode(@Body() body: PostCode): Promise<Array<HealthOffice>> {
        return this.healthOfficeService.findByPostcode(body.postCode);
    }

    @Get('')
    @ApiOperation({ summary: 'Get Health Office Information based on postal code' })
    @ApiOkResponse({ description: 'Array of Health Offices for the given postal code', type: [HealthOffice] })
    @ApiHeader({
        name: 'X-Custom-Lang',
        description: 'Set Language of Response or errors by two letter language code',
        example: 'de',
    })
    @ApiHeader({
        name: 'Authorization',
        description: 'Pass API Token for Authorization with prefix Api-Key',
        example: 'Api-Key 123abc456def',
      })
    @Header('Cache-Control', `public, max-age=${TTL.DAY}`)
    @Roles(Role.User, Role.Admin)
    @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
    async getHealthOfficeForPostcode(@Query() postCode: PostCode): Promise<Array<HealthOffice>> {
        return this.healthOfficeService.findByPostcode(postCode.postCode);
    }
}
