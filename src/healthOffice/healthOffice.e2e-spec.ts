import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {
  CacheInterceptor,
  CallHandler,
  HttpService,
  INestApplication,
  NestInterceptor,
} from '@nestjs/common';
import * as rxjs from 'rxjs';

import { HealthOfficeModule } from './healthOffice.module';
import { RKIDTO } from './interfaces/healthOffice';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { I18nModule } from '../i18n.module';
import { Observable } from 'rxjs';
import { User } from '../users/users.schema';
import { UsersService } from '../users/users.service';
import { Role } from '../auth/roles.enum';
import { AuthModule } from '../auth/auth.module';
import { getModelToken } from '@nestjs/mongoose';

jest.mock('../util/cache/cache.decorator', () => ({
  Cache: () => {
    return (target, propertyKey, descriptor) => {
      return descriptor;
    };
  },
  TTL: {
    DAY: 1,
  },
}));

const exampleHealthOfficeData: RKIDTO = {
  code: '1.08.4.25.',
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  street: 'Schillerstr. 30',
  postalCode: '89077',
  city: 'Ulm',
  federalState: 'Baden-Württemberg',
  phone: '0731 185-1730',
  fax: '0731 185-1738',
  email: 'Gesundheitsamt@alb-donau-kreis.de',
  covid19Hotline: null,
  covid19Email: null,
};

class MockedHttpService {
  get(): rxjs.Observable<{ data: { results: Array<RKIDTO> } }> {
    return rxjs.of({ data: { results: this.getRKIReturnedData() } });
  }

  getRKIReturnedData(): Array<RKIDTO> {
    return [];
  }
}

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}

export class CustomLogger extends TestingLogger {
  error(): any {}
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('Health Office', () => {
  let app: INestApplication;
  const httpService = new MockedHttpService();
  const userService = new MockedUserService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [HealthOfficeModule, I18nModule, AuthModule],
    })
      .overrideProvider(HttpService)
      .useValue(httpService)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();
    app.useLogger(new CustomLogger());
    await app.init();
  });

  beforeEach(() => {
    jest.spyOn(userService, 'getData').mockReturnValue({
      _id: '1',
      id: '1',
      name: 'Admin',
      role: Role.Admin,
      token: '1',
    });
  });

  describe('GET /healthOffice', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '89077' })
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      jest
        .spyOn(httpService, 'getRKIReturnedData')
        .mockReturnValue([exampleHealthOfficeData]);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'Admin',
        role: Role.Admin,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(200);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'User',
        role: Role.User,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(200);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'Monitor',
        role: Role.Monitor,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'HealthOffice',
        role: Role.HealthOffice,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(401);
    });
    it(`get single health office`, () => {
      jest
        .spyOn(httpService, 'getRKIReturnedData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([
          {
            name: 'Landratsamt Alb-Donau-Kreis',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 185-1730',
              fax: '0731 185-1738',
              mail: 'Gesundheitsamt@alb-donau-kreis.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`gets a demo health office`, () => {
      jest
        .spyOn(httpService, 'getRKIReturnedData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '00000' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([
          {
            name: 'Demo Gesundheitsamt',
            department: 'Fachdienst Gesundheit',
            code: 'demo',
            address: {
              street: 'Teststr. 1',
              postCode: '12345',
              place: 'Deutschland',
            },
            contact: {
              phone: '+49151123456789',
              fax: '49151123456789',
              mail: 'info@digitales-wartezimmer.org',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`uses corona email and phone if available`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([
        {
          ...exampleHealthOfficeData,
          covid19Email: 'corona@alb-donau-kres.de',
          covid19Hotline: '0731 123456',
        },
      ]);
      return request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([
          {
            name: 'Landratsamt Alb-Donau-Kreis',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 123456',
              fax: '0731 185-1738',
              mail: 'corona@alb-donau-kres.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`gets multiple health offices`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([
        exampleHealthOfficeData,
        {
          ...exampleHealthOfficeData,
          name: 'Landratsamt Freising',
        },
      ]);
      return request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([
          {
            name: 'Landratsamt Alb-Donau-Kreis',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 185-1730',
              fax: '0731 185-1738',
              mail: 'Gesundheitsamt@alb-donau-kreis.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
          {
            name: 'Landratsamt Freising',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 185-1730',
              fax: '0731 185-1738',
              mail: 'Gesundheitsamt@alb-donau-kreis.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`gets no matching`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([]);
      return request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '11111' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([]);
    });

    it(`handles invalid zip code`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([]);
      return request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: 'wrong' })
        .expect(400)
        .expect({
          statusCode: 400,
          message: ['postCode must be a postal code'],
          error: 'Bad Request',
        });
    });

    it(`handles error when retrieving data from RKI`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockImplementation(() => {
        throw new Error('Test-Error: RKI-Service unavailable');
      });
      return request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(503)
        .expect({
          statusCode: 503,
          message:
            'Das RKI - PLZ Tool ist nicht erreichbar oder hat einen Fehler produziert',
          error: 'Service Unavailable',
        });
    });

    it(`translates the error when retrieving data from RKI`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockImplementation(() => {
        throw new Error('Test-Error: RKI-Service unavailable');
      });
      return request(app.getHttpServer())
        .get('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .set('x-custom-lang', 'en')
        .query({ postCode: '89077' })
        .expect(503)
        .expect({
          statusCode: 503,
          message: 'RKI - PLZ Tool Service is unavailable or errored',
          error: 'Service Unavailable',
        });
    });
  });

  describe('POST /healthOffice', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .post('/healthOffice')
        .send({ postCode: '89077' })
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      jest
        .spyOn(httpService, 'getRKIReturnedData')
        .mockReturnValue([exampleHealthOfficeData]);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'Admin',
        role: Role.Admin,
        token: '1',
      });
      await request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(201);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'User',
        role: Role.User,
        token: '1',
      });
      await request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(201);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'Monitor',
        role: Role.Monitor,
        token: '1',
      });
      await request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'HealthOffice',
        role: Role.HealthOffice,
        token: '1',
      });
      await request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(403);
    });
    it(`get single health office`, () => {
      jest
        .spyOn(httpService, 'getRKIReturnedData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(201)
        .expect([
          {
            name: 'Landratsamt Alb-Donau-Kreis',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 185-1730',
              fax: '0731 185-1738',
              mail: 'Gesundheitsamt@alb-donau-kreis.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`gets a demo health office`, () => {
      jest
        .spyOn(httpService, 'getRKIReturnedData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/healthOffice')
        .send({ postCode: '00000' })
        .set('Authorization', 'Api-Key 1')
        .expect(201)
        .expect([
          {
            name: 'Demo Gesundheitsamt',
            department: 'Fachdienst Gesundheit',
            code: 'demo',
            address: {
              street: 'Teststr. 1',
              postCode: '12345',
              place: 'Deutschland',
            },
            contact: {
              phone: '+49151123456789',
              fax: '49151123456789',
              mail: 'info@digitales-wartezimmer.org',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`uses corona email and phone if available`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([
        {
          ...exampleHealthOfficeData,
          covid19Email: 'corona@alb-donau-kres.de',
          covid19Hotline: '0731 123456',
        },
      ]);
      return request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(201)
        .expect([
          {
            name: 'Landratsamt Alb-Donau-Kreis',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 123456',
              fax: '0731 185-1738',
              mail: 'corona@alb-donau-kres.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`gets multiple health offices`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([
        exampleHealthOfficeData,
        {
          ...exampleHealthOfficeData,
          name: 'Landratsamt Freising',
        },
      ]);
      return request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(201)
        .expect([
          {
            name: 'Landratsamt Alb-Donau-Kreis',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 185-1730',
              fax: '0731 185-1738',
              mail: 'Gesundheitsamt@alb-donau-kreis.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
          {
            name: 'Landratsamt Freising',
            department: 'Fachdienst Gesundheit',
            code: '1.08.4.25.',
            address: {
              street: 'Schillerstr. 30',
              postCode: '89077',
              place: 'Ulm',
            },
            contact: {
              phone: '0731 185-1730',
              fax: '0731 185-1738',
              mail: 'Gesundheitsamt@alb-donau-kreis.de',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
    });

    it(`gets no matching`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([]);
      return request(app.getHttpServer())
        .post('/healthOffice')
        .send({ postCode: '11111' })
        .set('Authorization', 'Api-Key 1')
        .expect(201)
        .expect([]);
    });

    it(`handles invalid zip code`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockReturnValue([]);
      return request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: 'wrong' })
        .expect(400)
        .expect({
          statusCode: 400,
          message: ['postCode must be a postal code'],
          error: 'Bad Request',
        });
    });

    it(`handles error when retrieving data from RKI`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockImplementation(() => {
        throw new Error('Test-Error: RKI-Service unavailable');
      });
      return request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .send({ postCode: '89077' })
        .expect(503)
        .expect({
          statusCode: 503,
          message:
            'Das RKI - PLZ Tool ist nicht erreichbar oder hat einen Fehler produziert',
          error: 'Service Unavailable',
        });
    });

    it(`translates the error when retrieving data from RKI`, () => {
      jest.spyOn(httpService, 'getRKIReturnedData').mockImplementation(() => {
        throw new Error('Test-Error: RKI-Service unavailable');
      });
      return request(app.getHttpServer())
        .post('/healthOffice')
        .set('Authorization', 'Api-Key 1')
        .set('x-custom-lang', 'en')
        .send({ postCode: '89077' })
        .expect(503)
        .expect({
          statusCode: 503,
          message: 'RKI - PLZ Tool Service is unavailable or errored',
          error: 'Service Unavailable',
        });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
