import { IsString, IsPostalCode } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PostCode {
  @IsString()
  @IsPostalCode('DE')
  @ApiProperty({
    description: 'German zip code',
  })
  postCode: string;

  constructor (postCode: string) {
    this.postCode = postCode;
  }
}
