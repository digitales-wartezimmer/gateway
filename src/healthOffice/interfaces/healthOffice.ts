import { ApiProperty } from '@nestjs/swagger';

class HealthOfficeAddress {
    @ApiProperty({
        description: 'Street and number',
    })
    street: string
    @ApiProperty({
        description: 'Postal Code',
    })
    postCode: string
    @ApiProperty({
        description: 'City or municipality',
    })
    place: string
}
class HealthOfficeContact {
    @ApiProperty({
        description: 'Phone or hotline number',
    })
    phone: string
    @ApiProperty({
        description: 'Fax number',
    })
    fax: string
    @ApiProperty({
        description: 'Contact email address',
    })
    mail: string
}

export class HealthOffice {
    @ApiProperty({
        description: 'Name of the Health Office',
    })
    name: string
    @ApiProperty({
        description: 'Department the health office belongs to',
    })
    department: string
    @ApiProperty({
        description: 'Internal code of the health office',
    })
    code: string
    @ApiProperty({
        description: 'Address of the health office',
        type: HealthOfficeAddress,
    })
    address: HealthOfficeAddress
    @ApiProperty({
        description: 'Contact information of the health office',
        type: HealthOfficeContact,
    })
    contact: HealthOfficeContact
    @ApiProperty({
        description: 'Array of zip codes the health office is responsible for',
        type: 'array',
        items: {
            type: 'number',
        },
        deprecated: true,
    })
    jurisdiction: Array<number>
    @ApiProperty({
        description: 'Information how data is transmitted to the health office',
        deprecated: true,
    })
    transmissionType: string
    @ApiProperty({
        description: 'States whether pdf service is supported for this health office',
        deprecated: true,
    })
    pdfServiceEnabled: boolean
}

export interface RKIDTO {
    "code": string,
    "name": string,
    "department": string,
    "street": string,
    "postalCode": string,
    "city": string,
    "federalState": string,
    "phone": string,
    "fax": string,
    "email": string,
    "covid19Hotline"?: string,
    "covid19Email"?: string
}
