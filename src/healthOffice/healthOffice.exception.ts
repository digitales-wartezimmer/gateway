import { NotFoundException } from '@nestjs/common';

export class HealthOfficeNotFoundException extends NotFoundException {
  constructor () {
    super('Health Office Not Found for given post code');
  }
}
