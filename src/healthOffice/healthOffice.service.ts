import {
  HttpService,
  Injectable,
  Logger,
  ServiceUnavailableException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as assert from 'assert';
import { HealthOffice, RKIDTO } from './interfaces/healthOffice';
import { I18nRequestScopeService } from 'nestjs-i18n';
import { Cache, TTL } from '../util/cache/cache.decorator';
import * as Sentry from '@sentry/minimal';

const demoHealthOffice: HealthOffice = {
  name: 'Demo Gesundheitsamt',
  department: 'Fachdienst Gesundheit',
  code: 'demo',
  address: { street: 'Teststr. 1', postCode: '12345', place: 'Deutschland' },
  contact: {
    phone: '+49151123456789',
    fax: '49151123456789',
    mail: 'info@digitales-wartezimmer.org',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

@Injectable()
export class HealthOfficeService {
  private readonly rkiToolUrl: string;
  private readonly logger = new Logger(HealthOfficeService.name);

  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
    private readonly i18n: I18nRequestScopeService,
  ) {
    this.rkiToolUrl = configService.get('RKI_PLZ_TOOL_URL');
  }

  @Cache({ key: 'healthOffice', ttl: TTL.DAY })
  async findByPostcode(postCode: string): Promise<Array<HealthOffice>> {
    if (
      postCode === '00000' &&
      this.configService.get('ENV_NAME') !== 'production'
    ) {
      return [demoHealthOffice];
    }
    let results: Array<RKIDTO> = [];
    try {
      const response = await this.httpService
        .get(this.rkiToolUrl, {
          method: 'GET',
          params: {
            query: postCode,
          },
          headers: {
            'User-Agent':
              'Digitales Wartezimmer (https://digitales-wartezimmer.org)',
          },
        })
        .toPromise();
      results = response.data.results;
      assert(Array.isArray(results));
    } catch (error) {
      Sentry.captureException({
        message: 'RKI - PLZ  Info Tool Endpoint errored!',
        error,
      });
      this.logger.error('RKI - PLZ Tool Endpoint errored!', error);
      throw new ServiceUnavailableException(
        await this.i18n.t('health_office.rki_tool_error'),
      );
    }
    return results.map(HealthOfficeService.parseRKIDTOtoHealthOffice);
  }

  private static parseRKIDTOtoHealthOffice(dto: RKIDTO): HealthOffice {
    return {
      name: dto.name,
      department: dto.department,
      code: dto.code,
      address: {
        street: dto.street,
        postCode: dto.postalCode,
        place: dto.city,
      },
      contact: {
        phone: dto.covid19Hotline || dto.phone,
        fax: dto.fax,
        mail: dto.covid19Email || dto.email,
      },
      jurisdiction: [],
      transmissionType: 'MAIL',
      pdfServiceEnabled: false,
    };
  }
}
