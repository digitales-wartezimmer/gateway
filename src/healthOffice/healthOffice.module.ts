import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { HealthOfficeService } from './healthOffice.service';
import { HealthOfficeController } from './healthOffice.controller';
import { CacheModule } from '../util/cache/cache.module';

@Module({
  controllers: [HealthOfficeController],
  providers: [HealthOfficeService],
  exports: [HealthOfficeService],
  imports: [CacheModule, ConfigModule, HttpModule],
})
export class HealthOfficeModule {}
