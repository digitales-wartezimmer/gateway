import { Test } from '@nestjs/testing';
import { I18nModule } from '../i18n.module';
import { HttpModule, HttpService, Logger } from '@nestjs/common';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { Connector } from '../connectors/connector.schema';
import { SormasService } from './sormas.service';
import { SormasModule } from './sormas.module';
import { SERVICE_KEYS } from '../connectors/connector.constants';
import * as rxjs from 'rxjs';
import { I18nRequestScopeService, I18nService } from 'nestjs-i18n';
import { Person, PersonDTO } from './interfaces/person';
import * as uuid from 'uuid';
import { ContactDTO } from './interfaces/contact';

const connector: Connector = {
  code: 'demo',
  name: 'test',
  service: SERVICE_KEYS.SORMAS,
  url: 'https://sormas.de',
  username: 'foo',
  password: 'bar',
  userUuid: '123-abc',
};

jest.mock('uuid', () => {
  let value = 0;
  return {
    v4: () => {
      return 'uuid-abc' + (value++).toString();
    },
    resetValue: () => {
      value = 0;
    },
  };
});

const persons: Array<Person> = [
  {
    firstName: 'Erste',
    lastName: 'Person',
    address: {},
  },
  {
    firstName: 'Zweite',
    lastName: 'Person',
    phone: '+49151123456789',
    address: {
      city: 'Berlin',
      postalCode: '10115',
    },
  },
  {
    firstName: 'Dritte',
    lastName: 'Persona',
    email: 'test@test.de',
    address: {
      city: 'Berlin',
      street: 'Friedrichstraße',
    },
  },
  {
    firstName: 'Vierte',
    lastName: 'Person!',
    email: 'test@test.de',
    phone: '+49151123456789',
    address: {
      city: 'Berlin',
      street: 'Friedrichstraße',
      postalCode: '10115',
      houseNumber: '12',
    },
  },
];

const personDTOs: Array<PersonDTO> = [
  {
    address: {
      city: undefined,
      houseNumber: undefined,
      postalCode: undefined,
      street: undefined,
      uuid: 'UUID-ABC1',
    },
    emailAddress: undefined,
    firstName: 'Erste',
    lastName: 'Person',
    phone: undefined,
    uuid: 'UUID-ABC0',
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: undefined,
      postalCode: '10115',
      street: undefined,
      uuid: 'UUID-ABC3',
    },
    emailAddress: undefined,
    firstName: 'Zweite',
    lastName: 'Person',
    phone: '+49151123456789',
    uuid: 'UUID-ABC2',
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: undefined,
      postalCode: undefined,
      street: 'Friedrichstraße',
      uuid: 'UUID-ABC5',
    },
    emailAddress: 'test@test.de',
    firstName: 'Dritte',
    lastName: 'Persona',
    phone: undefined,
    uuid: 'UUID-ABC4',
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: '12',
      postalCode: '10115',
      street: 'Friedrichstraße',
      uuid: 'UUID-ABC7',
    },
    emailAddress: 'test@test.de',
    firstName: 'Vierte',
    lastName: 'Person!',
    phone: '+49151123456789',
    uuid: 'UUID-ABC6',
  },
];

const contactDTOs: Array<ContactDTO> = [
  {
    uuid: 'UUID-ABC0',
    caze: {
      uuid: 'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
    },
    person: {
      uuid: 'UUID-ABC0',
    },
    reportingUser: {
      uuid: connector.userUuid,
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment: '',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: 'UUID-ABC1',
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
  {
    uuid: 'UUID-ABC2',
    caze: {
      uuid: 'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
    },
    person: {
      uuid: 'UUID-ABC2',
    },
    reportingUser: {
      uuid: connector.userUuid,
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment: 'Telefonnummer: +49151123456789',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: 'UUID-ABC3',
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
  {
    uuid: 'UUID-ABC4',
    caze: {
      uuid: 'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
    },
    person: {
      uuid: 'UUID-ABC4',
    },
    reportingUser: {
      uuid: connector.userUuid,
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment: 'E-Mail: test@test.de',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: 'UUID-ABC5',
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
  {
    uuid: 'UUID-ABC6',
    caze: {
      uuid: 'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
    },
    person: {
      uuid: 'UUID-ABC6',
    },
    reportingUser: {
      uuid: connector.userUuid,
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment: 'Telefonnummer: +49151123456789 E-Mail: test@test.de',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: 'UUID-ABC7',
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
];

class CustomLogger extends TestingLogger {
  error(): void {}
}

class MockedHttpService {
  post(): rxjs.Observable<{ data: Array<object> | object | Array<string> }> {
    return rxjs.of({ data: this.getAPIReturnedData() });
  }

  getAPIReturnedData(): Array<object> | object | Array<string> {
    return [];
  }
}

class MockedI18nService {
  constructor(
    private readonly req: Request,
    private readonly i18nService: I18nService,
  ) {}

  t(arg: string): string {
    return arg;
  }
}

describe('SormasService', () => {
  let sormasService: SormasService;

  const httpService = new MockedHttpService();
  const logger = new CustomLogger();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [I18nModule, SormasModule, HttpModule],
      providers: [],
    })
      .overrideProvider(HttpService)
      .useValue(httpService)
      .overrideProvider(I18nRequestScopeService)
      .useClass(MockedI18nService)
      .compile();

    Logger.overrideLogger(logger);

    sormasService = await moduleRef.resolve<SormasService>(SormasService);
  });

  beforeEach(() => {
    jest.spyOn(logger, 'error');
    jest.spyOn(httpService, 'post');
    jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([
      {
        uuid: 'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
        pseudonymized: false,
        disease: 'CORONAVIRUS',
        person: {
          uuid: 'WZHC4K-ZDKMTW-GY52FP-3M4ESD2U',
          caption: 'Max MUSTERMANN',
          firstName: 'Max',
          lastName: 'Mustermann',
        },
      },
    ]);
    jest.clearAllMocks();
  });

  describe('SormasService', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      uuid.resetValue();
    });
    describe('checkCaseIdExists', () => {
      it('returns false if invalid uuid given', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([]);
        const result = await sormasService.checkCaseIdExists(
          connector,
          'wrong',
        );
        expect(result).toBeFalsy();
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['WRONG'],
          expect.anything(),
        );
      });
      it('returns false if none found', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([]);
        const result = await sormasService.checkCaseIdExists(
          connector,
          'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
        );
        expect(result).toBeFalsy();
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ'],
          expect.anything(),
        );
      });
      it('returns true if case is found', async () => {
        const result = await sormasService.checkCaseIdExists(
          connector,
          'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
        );
        expect(result).toBeTruthy();
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ'],
          {
            method: 'POST',
            headers: expect.any(Object),
            auth: {
              username: 'foo',
              password: 'bar',
            },
          },
        );
      });
      it('transforms the uuid to uppercase before sending', async () => {
        const result = await sormasService.checkCaseIdExists(
          connector,
          'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ'.toLowerCase(),
        );
        expect(result).toBeTruthy();
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ'],
          expect.anything(),
        );
      });
      it('throws an error if response format is unexpected', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue({ status: false });
        await expect(
          sormasService.checkCaseIdExists(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
          ),
        ).rejects.toThrowError('sormas.api_tool_error');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ'],
          expect.anything(),
        );
      });
      it('throws an error if API errors', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockImplementation(() => {
          throw new Error('Not Found');
        });
        await expect(
          sormasService.checkCaseIdExists(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
          ),
        ).rejects.toThrowError('sormas.api_tool_error');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ'],
          expect.anything(),
        );
      });
    });
    describe('createPersons', () => {
      it('throws error if no OK response from sormas', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([]);
        await expect(
          sormasService.createPersons(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            persons,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_persons');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          expect.anything(),
        );
      });
      it('throws error if incorrect amount of OK responses from sormas', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue(['OK']);
        await expect(
          sormasService.createPersons(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            persons,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_persons');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          expect.anything(),
        );
      });
      it('throws error if different response from sormas', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue(['Something else happened']);
        await expect(
          sormasService.createPersons(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            persons,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_persons');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          expect.anything(),
        );
      });
      it('throws an error if response format is unexpected', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue({ success: false });
        await expect(
          sormasService.createPersons(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            persons,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_persons');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          expect.anything(),
        );
      });
      it('throws an error if API errors', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockImplementation(() => {
          throw new Error('Something went wrong');
        });
        await expect(
          sormasService.createPersons(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            persons,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_persons');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          expect.anything(),
        );
      });
      it('resolves for OK response', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue(['OK', 'OK', 'OK', 'OK']);
        const result = await sormasService.createPersons(
          connector,
          'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
          persons,
        );
        expect(result).toEqual(personDTOs);
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          {
            method: 'POST',
            headers: expect.any(Object),
            auth: {
              username: 'foo',
              password: 'bar',
            },
          },
        );
      });
      it('handles empty strings', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue(['OK', 'OK', 'OK', 'OK']);
        const result = await sormasService.createPersons(
          connector,
          'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
          [
            {
              ...persons[0],
              phone: '  ',
              email: '  ',
              address: {
                city: '  ',
                street: '  ',
                postalCode: '  ',
                houseNumber: '  ',
              },
            },
            persons[1],
            persons[2],
            persons[3],
          ],
        );
        expect(result).toEqual(personDTOs);
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          expect.anything(),
        );
      });
    });
    describe('createContacts', () => {
      it('throws error if no OK response from sormas', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([]);
        await expect(
          sormasService.createContacts(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            personDTOs,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_contacts');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          expect.anything(),
        );
      });
      it('throws error if incorrect amount of OK responses from sormas', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue(['OK']);
        await expect(
          sormasService.createContacts(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            personDTOs,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_contacts');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          expect.anything(),
        );
      });
      it('throws error if different response from sormas', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue(['Something else happened']);
        await expect(
          sormasService.createContacts(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            personDTOs,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_contacts');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          expect.anything(),
        );
      });
      it('throws an error if response format is unexpected', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue({ success: false });
        await expect(
          sormasService.createContacts(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            personDTOs,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_contacts');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          expect.anything(),
        );
      });
      it('throws an error if API errors', async () => {
        jest.spyOn(httpService, 'getAPIReturnedData').mockImplementation(() => {
          throw new Error('Something went wrong');
        });
        await expect(
          sormasService.createContacts(
            connector,
            'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            personDTOs,
          ),
        ).rejects.toThrowError('sormas.api_tool_error_create_contacts');
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          expect.anything(),
        );
      });
      it('resolves for OK response', async () => {
        jest
          .spyOn(httpService, 'getAPIReturnedData')
          .mockReturnValue(['OK', 'OK', 'OK', 'OK']);
        const result = await sormasService.createContacts(
          connector,
          'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
          personDTOs,
        );
        expect(result).toEqual(contactDTOs);
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          {
            method: 'POST',
            headers: expect.any(Object),
            auth: {
              username: 'foo',
              password: 'bar',
            },
          },
        );
      });
    });
  });
});
