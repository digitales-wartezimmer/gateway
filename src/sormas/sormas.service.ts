import {
  HttpService,
  Injectable,
  Logger,
  ServiceUnavailableException,
} from '@nestjs/common';
import * as Sentry from '@sentry/minimal';
import { Connector } from '../connectors/connector.schema';
import { I18nRequestScopeService } from 'nestjs-i18n';
import * as assert from 'assert';
import { v4 as uuid } from 'uuid';
import { Person, PersonDTO } from './interfaces/person';
import { ContactDTO } from './interfaces/contact';

@Injectable()
export class SormasService {
  private readonly logger = new Logger(SormasService.name);

  constructor(
    private httpService: HttpService,
    private i18n: I18nRequestScopeService,
  ) {}

  private createUUID(): string {
    return uuid().toUpperCase();
  }

  private getCurrentTimestamp(): number {
    return Math.floor(Date.now() / 1000);
  }

  /**
   * Trim a string and return undefined if string is empty
   * @param str Input string
   */
  private getStringOrUndefined(str: string | undefined): string | undefined {
    if (!str) {
      return str;
    }
    const trimmed = str.trim();
    if (trimmed.length === 0) {
      return undefined;
    }
    return trimmed;
  }

  public async checkCaseIdExists(
    connector: Connector,
    caseId: string,
  ): Promise<boolean> {
    try {
      const response = await this.httpService
        .post(connector.url + '/cases/query', [caseId.toUpperCase()], {
          method: 'POST',
          headers: {
            'User-Agent':
              'Digitales Wartezimmer (https://digitales-wartezimmer.org)',
          },
          auth: {
            username: connector.username,
            password: connector.password,
          },
        })
        .toPromise();
      assert(Array.isArray(response.data));
      return response.data.length > 0;
    } catch (error) {
      Sentry.captureException({
        message: 'Sormas Integration errored',
        error,
      });
      this.logger.error('Sormas Integration errored', error);
      throw new ServiceUnavailableException(
        await this.i18n.t('sormas.api_tool_error'),
      );
    }
  }

  public async createPersons(
    connector: Connector,
    caseId: string,
    persons: Array<Person>,
  ): Promise<Array<PersonDTO>> {
    const personDTOs: Array<PersonDTO> = persons.map(person => {
      return {
        uuid: this.createUUID(),
        firstName: person.firstName,
        lastName: person.lastName,
        phone: this.getStringOrUndefined(person.phone),
        emailAddress: this.getStringOrUndefined(person.email),
        address: {
          uuid: this.createUUID(),
          city: this.getStringOrUndefined(person.address.city),
          postalCode: this.getStringOrUndefined(person.address.postalCode),
          street: this.getStringOrUndefined(person.address.street),
          houseNumber: this.getStringOrUndefined(person.address.houseNumber),
        },
      } as PersonDTO;
    });
    try {
      const response = await this.httpService
        .post(connector.url + '/persons/push', personDTOs, {
          method: 'POST',
          headers: {
            'User-Agent':
              'Digitales Wartezimmer (https://digitales-wartezimmer.org)',
          },
          auth: {
            username: connector.username,
            password: connector.password,
          },
        })
        .toPromise();
      assert(Array.isArray(response.data));
      assert(response.data.length === persons.length);
      // Check every value is okay
      assert(!response.data.some(value => value !== 'OK'));
      return personDTOs;
    } catch (error) {
      Sentry.captureException({
        message: 'Sormas Integration errored during creation of Persons',
        error,
      });
      this.logger.error(
        'Sormas Integration errored during creation of Persons',
        error,
      );
      throw new ServiceUnavailableException(
        await this.i18n.t('sormas.api_tool_error_create_persons'),
      );
    }
  }

  public async createContacts(
    connector: Connector,
    caseId: string,
    persons: Array<PersonDTO>,
  ): Promise<Array<ContactDTO>> {
    const contactDTOs: Array<ContactDTO> = persons.map(person => {
      const phoneString = person.phone
        ? `Telefonnummer: ${person.phone}`
        : undefined;
      const emailString = person.emailAddress
        ? `E-Mail: ${person.emailAddress}`
        : undefined;

      return {
        uuid: this.createUUID(),
        caze: {
          uuid: caseId.toUpperCase(),
        },
        disease: 'CORONAVIRUS',
        reportDateTime: this.getCurrentTimestamp(),
        reportingUser: {
          uuid: connector.userUuid,
        },
        contactClassification: 'UNCONFIRMED',
        person: {
          uuid: person.uuid,
        },
        healthConditions: {
          creationDate: this.getCurrentTimestamp(),
          uuid: this.createUUID(),
        },
        followUpStatus: 'FOLLOW_UP',
        followUpComment: [phoneString, emailString]
          .filter(str => !!str)
          .join(' '),
        contactIdentificationSource: 'TRACING_APP',
        tracingApp: 'OTHER',
        tracingAppDetails: 'Digitales Wartezimmer',
      } as ContactDTO;
    });
    try {
      const response = await this.httpService
        .post(connector.url + '/contacts/push', contactDTOs, {
          method: 'POST',
          headers: {
            'User-Agent':
              'Digitales Wartezimmer (https://digitales-wartezimmer.org)',
          },
          auth: {
            username: connector.username,
            password: connector.password,
          },
        })
        .toPromise();
      assert(Array.isArray(response.data));
      assert(response.data.length === persons.length);
      // Check every value is okay
      assert(!response.data.some(value => value !== 'OK'));
      return contactDTOs;
    } catch (error) {
      Sentry.captureException({
        message: 'Sormas Integration errored during creation of Contacts',
        error,
      });
      this.logger.error(
        'Sormas Integration errored during creation of Contacts',
        error,
      );
      throw new ServiceUnavailableException(
        await this.i18n.t('sormas.api_tool_error_create_contacts'),
      );
    }
  }
}
