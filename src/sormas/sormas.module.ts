import { HttpModule, Module } from '@nestjs/common';
import { SormasService } from './sormas.service';

@Module({
  controllers: [],
  providers: [SormasService],
  exports: [SormasService],
  imports: [HttpModule],
})
export class SormasModule {}
