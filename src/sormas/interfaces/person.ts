export interface Person {
  firstName: string;
  lastName: string;
  phone?: string;
  email?: string;
  address: {
    city?: string;
    postalCode?: string;
    street?: string;
    houseNumber?: string;
  };
}

export interface PersonDTO {
  uuid: string;
  firstName: string;
  lastName: string;
  phone?: string;
  emailAddress?: string;
  address: {
    uuid: string;
    city?: string;
    postalCode?: string;
    street?: string;
    houseNumber?: string;
  };
}
