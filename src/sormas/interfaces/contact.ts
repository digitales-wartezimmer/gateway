export interface Contact {
  caseId: string;
  personId: string;
  email?: string;
  phone?: string;
}

export interface ContactDTO {
  uuid: string;
  caze: {
    uuid: string;
  };
  disease: 'CORONAVIRUS' | string;
  reportDateTime: number;
  reportingUser: {
    uuid: string;
  };
  contactClassification: 'UNCONFIRMED' | 'CONFIRMED' | 'NO_CONTACT';
  person: {
    uuid: string;
  };
  healthConditions: {
    creationDate: number;
    uuid: string;
  };
  followUpStatus:
    | 'FOLLOW_UP'
    | 'COMPLETED'
    | 'CANCELED'
    | 'LOST'
    | 'NO_FOLLOW_UP';
  followUpComment: string;
  contactIdentificationSource:
    | 'CASE_PERSON'
    | 'CONTACT_PERSON'
    | 'TRACING_APP'
    | 'OTHER'
    | 'UNKNOWN';
  tracingApp: 'CORONA_WARN_APP' | 'OTHER' | 'UNKNOWN';
  tracingAppDetails: string;
}
