import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';

import { HealthOfficeModule } from './healthOffice/healthOffice.module';
import { FormsModule } from './forms/forms.module';
import { AppHealthModule } from './appHealth/appHealth.module';
import { I18nModule } from './i18n.module';
import { PlzInfoModule } from './plzInfo/plzInfo.module';
import { DBModule } from './db.module';
import { UtilModule } from './util/util.module';
import { ConnectorModule } from './connectors/connector.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.development.env', '.env'],
    }),
    I18nModule,
    DBModule,
    AppHealthModule,
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        ttl: config.get('RATE_LIMIT_TTL'),
        limit: config.get('RATE_LIMIT_LIMIT'),
      }),
    }),
    UtilModule,
    FormsModule,
    HealthOfficeModule,
    PlzInfoModule,
    ConnectorModule,
    AuthModule,
    UsersModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {}
