import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as helmet from 'helmet';
import * as session from 'express-session';
import { ConfigService } from '@nestjs/config';
import * as Sentry from '@sentry/node';
import * as passport from 'passport';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { setupSwaggerDocs } from './swagger';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const configService = app.get(ConfigService);

  Sentry.init({
    dsn: configService.get<string>('SENTRY_URL'),
    environment: configService.get<string>('ENV_NAME'),
  });
  app.use(
    Sentry.Handlers.requestHandler({
      request: false,
      user: false,
      ip: false,
    }),
  );
  app.use(Sentry.Handlers.errorHandler());

  app.setGlobalPrefix(configService.get<string>('URL_PREFIX'));

  app.set('trust proxy', true);

  const sessionParams = {
    secret: configService.get<string>('SESSION_SECRET'),
    secure: configService.get<string>('ENV_NAME') !== 'development',
    resave: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      sameSite: true,
      maxAge: 1000 * 60 * 30, // 30 minutes
      secure: configService.get<string>('ENV_NAME') !== 'development',
    },
  };
  app.use(session(sessionParams));

  app.use(cookieParser());

  app.use(passport.initialize());
  app.use(passport.session());

  //app.use(csrf());

  const cspDirectives = {
    directives: {
      defaultSrc: ["'self'"],
      scriptSrc: ["'self'"],
      styleSrc: ["'self'"],
      upgradeInsecureRequests: true,
    },
  };
  app.use(
    helmet({
      contentSecurityPolicy: cspDirectives,
    }),
  );

  app.enableCors({
    origin: [
      'https://digitales-wartezimmer.org',
      'https://staging.digitales-wartezimmer.org',
    ],
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    preflightContinue: false,
    credentials: true,
    optionsSuccessStatus: 204,
    allowedHeaders: ['Authorization', 'X-Custom-Lang'],
  });

  setupSwaggerDocs(app);

  const appLogger = new Logger('APP');
  appLogger.log(`Environment: ${configService.get('ENV_NAME')}`);

  await app.listen(configService.get<number>('PORT') || 3000);
}
bootstrap();
