import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {
  CacheInterceptor,
  CallHandler,
  INestApplication,
  NestInterceptor,
} from '@nestjs/common';
import { HealthOffice } from '../../healthOffice/interfaces/healthOffice';
import { HealthOfficeService } from '../../healthOffice/healthOffice.service';
import { RegisterAsContactModule } from './registerAsContact.module';
import { RegisterAsContactDTO } from './interfaces/registerAsContactDTO';
import { MailTemplate } from '../interfaces/mailTemplate';
import { I18nModule } from '../../i18n.module';
import { getModelToken } from '@nestjs/mongoose';
import { WorkflowConfiguration } from '../workflows/configuration/workflowConfiguration.schema';
import { TemplatesModule } from '../../util/templates/templates.module';
import { Observable } from 'rxjs';
import { Connector } from '../../connectors/connector.schema';
import { ConnectorModule } from '../../connectors/connector.module';
import { UsersService } from '../../users/users.service';
import { AuthModule } from '../../auth/auth.module';
import { User } from '../../users/users.schema';
import { Role } from '../../auth/roles.enum';

const exampleHealthOfficeData: HealthOffice = {
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  code: '1.08.4.25.',
  address: { street: 'Schillerstr. 30', postCode: '89077', place: 'Ulm' },
  contact: {
    phone: '0731 185-1730',
    fax: '0731 185-1738',
    mail: 'Gesundheitsamt@alb-donau-kreis.de',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}

const exampleRequestBody: RegisterAsContactDTO = {
  contact: {
    firstName: 'Andreas',
    lastName: 'Fiedler',
    birthday: '1990-01-19T00:00:00',
    gender: 'M',
    address: {
      street: 'Zinglerstraße 50',
      zip: '89077',
      city: 'Ulm',
    },
    email: 'test-dev@test.com',
    phone: '+491761234567',
  },
  infection: {
    sourceOfInfection: 'personal',
    contactData: {
      firstName: 'Hans',
      lastName: 'Müller',
      phone: '+4915124315261',
      email: 'test@test.de',
    },
    locality: 'Restaurant XY',
    lastContact: '2020-08-28T00:00:00',
    countRiskEncounters: '1',
    dateRiskEncounter: '2020-08-28T09:00:32',
    category: 2,
    reason: 'Begründung für Kontakt',
    hasSymptoms: true,
    symptoms: 'Husten',
    beginOfSymptoms: '2020-08-29T00:00:00',
    preconditions: {
      immuneDisease: false,
      heartDisease: true,
      diseaseExplanation: 'Diabetes',
    },
    pregnancy: true,
    pregnancyWeek: 12,
    personalNote: 'Meine persönliche Nachricht',
  },
  idempotencyToken: '2a372007-e717-40f4-9348-ef4f750f33f4',
  timestamp: '2020-08-30T06:48:00.765Z',
} as RegisterAsContactDTO;

const exampleEmailTemplate: MailTemplate = {
  userEmail: 'test-dev@test.com',
  healthOfficeEmail: 'Gesundheitsamt@alb-donau-kreis.de',
  subject: 'Meldung COVID-19 Verdachtsfall',
  body: expect.any(String),
};
function getResponseBody(template: MailTemplate) {
  return {
    workflow: 'email',
    submitted: false,
    errored: false,
    data: template,
  };
}
class MockedHealthOfficeService {
  findByPostcode(): Promise<Array<HealthOffice>> {
    return new Promise(resolve => {
      resolve(this.getHealthOfficeData());
    });
  }

  getHealthOfficeData(): Array<HealthOffice> {
    return [];
  }
}

class MockedWorkflowConfiguration {
  data: any;
  findOne() {
    this.data = {
      name: 'Test',
      code: '1234',
      workflows: {
        travelReturn: 'email',
        registerAsContact: 'email',
      },
    };
    return this;
  }
  exec() {
    return this.data;
  }
}

class MockedConnector {
  data: any;

  findOne() {
    return this;
  }
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('Register As Contact', () => {
  let app: INestApplication;
  const healthOfficeService = new MockedHealthOfficeService();
  const connectorModel = new MockedConnector();
  const userService = new MockedUserService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        RegisterAsContactModule,
        I18nModule,
        TemplatesModule,
        ConnectorModule,
        AuthModule,
      ],
    })
      .overrideProvider(HealthOfficeService)
      .useValue(healthOfficeService)
      .overrideProvider(getModelToken(WorkflowConfiguration.name))
      .useValue(MockedWorkflowConfiguration)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(getModelToken(Connector.name))
      .useValue(connectorModel)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  beforeEach(() => {
    jest
      .spyOn(userService, 'getData')
      .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
  });

  describe('POST /patientSubmission', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .send(exampleRequestBody)
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
      await request(app.getHttpServer())
        .post('/patientSubmission')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'User', role: Role.User, token: '1' });
      await request(app.getHttpServer())
        .post('/patientSubmission')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Monitor', role: Role.Monitor, token: '1' });
      await request(app.getHttpServer())
        .post('/patientSubmission')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'HealthOffice', role: Role.HealthOffice, token: '1' });
      await request(app.getHttpServer())
        .post('/patientSubmission')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .post('/patientSubmission')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(401);
    });
    it(`returns Email Template`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send(exampleRequestBody)
        .expect(201)
        .expect(res => {
          expect(res.body).toEqual(getResponseBody(exampleEmailTemplate));
          expect(res.body.data.body).toMatchSnapshot();
        });
    });
    it(`returns Email Template without given email`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          contact: {
            ...exampleRequestBody.contact,
            email: undefined,
          },
        })
        .expect(201)
        .expect(res => {
          expect(res.body).toEqual(
            getResponseBody({
              ...exampleEmailTemplate,
              userEmail: undefined,
            }),
          );
          expect(res.body.data.body).toMatchSnapshot();
        });
    });
    it(`throws an error if no health office matches`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          contact: {
            ...exampleRequestBody.contact,
            address: {
              ...exampleRequestBody.contact.address,
              zip: '12345',
            },
          },
        })
        .expect(404)
        .expect({
          statusCode: 404,
          message:
            'Es konnte kein Gesundheitsamt für die angegebene Postleitzahl gefunden werden',
          error: 'Not Found',
        });
    });
    it(`throws an error if validation of request body fails`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          infection: {
            ...exampleRequestBody.infection,
            contactData: undefined,
          },
        })
        .expect(400)
        .expect({
          statusCode: 400,
          message: ['infection.contactData should not be null or undefined'],
          error: 'Bad Request',
        });
    });
    it(`returns Email Template for personal infection`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          infection: {
            ...exampleRequestBody.infection,
            sourceOfInfection: 'personal',
          },
        })
        .expect(201)
        .expect(res => {
          expect(res.body).toMatchSnapshot();
        });
    });
    it(`returns Email Template for establishment infection`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          infection: {
            ...exampleRequestBody.infection,
            sourceOfInfection: 'establishment',
          },
        })
        .expect(201)
        .expect(res => {
          expect(res.body).toMatchSnapshot();
        });
    });
    it(`returns Email Template for CWA infection`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/patientSubmission')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          infection: {
            ...exampleRequestBody.infection,
            sourceOfInfection: 'cwa',
          },
        })
        .expect(201)
        .expect(res => {
          expect(res.body).toMatchSnapshot();
        });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
