import {
    IsEmail,
    IsNumber,
    IsPhoneNumber,
    IsString,
    Max,
    Min,
    ValidateNested,
    IsOptional, IsISO8601, IsEnum, ValidateIf, IsDefined, IsNumberString,
} from 'class-validator';
import {Type} from "class-transformer";
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Condition, ContactData, Terms } from '../../interfaces/shared';

export enum InfectionSource {
    PERSONAL = 'personal',
    CWA = 'cwa',
    ESTABLISHMENT = 'establishment'
}

class PersonalContactData {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional({
        description: 'First Name of the contact person',
    })
    firstName?: string;
    @IsOptional()
    @IsString()
    @ApiPropertyOptional({
        description: 'Last Name of the contact person',
    })
    lastName?: string;
    @ValidateIf((obj, value) => value !== '')
    @IsPhoneNumber("DE")
    @IsOptional()
    @ApiPropertyOptional({
        description: 'German phone number of the contact person',
    })
    phone?: string;
    @ValidateIf((obj, value) => value !== '')
    @IsOptional()
    @IsEmail()
    @ApiPropertyOptional({
        description: 'Email Address of the contact person',
    })
    email?: string;
}

class Infection extends Condition {
    @IsString()
    @IsEnum(InfectionSource)
    @ApiProperty({
        description: 'Source of Infection',
        enum: InfectionSource,
    })
    sourceOfInfection: InfectionSource;

    @ValidateIf(o => o.sourceOfInfection === InfectionSource.PERSONAL)
    @ValidateNested()
    @IsDefined()
    @Type(() => PersonalContactData)
    @ApiPropertyOptional({
        description: 'Information about contact person in case infection is via personal contact',
        type: PersonalContactData,
    })
    contactData?: PersonalContactData

    @ValidateIf(o => [InfectionSource.PERSONAL, InfectionSource.ESTABLISHMENT].includes(o.sourceOfInfection))
    @IsDefined()
    @IsISO8601({strict: true})
    @ApiPropertyOptional({
        description: 'Iso String of date of last contact in case of infection through personal contact or establishment',
    })
    lastContact?: string;
    @ValidateIf(o => [InfectionSource.PERSONAL, InfectionSource.ESTABLISHMENT].includes(o.sourceOfInfection))
    @ApiPropertyOptional({
        description: 'Information about the locality in case of infection through personal contact or establishment',
    })
    locality?: string;

    @ValidateIf(o => o.sourceOfInfection === InfectionSource.CWA)
    @IsDefined()
    @IsISO8601({strict: true})
    @ApiPropertyOptional({
        description: 'Iso string of the date of the risk encounter in case of infection notified by Corona Warn app',
    })
    dateRiskEncounter?: string;
    @ValidateIf(o => o.sourceOfInfection === InfectionSource.CWA)
    @IsDefined()
    @IsNumberString()
    @ApiPropertyOptional({
        description: 'Count of risk encounters as string in case of infection notified by Corona Warn app',
    })
    countRiskEncounters?: string;

    @ValidateIf(o => o.sourceOfInfection === InfectionSource.PERSONAL)
    @IsNumber()
    @Min(1)
    @Max(3)
    @ApiPropertyOptional({
        description: 'Contact Category level in case of infection through personal contact',
        minimum: 1,
        maximum: 3,
    })
    category?: number;
    @ValidateIf(o => o.sourceOfInfection === InfectionSource.PERSONAL)
    @IsString()
    @ApiPropertyOptional({
        description: 'Explanation of contact category assessment in case of infection through personal contact',
    })
    reason?: string;
}

export class RegisterAsContactDTO {
    @ApiProperty({
        description: 'Contact information of the user',
        type: ContactData,
    })
    @ValidateNested()
    @Type(() => ContactData)
    contact: ContactData;
    @ApiProperty({
        description: 'Information about the Infection and its source',
        type: Infection,
    })
    @ValidateNested()
    @Type(() => Infection)
    infection: Infection;
    @ValidateNested()
    @Type(() => Terms)
    @ApiProperty({
        description: 'Information about users acceptence of terms and conditions',
        type: Terms,
    })
    terms: Terms;
    @IsISO8601({strict: true})
    @ApiProperty({
        description: 'Timestamp of the request',
    })
    timestamp: string;
    @IsString()
    @ApiProperty({
        description: 'An idempotency token to identify requests',
    })
    idempotencyToken: string;
}
