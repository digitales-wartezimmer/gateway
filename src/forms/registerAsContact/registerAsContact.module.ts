import { Module } from '@nestjs/common'
import { RegisterAsContactController } from './registerAsContact.controller';
import { WorkflowConfigurationModule } from '../workflows/configuration/workflowConfiguration.module';

@Module({
  controllers: [RegisterAsContactController],
  providers: [],
  exports: [],
  imports: [WorkflowConfigurationModule],
})
export class RegisterAsContactModule {}
