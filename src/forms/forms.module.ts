import { Module } from '@nestjs/common'
import { RegisterAsContactModule } from './registerAsContact/registerAsContact.module';
import { TravelReturnModule } from './travelReturn/travelReturn.module';
import { IndexCaseModule } from './indexCase/indexCase.module';

@Module({
  controllers: [],
  providers: [],
  exports: [],
  imports: [RegisterAsContactModule, TravelReturnModule, IndexCaseModule],
})
export class FormsModule {}
