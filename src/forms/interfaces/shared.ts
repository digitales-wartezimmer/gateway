import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean, IsDefined,
  IsEmail, IsEnum,
  IsISO8601, IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsPostalCode,
  IsString, Max, Min, ValidateIf,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class Address {
  @ApiProperty({
    description: 'Street name and number',
  })
  @IsString()
  street: string;
  @IsString()
  @IsPostalCode('DE')
  @ApiProperty({
    description: 'Zip code (German)',
  })
  zip: string;
  @IsString()
  @ApiProperty({
    description: 'City',
  })
  city: string;
}

export enum Gender {
  MALE = 'M',
  FEMALE = 'F',
  DIVERSE = 'D'
}

export class ContactData {
  @ApiProperty({
    description: 'First Name',
  })
  @IsString()
  firstName: string;
  @IsString()
  @ApiProperty({
    description: 'Last Name',
  })
  lastName: string;
  @IsISO8601({strict: true})
  @ApiProperty({
    description: 'Date of birth as ISO string',
  })
  birthday: string;
  @IsString()
  @IsEnum(Gender)
  @ApiProperty({
    description: 'Gender',
    enum: Gender,
  })
  gender: Gender;
  @ValidateNested()
  @ApiProperty({
    description: 'Address Information',
    type: Address,
  })
  @Type(() => Address)
  address: Address;
  @ApiProperty({
    description: 'German Phone number',
  })
  @IsPhoneNumber("DE")
  phone: string;
  @ApiPropertyOptional({
    description: 'Email address',
  })
  @IsOptional()
  @IsEmail()
  email?: string;
}

class Preconditions {
  @ApiProperty({
    description: 'Existing Immune disease',
  })
  @IsBoolean()
  immuneDisease: boolean;
  @IsBoolean()
  @ApiProperty({
    description: 'Existing Heart disease',
  })
  heartDisease: boolean;
  @ValidateIf(o => o.immuneDisease || o.heartDisease)
  @IsDefined()
  @IsString()
  @ApiPropertyOptional({
    description: 'Explanation/Name of preexising condition',
  })
  diseaseExplanation?: string;
}

export class Condition {
  @ApiProperty({
    description: 'User experiences symptoms',
  })
  @IsBoolean()
  hasSymptoms: boolean;
  @ValidateIf(o => o.hasSymptoms)
  @IsString()
  @ApiPropertyOptional({
    description: 'Description of symptoms, required if symptoms exist',
  })
  symptoms?: string;
  @ValidateIf(o => o.hasSymptoms)
  @IsISO8601({strict: true})
  @ApiPropertyOptional({
    description: 'ISO string of date of begin of symptoms, required if symptoms exist',
  })
  beginOfSymptoms?: string;

  @ValidateNested()
  @IsDefined()
  @Type(() => Preconditions)
  @ApiProperty({
    description: 'Existing Preconditions',
    type: Preconditions,
  })
  preconditions: Preconditions;

  @IsBoolean()
  @ApiProperty({
    description: 'User is pregnant',
  })
  pregnancy: boolean;
  @ValidateIf(o => o.pregnancy)
  @IsNumber()
  @Min(0)
  @Max(42)
  @ApiPropertyOptional({
    description: 'Week of pregnancy, required if user is pregnant',
    minimum: 0,
    maximum: 42,
  })
  pregnancyWeek?: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    description: 'Personal Note from user to health office',
  })
  personalNote?: string;
}

export class Terms {
  @IsBoolean()
  @ApiProperty({
    description: 'User accepted privacy policy, not checked right now',
  })
  @IsOptional()
  privacy?: boolean;
  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional({
    description: 'User accepted terms of service, not checked right now',
  })
  termsOfService?: boolean;
}
