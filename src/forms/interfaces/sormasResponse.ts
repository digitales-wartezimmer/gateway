import { ApiPropertyOptional } from '@nestjs/swagger';

export class SormasResponse {
  @ApiPropertyOptional({
    description: 'Error Description if any',
  })
  error?: string;
}
