import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CSVFileAttachment {
    @ApiProperty({
        description: 'Filename of the CSV attachment',
    })
    fileName: string
    @ApiProperty({
        description: 'CSV File Content as JSON',
        example: [{ 'Column 1': 'Value 1', 'Column 2': 'Value 2' }, { 'Column 1': 'Value 1.1', 'Column 2': 'Value 2.1' }],
    })
    contentData: Array<Record<string, string>>
}

export class MailTemplate {
    @ApiPropertyOptional({
        description: 'Email of the user',
    })
    userEmail: string
    @ApiProperty({
        description: 'Email of the relevant Health office',
    })
    healthOfficeEmail: string
    @ApiProperty({
        description: 'Subject of the E-Mail',
    })
    subject: string
    @ApiProperty({
        description: 'Body of the E-Mail containing all relevant information',
    })
    body: string
    @ApiPropertyOptional({
        description: 'Optional CSV File Attachment',
        type: CSVFileAttachment,
    })
    csvAttachment?: CSVFileAttachment
}
