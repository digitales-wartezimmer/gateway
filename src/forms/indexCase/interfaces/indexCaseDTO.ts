import {
    ArrayNotEmpty,
    IsArray,
    IsEmail,
    IsISO8601,
    IsOptional,
    IsPhoneNumber,
    IsPostalCode,
    IsString,
    ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Terms } from '../../interfaces/shared';

export class PersonalInformation {
    @ApiProperty({
        description: 'First Name',
    })
    @IsString()
    firstName: string;
    @IsString()
    @ApiProperty({
        description: 'Last Name',
    })
    lastName: string;
    @IsString()
    @ApiPropertyOptional({
        description: 'Case ID',
    })
    @IsOptional()
    caseId?: string
    @IsOptional()
    @IsEmail()
    email?: string;
}

class PartialAddress {
    @ApiPropertyOptional({
        description: 'Street name',
    })
    @IsString()
    @IsOptional()
    street?: string;

    @ApiPropertyOptional({
        description: 'House number',
    })
    @IsString()
    @IsOptional()
    houseNumber?: string;

    @IsString()
    @IsPostalCode('DE')
    @ApiPropertyOptional({
        description: 'Zip code (German)',
    })
    @IsOptional()
    zip?: string;
    
    @IsString()
    @IsOptional()
    @ApiProperty({
        description: 'City',
    })
    city?: string;
}
export class ContactInformation {
    @ApiProperty({
        description: 'First Name',
    })
    @IsString()
    firstName: string;
    @IsString()
    @ApiProperty({
        description: 'Last Name',
    })
    lastName: string;
    @IsOptional()
    @IsPhoneNumber('DE')
    @ApiPropertyOptional({
      description: 'Phone number',
    })
    phone?: string;
    @ApiPropertyOptional({
        description: 'Email address',
    })
    @IsOptional()
    @IsEmail()
    email?: string;
    @ValidateNested()
    @ApiProperty({
        description: 'Address Information',
        type: PartialAddress,
    })
    @Type(() => PartialAddress)
    address: PartialAddress;
}

export class IndexCaseDTO {
    @ValidateNested()
    @Type(() => PersonalInformation)
    @ApiProperty({
        description: 'Personal Information of index person',
        type: PersonalInformation,
    })
    contact: PersonalInformation;
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => ContactInformation)
    @ApiProperty({
        description: 'List of contacts',
        type: [ContactInformation],
    })
    @ArrayNotEmpty()
    contacts: Array<ContactInformation>
    @IsString()
    @IsPostalCode('DE')
    @ApiProperty({
        description: 'Zip code (German)',
    })
    zip: string;
    @IsOptional()
    @IsString()
    @ApiPropertyOptional({
        description: 'Personal Note from user to health office',
    })
    personalNote?: string;

    @ValidateNested()
    @Type(() => Terms)
    @ApiProperty({
        description: 'Information about users acceptence of terms and conditions',
        type: Terms,
    })
    terms: Terms;
    @IsISO8601({strict: true})
    @ApiProperty({
        description: 'Timestamp of the request',
    })
    timestamp: string;
    @IsString()
    @ApiProperty({
        description: 'An idempotency token to identify requests',
    })
    idempotencyToken: string;
}
