import { Body, Controller, Post, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { IndexCaseDTO } from './interfaces/indexCaseDTO';
import { SentryInterceptor } from '../../interceptors/sentry.interceptor';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiHeader, ApiOperation, ApiTags } from '@nestjs/swagger';
import { I18n, I18nContext } from 'nestjs-i18n';
import * as Sentry from '@sentry/minimal';
import { FORM_KEYS } from '../workflows/configuration/workflowConfiguration.constants';
import { WorkflowConfigurationService } from '../workflows/configuration/workflowConfiguration.service';
import { ResponseDataDTO, ResponseDTO } from '../workflows/interfaces';
import { Roles } from '../../auth/roles.decorator';
import { Role } from '../../auth/roles.enum';


@UseInterceptors(SentryInterceptor)
@Controller('indexCase')
@ApiTags('Forms')
export class IndexCaseController {
  constructor (private workflowService: WorkflowConfigurationService) {
  }

  @Post('')
  @ApiOperation({ summary: 'Send Data from Index Case Form to Health Office directly or receive pre-prepared email contents' })
  @ApiCreatedResponse({
    description: 'Response depends on workflow and its result. If data has been sent successfully to the health office, the submitted value will be truthy without any further data. If data is validated but cannot be sent to health office directly, the email contents are included in response so the user can send it on its own.',
    type: ResponseDTO,
  })
  @ApiBadRequestResponse({ description: 'Given data did fail validation' })
  @ApiHeader({
    name: 'X-Custom-Lang',
    description: 'Set Language of Response or errors by two letter language code',
    example: 'de',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Pass API Token for Authorization with prefix Api-Key',
    example: 'Api-Key 123abc456def',
  })
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  @Roles(Role.User, Role.Admin)
  async getEmailTemplate (@Body() data: IndexCaseDTO, @I18n() i18n: I18nContext): Promise<ResponseDTO<ResponseDataDTO>> {
    Sentry.captureMessage('IndexCaseForm submitted');
    return await this.workflowService.processSubmission(FORM_KEYS.INDEX_CASE, data.zip, data, i18n.detectedLanguage);
  }
}
