import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {
  CacheInterceptor,
  CallHandler,
  HttpService,
  INestApplication,
  Logger,
  NestInterceptor,
  NotFoundException,
} from '@nestjs/common';
import { HealthOffice } from '../../healthOffice/interfaces/healthOffice';
import { HealthOfficeService } from '../../healthOffice/healthOffice.service';
import { MailTemplate } from '../interfaces/mailTemplate';
import { I18nModule } from '../../i18n.module';
import { WorkflowConfigurationModule } from '../workflows/configuration/workflowConfiguration.module';
import { getModelToken } from '@nestjs/mongoose';
import { WorkflowConfiguration } from '../workflows/configuration/workflowConfiguration.schema';
import { IndexCaseDTO } from './interfaces/indexCaseDTO';
import { IndexCaseModule } from './indexCase.module';
import { TemplatesModule } from '../../util/templates/templates.module';
import { Observable } from 'rxjs';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { Connector } from '../../connectors/connector.schema';
import { ConnectorService } from '../../connectors/connector.service';
import { ConnectorModule } from '../../connectors/connector.module';
import { SERVICE_KEYS } from '../../connectors/connector.constants';
import * as rxjs from 'rxjs';
import { PersonDTO } from '../../sormas/interfaces/person';
import { ContactDTO } from '../../sormas/interfaces/contact';
import { AuthModule } from '../../auth/auth.module';
import { UsersService } from '../../users/users.service';
import { User } from '../../users/users.schema';
import { Role } from '../../auth/roles.enum';

const exampleHealthOfficeData: HealthOffice = {
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  code: '1.08.4.25.',
  address: { street: 'Schillerstr. 30', postCode: '89077', place: 'Ulm' },
  contact: {
    phone: '0731 185-1730',
    fax: '0731 185-1738',
    mail: 'Gesundheitsamt@alb-donau-kreis.de',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

const exampleRequestBody: IndexCaseDTO = {
  idempotencyToken: '5f88ac88-b068-4aa0-8ec2-90970f5a145a',
  timestamp: '2020-09-11T12:52:53.128Z',
  contact: {
    firstName: 'Andreas',
    lastName: 'Fiedler',
    email: 'test@test.com',
  },
  zip: '89077',
  contacts: [
    {
      firstName: 'Manuela',
      lastName: 'Fiedler',
      email: 'test1@test.com',
      address: {},
    },
    {
      firstName: 'Jochen',
      lastName: 'Bauer',
      phone: '+4915112345678',
      address: {
        city: 'Berlin',
      },
    },
    {
      firstName: 'Markus',
      lastName: 'Müller',
      phone: '+4915187654321',
      email: 'markus.mueller@t-online.de',
      address: {
        street: 'Schwalbenweg',
        zip: '10115',
        city: 'Berlin',
        houseNumber: '15',
      },
    },
  ],
} as IndexCaseDTO;

const exampleEmailTemplate: MailTemplate = {
  userEmail: 'test@test.com',
  healthOfficeEmail: 'Gesundheitsamt@alb-donau-kreis.de',
  subject: 'Kontaktpersonen von Andreas Fiedler',
  body: expect.any(String),
};

const csvContentData = [
  {
    disease: 'CORONA',
    reportDateTime: '',
    lastContactDate: '',
    contactIdentificationSource: 'CASE_PERSON',
    contactClassification: 'CONFIRMEND',
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
    'person.firstName': 'Manuela',
    'person.lastName': 'Fiedler',
    'person.phone': '',
    'person.email': 'test1@test.com',
    'person.address.city': '',
    'person.address.postalCode': '',
    'person.address.street': '',
    'person.address.houseNumber': '',
  },
  {
    disease: 'CORONA',
    reportDateTime: '',
    lastContactDate: '',
    contactIdentificationSource: 'CASE_PERSON',
    contactClassification: 'CONFIRMEND',
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
    'person.firstName': 'Jochen',
    'person.lastName': 'Bauer',
    'person.phone': '+4915112345678',
    'person.email': '',
    'person.address.city': 'Berlin',
    'person.address.postalCode': '',
    'person.address.street': '',
    'person.address.houseNumber': '',
  },
  {
    disease: 'CORONA',
    reportDateTime: '',
    lastContactDate: '',
    contactIdentificationSource: 'CASE_PERSON',
    contactClassification: 'CONFIRMEND',
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
    'person.firstName': 'Markus',
    'person.lastName': 'Müller',
    'person.phone': '+4915187654321',
    'person.email': 'markus.mueller@t-online.de',
    'person.address.city': 'Berlin',
    'person.address.postalCode': '10115',
    'person.address.street': 'Schwalbenweg',
    'person.address.houseNumber': '15',
  },
];

const personDTOs: Array<PersonDTO> = [
  {
    address: {
      city: undefined,
      houseNumber: undefined,
      postalCode: undefined,
      street: undefined,
      uuid: expect.any(String),
    },
    emailAddress: 'test1@test.com',
    firstName: 'Manuela',
    lastName: 'Fiedler',
    phone: undefined,
    uuid: expect.any(String),
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: undefined,
      postalCode: undefined,
      street: undefined,
      uuid: expect.any(String),
    },
    emailAddress: undefined,
    firstName: 'Jochen',
    lastName: 'Bauer',
    phone: '+4915112345678',
    uuid: expect.any(String),
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: '15',
      postalCode: '10115',
      street: 'Schwalbenweg',
      uuid: expect.any(String),
    },
    emailAddress: 'markus.mueller@t-online.de',
    firstName: 'Markus',
    lastName: 'Müller',
    phone: '+4915187654321',
    uuid: expect.any(String),
  },
];

const contactDTOs: Array<ContactDTO> = [
  {
    uuid: expect.any(String),
    caze: {
      uuid: '123ABC',
    },
    person: {
      uuid: expect.any(String),
    },
    reportingUser: {
      uuid: '123-abc',
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment: 'E-Mail: test1@test.com',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: expect.any(String),
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
  {
    uuid: expect.any(String),
    caze: {
      uuid: '123ABC',
    },
    person: {
      uuid: expect.any(String),
    },
    reportingUser: {
      uuid: '123-abc',
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment: 'Telefonnummer: +4915112345678',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: expect.any(String),
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
  {
    uuid: expect.any(String),
    caze: {
      uuid: '123ABC',
    },
    person: {
      uuid: expect.any(String),
    },
    reportingUser: {
      uuid: '123-abc',
    },
    reportDateTime: expect.any(Number),
    contactClassification: 'UNCONFIRMED',
    contactIdentificationSource: 'TRACING_APP',
    disease: 'CORONAVIRUS',
    followUpComment:
      'Telefonnummer: +4915187654321 E-Mail: markus.mueller@t-online.de',
    followUpStatus: 'FOLLOW_UP',
    healthConditions: {
      creationDate: expect.any(Number),
      uuid: expect.any(String),
    },
    tracingApp: 'OTHER',
    tracingAppDetails: 'Digitales Wartezimmer',
  },
];

function getResponseBody(template: MailTemplate) {
  return {
    workflow: 'email',
    submitted: false,
    errored: false,
    data: template,
  };
}

class CustomLogger extends TestingLogger {
  error(): void {}
}

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}

class MockedHealthOfficeService {
  findByPostcode(): Promise<Array<HealthOffice>> {
    return new Promise(resolve => {
      resolve(this.getHealthOfficeData());
    });
  }

  getHealthOfficeData(): Array<HealthOffice> {
    return [];
  }
}

class MockedHttpService {
  post(
    url: string,
  ): rxjs.Observable<{
    data: Array<object> | object | Array<string> | undefined;
  }> {
    let data;
    if (url.includes('/cases/query')) {
      data = this.getCaseCheckAPIData();
    }
    if (url.includes('/persons/push')) {
      data = this.getPersonPushAPIData();
    }
    if (url.includes('/contacts/push')) {
      data = this.getContactPushAPIData();
    }
    return rxjs.of({ data });
  }

  getCaseCheckAPIData(): Array<object> | object {
    return [];
  }
  getPersonPushAPIData(): Array<string> {
    return [];
  }
  getContactPushAPIData(): Array<string> {
    return [];
  }
}

class MockedConnectorService {
  getConnector(): Promise<Connector> {
    return Promise.resolve({
      code: 'demo',
      name: 'test',
      service: 'sormas',
      url: 'https://sormas.de',
      username: 'foo',
      password: 'bar',
      userUuid: '123-abc',
    } as Connector);
  }
}
class MockedWorkflowConfiguration {
  data: any;

  findOne() {
    return this;
  }

  setEmailConfig() {
    this.data = {
      name: 'Test',
      code: '1234',
      workflows: {
        travelReturn: 'email',
        registerAsContact: 'email',
        indexCase: 'email',
      },
    };
  }

  setSormasConfig() {
    this.data = {
      name: 'Test',
      code: 'test',
      workflows: {
        travelReturn: 'email',
        registerAsContact: 'email',
        indexCase: 'sormas',
      },
    };
  }

  exec() {
    return this.data;
  }
}

class MockedConnector {
  data: any;

  findOne() {
    return this;
  }
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('IndexCase', () => {
  let app: INestApplication;
  const workflowConfigurationModel = new MockedWorkflowConfiguration();
  const connectorModel = new MockedConnector();
  const healthOfficeService = new MockedHealthOfficeService();
  const connectorService = new MockedConnectorService();
  const httpService = new MockedHttpService();
  const logger = new CustomLogger();
  const userService = new MockedUserService();
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        IndexCaseModule,
        WorkflowConfigurationModule,
        I18nModule,
        TemplatesModule,
        ConnectorModule,
        AuthModule,
      ],
    })
      .overrideProvider(HealthOfficeService)
      .useValue(healthOfficeService)
      .overrideProvider(getModelToken(WorkflowConfiguration.name))
      .useValue(workflowConfigurationModel)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(getModelToken(Connector.name))
      .useValue(connectorModel)
      .overrideProvider(ConnectorService)
      .useValue(connectorService)
      .overrideProvider(HttpService)
      .useValue(httpService)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();

    Logger.overrideLogger(logger);
    await app.init();
  });

  beforeEach(() => {
    jest
      .spyOn(healthOfficeService, 'getHealthOfficeData')
      .mockReturnValue([exampleHealthOfficeData]);
    jest
      .spyOn(userService, 'getData')
      .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
    jest.clearAllMocks();
  });

  describe('POST /indexCase', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .post('/indexCase')
        .send(exampleRequestBody)
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      workflowConfigurationModel.setEmailConfig();
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
      await request(app.getHttpServer())
        .post('/indexCase')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'User', role: Role.User, token: '1' });
      await request(app.getHttpServer())
        .post('/indexCase')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Monitor', role: Role.Monitor, token: '1' });
      await request(app.getHttpServer())
        .post('/indexCase')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'HealthOffice', role: Role.HealthOffice, token: '1' });
      await request(app.getHttpServer())
        .post('/indexCase')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .post('/indexCase')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(401);
    });
    describe('Email Workflow', () => {
      beforeAll(() => {
        workflowConfigurationModel.setEmailConfig();
      });
      it(`returns Email Template`, () => {
        return request(app.getHttpServer())
          .post('/indexCase')
          .send(exampleRequestBody)
          .set('Authorization', 'Api-Key 1')
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                csvAttachment: expect.any(Object),
              }),
            );
            expect(res.body.data.body).toMatchSnapshot();
          });
      });
      it(`returns Email Template without given email`, () => {
        return request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              email: undefined,
            },
          })
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                userEmail: undefined,
                csvAttachment: expect.any(Object),
              }),
            );
            expect(res.body.data.body).toMatchSnapshot();
          });
      });
      describe('it allows international phone numbers', () => {
        [
          '089963621',
          '015120611289',
          '+4917644228855',
          '+436769266761',
          '+1-202-555-0107',
        ].forEach(number => {
          it('allows phone number: ' + number, () => {
            return request(app.getHttpServer())
              .post('/indexCase')
              .set('Authorization', 'Api-Key 1')
              .send({
                ...exampleRequestBody,
                contacts: [
                  {
                    ...exampleRequestBody.contacts[0],
                    phone: number,
                  },
                ],
              })
              .expect(201)
              .expect(res => {
                expect(res.body).toEqual(
                  getResponseBody({
                    ...exampleEmailTemplate,
                    csvAttachment: expect.any(Object),
                  }),
                );
                expect(res.body.data.body).toContain(number);
                expect(res.body.data.body).toMatchSnapshot();
              });
          });
        });
      });

      it(`returns csv file`, () => {
        return request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send(exampleRequestBody)
          .expect(201)
          .expect(res =>
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                csvAttachment: {
                  contentData: csvContentData,
                  fileName: 'contacts_andreas_fiedler.csv',
                },
              }),
            ),
          );
      });
      it(`returns Email and csv with case number`, () => {
        return request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              caseId: '123abc',
            },
          })
          .expect(201)
          .expect(res =>
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                subject: 'Kontaktpersonen von 123abc',
                csvAttachment: {
                  contentData: csvContentData,
                  fileName: 'contacts_123abc.csv',
                },
              }),
            ),
          );
      });
      it(`throws an error if no health office matches`, () => {
        jest
          .spyOn(healthOfficeService, 'getHealthOfficeData')
          .mockReturnValue([]);
        return request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            zip: '12345',
          })
          .expect(404)
          .expect({
            statusCode: 404,
            message:
              'Es konnte kein Gesundheitsamt für die angegebene Postleitzahl gefunden werden',
            error: 'Not Found',
          });
      });
      it(`throws an error if validation of request body fails`, () => {
        return request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contacts: [],
          })
          .expect(400)
          .expect({
            statusCode: 400,
            message: ['contacts should not be empty'],
            error: 'Bad Request',
          });
      });
    });

    describe('Sormas Workflow', () => {
      beforeAll(() => {
        workflowConfigurationModel.setSormasConfig();
      });
      beforeEach(() => {
        jest.spyOn(connectorService, 'getConnector').mockResolvedValue({
          code: 'demo',
          name: 'test',
          service: SERVICE_KEYS.SORMAS,
          url: 'https://sormas.de',
          username: 'foo',
          password: 'bar',
          userUuid: '123-abc',
        });
        jest.spyOn(httpService, 'getCaseCheckAPIData').mockReturnValue([
          {
            uuid: 'TIDDPR-DG3GOG-EY3UYJ-2UWXCCDQ',
            pseudonymized: false,
            disease: 'CORONAVIRUS',
            person: {
              uuid: 'WZHC4K-ZDKMTW-GY52FP-3M4ESD2U',
              caption: 'Max MUSTERMANN',
              firstName: 'Max',
              lastName: 'Mustermann',
            },
          },
        ]);
        jest
          .spyOn(httpService, 'getPersonPushAPIData')
          .mockReturnValue(['OK', 'OK', 'OK']);
        jest
          .spyOn(httpService, 'getContactPushAPIData')
          .mockReturnValue(['OK', 'OK', 'OK']);
        jest.spyOn(httpService, 'post');
        jest.spyOn(logger, 'error');
        jest.clearAllMocks();
      });
      it('falls back to email without caseId', async () => {
        await request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send(exampleRequestBody)
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                csvAttachment: expect.any(Object),
              }),
            );
            expect(res.body.data.body).toMatchSnapshot();
          });
        expect(logger.error).toHaveBeenCalledWith(
          'IndexCaseSormasWorkflow: No caseId given But it was picked up by Fallback E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
      it('falls back to email when no connector found', async () => {
        jest.spyOn(connectorService, 'getConnector').mockImplementation(() => {
          throw new NotFoundException('Could not find Connector');
        });
        await request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              caseId: '123abc',
            },
          })
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                subject: 'Kontaktpersonen von 123abc',
                csvAttachment: expect.any(Object),
              }),
            );
            expect(res.body.data.body).toMatchSnapshot();
          });
        expect(logger.error).toHaveBeenCalledWith(
          'IndexCaseSormasWorkflow: Could not find Connector But it was picked up by Fallback E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
      it('returns error when case id not found', async () => {
        jest.spyOn(httpService, 'getCaseCheckAPIData').mockReturnValue([]);
        await request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              caseId: '123abc',
            },
          })
          .expect(400)
          .expect({
            statusCode: 400,
            message: 'Die angegebene Fall-ID konnte nicht gefunden werden',
            error: 'Bad Request',
          });
      });
      it('falls back to email when person creation failed', async () => {
        const err = new Error('Something bad happened');
        jest
          .spyOn(httpService, 'getPersonPushAPIData')
          .mockImplementation(() => {
            throw err;
          });
        await request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              caseId: '123abc',
            },
          })
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                subject: 'Kontaktpersonen von 123abc',
                csvAttachment: expect.any(Object),
              }),
            );
            expect(res.body.data.body).toMatchSnapshot();
          });
        expect(logger.error).toHaveBeenCalledWith(
          'Sormas Integration errored during creation of Persons',
          err,
          'SormasService',
        );
        expect(logger.error).toHaveBeenCalledWith(
          'IndexCaseSormasWorkflow: Die externe API von SORMAS ist nicht erreichbar oder hat einen Fehler produziert beim Erstellen von Personen But it was picked up by Fallback E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
      it('falls back to email when contact creation failed', async () => {
        const err = new Error('Something else happened');
        jest
          .spyOn(httpService, 'getContactPushAPIData')
          .mockImplementation(() => {
            throw err;
          });
        await request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              caseId: '123abc',
            },
          })
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual(
              getResponseBody({
                ...exampleEmailTemplate,
                subject: 'Kontaktpersonen von 123abc',
                csvAttachment: expect.any(Object),
              }),
            );
            expect(res.body.data.body).toMatchSnapshot();
          });
        expect(logger.error).toHaveBeenCalledWith(
          'Sormas Integration errored during creation of Contacts',
          err,
          'SormasService',
        );
        expect(logger.error).toHaveBeenCalledWith(
          'IndexCaseSormasWorkflow: Die externe API von SORMAS ist nicht erreichbar oder hat einen Fehler produziert beim Erstellen von Kontakten But it was picked up by Fallback E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
      it('sends data to sormas with caseId', async () => {
        await request(app.getHttpServer())
          .post('/indexCase')
          .set('Authorization', 'Api-Key 1')
          .send({
            ...exampleRequestBody,
            contact: {
              ...exampleRequestBody.contact,
              caseId: '123abc',
            },
          })
          .expect(201)
          .expect(res => {
            expect(res.body).toEqual({
              workflow: 'sormas',
              submitted: true,
              errored: false,
              data: {},
            });
          });
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/cases/query',
          ['123ABC'],
          {
            method: 'POST',
            headers: expect.any(Object),
            auth: {
              username: 'foo',
              password: 'bar',
            },
          },
        );
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/persons/push',
          personDTOs,
          {
            method: 'POST',
            headers: expect.any(Object),
            auth: {
              username: 'foo',
              password: 'bar',
            },
          },
        );
        expect(httpService.post).toHaveBeenCalledWith(
          'https://sormas.de/contacts/push',
          contactDTOs,
          {
            method: 'POST',
            headers: expect.any(Object),
            auth: {
              username: 'foo',
              password: 'bar',
            },
          },
        );
      });
    });
  });
  afterAll(async () => {
    await app.close();
  });
});
