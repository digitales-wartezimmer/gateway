import { Module } from '@nestjs/common'
import { WorkflowConfigurationModule } from '../workflows/configuration/workflowConfiguration.module';
import { IndexCaseController } from './indexCase.controller';

@Module({
  controllers: [IndexCaseController],
  providers: [],
  exports: [],
  imports: [WorkflowConfigurationModule],
})
export class IndexCaseModule {}
