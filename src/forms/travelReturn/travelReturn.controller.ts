import {
  Body,
  Controller,
  Post,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { SentryInterceptor } from '../../interceptors/sentry.interceptor';
import { TravelReturnDTO } from './interfaces/travelReturnDTO';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiHeader,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { I18n, I18nContext } from 'nestjs-i18n';
import { WorkflowConfigurationService } from '../workflows/configuration/workflowConfiguration.service';
import { FORM_KEYS } from '../workflows/configuration/workflowConfiguration.constants';
import { ResponseDataDTO, ResponseDTO } from '../workflows/interfaces';
import { Roles } from '../../auth/roles.decorator';
import { Role } from '../../auth/roles.enum';
import * as Sentry from '@sentry/minimal';

@UseInterceptors(SentryInterceptor)
@Controller('travelReturn')
@ApiTags('Forms')
export class TravelReturnController {
  constructor(private workflowService: WorkflowConfigurationService) {}

  @Post('')
  @ApiOperation({
    summary:
      'Send Data from Travel Return Form to Health Office directly or receive pre-prepared email contents',
  })
  @ApiCreatedResponse({
    description:
      'Data is validated but cannot be sent to health office directly. Therefore email contents are included in response so the user can send it on its own.',
    type: ResponseDTO,
  })
  @ApiBadRequestResponse({ description: 'Given data did fail validation' })
  @ApiHeader({
    name: 'X-Custom-Lang',
    description:
      'Set Language of Response or errors by two letter language code',
    example: 'de',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Pass API Token for Authorization with prefix Api-Key',
    example: 'Api-Key 123abc456def',
  })
  @Roles(Role.User, Role.Admin)
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  async getEmailTemplate(
    @Body() travelReturn: TravelReturnDTO,
    @I18n() i18n: I18nContext,
  ): Promise<ResponseDTO<ResponseDataDTO>> {
    Sentry.captureMessage('TravelReturnForm submitted');
    return await this.workflowService.processSubmission(
      FORM_KEYS.TRAVEL_RETURN,
      travelReturn.contact.address.zip,
      travelReturn,
      i18n.detectedLanguage,
    );
  }
}
