import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {
  CacheInterceptor,
  CallHandler,
  INestApplication,
  NestInterceptor,
} from '@nestjs/common';
import { HealthOffice } from '../../healthOffice/interfaces/healthOffice';
import { HealthOfficeService } from '../../healthOffice/healthOffice.service';
import { MailTemplate } from '../interfaces/mailTemplate';
import { TravelReturnModule } from './travelReturn.module';
import { TravelReturnDTO } from './interfaces/travelReturnDTO';
import { I18nModule } from '../../i18n.module';
import { WorkflowConfigurationModule } from '../workflows/configuration/workflowConfiguration.module';
import { getModelToken } from '@nestjs/mongoose';
import { WorkflowConfiguration } from '../workflows/configuration/workflowConfiguration.schema';
import { TemplatesModule } from '../../util/templates/templates.module';
import { Observable } from 'rxjs';
import { Connector } from '../../connectors/connector.schema';
import { ConnectorModule } from '../../connectors/connector.module';
import { AuthModule } from '../../auth/auth.module';
import { UsersService } from '../../users/users.service';
import { User } from '../../users/users.schema';
import { Role } from '../../auth/roles.enum';

const exampleHealthOfficeData: HealthOffice = {
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  code: '1.08.4.25.',
  address: { street: 'Schillerstr. 30', postCode: '89077', place: 'Ulm' },
  contact: {
    phone: '0731 185-1730',
    fax: '0731 185-1738',
    mail: 'Gesundheitsamt@alb-donau-kreis.de',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

const exampleRequestBody: TravelReturnDTO = {
  idempotencyToken: '5f88ac88-b068-4aa0-8ec2-90970f5a145a',
  timestamp: '2020-09-11T12:52:53.128Z',
  contact: {
    firstName: 'Andreas',
    lastName: 'Fiedler',
    birthday: '1990-10-10T00:00:00',
    gender: 'M',
    address: {
      street: 'Test 1',
      zip: '10115',
      city: 'Berlin Mitte',
    },
    email: 'test@test.com',
    phone: '+4915102028718',
  },
  travel: {
    country: 'Österreich',
    region: 'Innsbruck',
    duration: 3,
    dateOfEntry: '2020-09-09T00:00:00',
    modeOfEntry: ['bus', 'car'],
  },
  test: {
    hasTested: true,
    country: 'Österreich',
    result: 'positive',
    date: '2020-09-08T00:00:00',
  },
  condition: {
    hasSymptoms: true,
    symptoms: 'Husten ,Schnupfen',
    beginOfSymptoms: '2020-09-07T00:00:00',
    preconditions: {
      immuneDisease: true,
      heartDisease: false,
      diseaseExplanation: 'Rheuma',
    },
    pregnancy: true,
    pregnancyWeek: 30,
    personalNote: 'Test meine persönliche Nachricht',
  },
} as TravelReturnDTO;

const exampleEmailTemplate: MailTemplate = {
  userEmail: 'test@test.com',
  healthOfficeEmail: 'Gesundheitsamt@alb-donau-kreis.de',
  subject: 'Meldung COVID-19 Reiserückkehrer',
  body: expect.any(String),
};

function getResponseBody(template: MailTemplate) {
  return {
    workflow: 'email',
    submitted: false,
    errored: false,
    data: template,
  };
}

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}

class MockedHealthOfficeService {
  findByPostcode(): Promise<Array<HealthOffice>> {
    return new Promise(resolve => {
      resolve(this.getHealthOfficeData());
    });
  }

  getHealthOfficeData(): Array<HealthOffice> {
    return [];
  }
}

class MockedWorkflowConfiguration {
  data: any;

  findOne() {
    this.data = {
      name: 'Test',
      code: '1234',
      workflows: {
        travelReturn: 'email',
        registerAsContact: 'email',
      },
    };
    return this;
  }

  exec() {
    return this.data;
  }
}

class MockedConnector {
  data: any;

  findOne() {
    return this;
  }
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('Travel Return', () => {
  let app: INestApplication;
  const healthOfficeService = new MockedHealthOfficeService();
  const connectorModel = new MockedConnector();
  const userService = new MockedUserService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        TravelReturnModule,
        WorkflowConfigurationModule,
        I18nModule,
        TemplatesModule,
        ConnectorModule,
        AuthModule,
      ],
    })
      .overrideProvider(HealthOfficeService)
      .useValue(healthOfficeService)
      .overrideProvider(getModelToken(WorkflowConfiguration.name))
      .useValue(MockedWorkflowConfiguration)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(getModelToken(Connector.name))
      .useValue(connectorModel)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  beforeEach(() => {
    jest
      .spyOn(userService, 'getData')
      .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
  });

  describe('POST /travelReturn', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
      await request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'User', role: Role.User, token: '1' });
      await request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Monitor', role: Role.Monitor, token: '1' });
      await request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'HealthOffice', role: Role.HealthOffice, token: '1' });
      await request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(401);
    });
    it(`returns Email Template`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/travelReturn')
        .set('Authorization', 'Api-Key 1')
        .send(exampleRequestBody)
        .expect(201)
        .expect(res => {
          expect(res.body).toEqual(getResponseBody(exampleEmailTemplate));
          expect(res.body.data.body).toMatchSnapshot();
        });
    });
    it(`returns Email Template without given email`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/travelReturn')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          contact: {
            ...exampleRequestBody.contact,
            email: undefined,
          },
        })
        .expect(201)
        .expect(res => {
          expect(res.body).toEqual(
            getResponseBody({
              ...exampleEmailTemplate,
              userEmail: undefined,
            }),
          );
          expect(res.body.data.body).toMatchSnapshot();
        });
    });
    it(`throws an error if no health office matches`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([]);
      return request(app.getHttpServer())
        .post('/travelReturn')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          contact: {
            ...exampleRequestBody.contact,
            address: {
              ...exampleRequestBody.contact.address,
              zip: '12345',
            },
          },
        })
        .expect(404)
        .expect({
          statusCode: 404,
          message:
            'Es konnte kein Gesundheitsamt für die angegebene Postleitzahl gefunden werden',
          error: 'Not Found',
        });
    });
    it(`throws an error if validation of request body fails`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/travelReturn')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          test: {
            ...exampleRequestBody.test,
            date: undefined,
          },
        })
        .expect(400)
        .expect({
          statusCode: 400,
          message: ['test.date must be a valid ISO 8601 date string'],
          error: 'Bad Request',
        });
    });
    it(`returns Email Template for test`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/travelReturn')
        .send(exampleRequestBody)
        .set('Authorization', 'Api-Key 1')
        .expect(201)
        .expect(res => {
          expect(res.body).toMatchSnapshot();
        });
    });
    it(`returns Email Template for no test`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      return request(app.getHttpServer())
        .post('/travelReturn')
        .set('Authorization', 'Api-Key 1')
        .send({
          ...exampleRequestBody,
          test: {
            hasTested: false,
          },
        })
        .expect(201)
        .expect(res => {
          expect(res.body).toMatchSnapshot();
        });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
