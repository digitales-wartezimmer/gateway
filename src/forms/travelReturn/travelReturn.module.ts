import { Module } from '@nestjs/common'
import { TravelReturnController } from './travelReturn.controller';
import { WorkflowConfigurationModule } from '../workflows/configuration/workflowConfiguration.module';

@Module({
  controllers: [TravelReturnController],
  providers: [],
  exports: [],
  imports: [WorkflowConfigurationModule],
})
export class TravelReturnModule {}
