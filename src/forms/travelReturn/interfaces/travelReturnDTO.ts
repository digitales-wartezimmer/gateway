import {
    IsBoolean,
    IsNumber,
    IsString,
    Min,
    ValidateNested,
    IsISO8601,
    IsEnum,
    ValidateIf,
    IsArray,
    ArrayNotEmpty,
} from 'class-validator';
import {Type} from "class-transformer";
import { ContactData, Condition, Terms } from '../../interfaces/shared';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export enum TestResult {
    POSITIVE = 'positive',
    NEGATIVE = 'negative',
    UNKNOWN = 'unknown'
}

export enum ModeOfEntry {
    BUS = 'bus',
    CAR = 'car',
    PLANE = 'plane',
    SHIP = 'ship',
    TRAIN = 'train'
}

class Travel {
    @ApiProperty({
        description: 'Country the user travelled to',
    })
    @IsString()
    country: string;
    @ApiProperty({
        description: 'Region or cities the user travelled to',
    })
    @IsString()
    region: string;
    @IsNumber()
    @Min(0)
    @ApiProperty({
        description: 'Duration in days the user stayed in the country',
        minimum: 0,
    })
    duration: number;
    @IsISO8601({strict: true})
    @ApiProperty({
        description: 'ISO String of the date of entry back to Germany',
    })
    dateOfEntry: string;
    @IsArray()
    @ArrayNotEmpty()
    @IsEnum(ModeOfEntry, { each: true })
    @ApiProperty({
        description: 'Modes of entry how the user entried back into Germany',
        enum: ModeOfEntry,
        isArray: true,
    })
    modeOfEntry: Array<ModeOfEntry>;
}

class Test {
    @IsBoolean()
    @ApiProperty({
        description: 'Whether the user already performed a test',
    })
    hasTested: boolean;

    @ValidateIf(o => o.hasTested)
    @IsString()
    @ApiPropertyOptional({
        description: 'Country where the test has been performed, only required if user did get tested',
    })
    country: string;
    @ValidateIf(o => o.hasTested)
    @IsISO8601({strict: true})
    @ApiPropertyOptional({
        description: 'ISO string of test date, only required if user did get tested',
    })
    date: string;
    @ValidateIf(o => o.hasTested)
    @IsString()
    @IsEnum(TestResult)
    @ApiPropertyOptional({
        description: 'Result of Test, only required if user did get tested',
        enum: TestResult,
    })
    result: TestResult;
}

export class TravelReturnDTO {
    @ValidateNested()
    @Type(() => ContactData)
    @ApiProperty({
        description: 'Contact information of the user',
        type: ContactData,
    })
    contact: ContactData;
    @ValidateNested()
    @Type(() => Condition)
    @ApiProperty({
        description: 'Information about the condition and symptoms of the user',
        type: Condition,
    })
    condition: Condition;
    @ValidateNested()
    @Type(() => Travel)
    @ApiProperty({
        description: 'Information about the travel of the user',
        type: Travel,
    })
    travel: Travel;
    @ValidateNested()
    @Type(() => Test)
    @ApiProperty({
        description: 'Information about the test of the user',
        type: Test,
    })
    test: Test;
    @ValidateNested()
    @Type(() => Terms)
    @ApiProperty({
        description: 'Information about users acceptence of terms and conditions',
        type: Terms,
    })
    terms: Terms;
    @IsISO8601({strict: true})
    @ApiProperty({
        description: 'Timestamp of the request',
    })
    timestamp: string;
    @IsString()
    @ApiProperty({
        description: 'An idempotency token to identify requests',
    })
    idempotencyToken: string;
}
