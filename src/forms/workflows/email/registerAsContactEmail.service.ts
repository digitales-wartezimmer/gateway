import { Injectable } from '@nestjs/common';
import { InfectionSource, RegisterAsContactDTO } from '../../registerAsContact/interfaces/registerAsContactDTO';
import { MailTemplate } from "../../interfaces/mailTemplate";
import { EmailWorkflow } from './emailWorkflow';
import * as Sentry from '@sentry/minimal';

@Injectable()
export class RegisterAsContactEmailWorkflow extends EmailWorkflow {
    private async getSourceOfInfectionText(submission: RegisterAsContactDTO): Promise<string> {
        const sourceOfInfectionText: Record<InfectionSource, string> = {
            [InfectionSource.PERSONAL]: await this.i18n.t('services.email.register_as_contact.source.source_of_infection.personal'),
            [InfectionSource.CWA]: await this.i18n.t('services.email.register_as_contact.source.source_of_infection.cwa'),
            [InfectionSource.ESTABLISHMENT]: await this.i18n.t('services.email.register_as_contact.source.source_of_infection.establishment'),
        }
        return sourceOfInfectionText[submission.infection.sourceOfInfection]
    }

    private async getCategoryText(submission: RegisterAsContactDTO): Promise<string> {
        return await this.i18n.t('services.email.register_as_contact.source.personal.category_text.' + submission.infection.category)
    }

    public async getMailTemplate(submission: RegisterAsContactDTO, lang: string): Promise<MailTemplate> {
        const templateText = await this.templateService.getTemplate('email/register-as-contact', lang, {
            ...submission,
            contact: {
              ...submission.contact,
              genderText: await this.getGenderText(submission.contact.gender),
            },
            infection: {
               ...submission.infection,
               sourceOfInfectionText: await this.getSourceOfInfectionText(submission),
               categoryText: await this.getCategoryText(submission),
            },
          });
        Sentry.captureMessage('RegisterAsContactForm (E-Mail) completed');
        return {
            "subject": await this.i18n.t('services.email.register_as_contact.subject'),
            "body": templateText,
        } as MailTemplate
    }
}
