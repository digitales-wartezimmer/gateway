import { Injectable } from '@nestjs/common';
import { MailTemplate } from '../../interfaces/mailTemplate';
import { EmailWorkflow } from './emailWorkflow';
import { IndexCaseDTO } from '../../indexCase/interfaces/indexCaseDTO';
import * as Sentry from '@sentry/minimal';

@Injectable()
export class IndexCaseEmailWorkflow extends EmailWorkflow {
  private getCSVData (data: IndexCaseDTO): Array<Record<string, string>> {
    return data.contacts.map((contact) => ({
      disease: 'CORONA',
      reportDateTime: '',
      lastContactDate: '',
      contactIdentificationSource: 'CASE_PERSON',
      contactClassification: 'CONFIRMEND',
      tracingApp: 'OTHER',
      tracingAppDetails: 'Digitales Wartezimmer',
      'person.firstName': contact.firstName,
      'person.lastName': contact.lastName,
      'person.phone': contact.phone || '',
      'person.email': contact.email || '',
      'person.address.street': contact.address.street || '',
      'person.address.houseNumber': contact.address.houseNumber || '',
      'person.address.postalCode': contact.address.zip || '',
      'person.address.city': contact.address.city || '',
    }));
  }

  private async getSubject (data: IndexCaseDTO): Promise<string> {
    const byText = data.contact.caseId || `${data.contact.firstName} ${data.contact.lastName}`;
    return await this.i18n.t('services.email.index_case.subject', { args: [byText] });
  }

  private getCSVFileName (data: IndexCaseDTO): string {
    if (data.contact.caseId) {
      return `contacts_${data.contact.caseId}.csv`;
    }
    return `contacts_${data.contact.firstName.toLowerCase()}_${data.contact.lastName.toLowerCase()}.csv`;
  }

  public async getMailTemplate (data: IndexCaseDTO, lang: string): Promise<MailTemplate> {
    const templateText = await this.templateService.getTemplate('email/index-case', lang, data);
    Sentry.captureMessage('IndexCaseForm (E-Mail) completed');
    return {
      'subject': await this.getSubject(data),
      'body': templateText,
      'csvAttachment': {
        contentData: this.getCSVData(data),
        fileName: this.getCSVFileName(data),
      },
    } as MailTemplate;
  }
}
