import { MailTemplate } from '../../interfaces/mailTemplate';
import { Injectable } from '@nestjs/common';
import { Workflow } from '../workflow';
import { HealthOffice } from '../../../healthOffice/interfaces/healthOffice';
import { ResponseDTO, SubmissionDTO } from '../interfaces';
import { WORKFLOW_TYPES } from '../configuration/workflowConfiguration.constants';

@Injectable()
export abstract class EmailWorkflow extends Workflow {
  public abstract getMailTemplate (data: Record<string, any>, lang: string): Promise<MailTemplate>

  protected async getGenderText (gender: string): Promise<string> {
    switch (gender) {
      case 'M':
        return await this.i18n.t('services.email.gender.male');
      case 'F':
        return await this.i18n.t('services.email.gender.female');
      case 'W':
        return await this.i18n.t('services.email.gender.female');
      case 'D':
        return await this.i18n.t('services.email.gender.diverse');
      default:
        return await this.i18n.t('services.email.gender.unknown');
    }
  }

  public async process(healthOffice: HealthOffice, submission: SubmissionDTO, lang: string): Promise<ResponseDTO<MailTemplate>> {
    const template: MailTemplate = await this.getMailTemplate(submission, lang);

    return {
      workflow: WORKFLOW_TYPES.EMAIL,
      submitted: false,
      errored: false,
      data: {
        "userEmail": submission.contact.email,
        "healthOfficeEmail": healthOffice.contact.mail,
        ...template,
      },
    }
  }
}
