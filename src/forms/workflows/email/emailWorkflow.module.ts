import { Module } from '@nestjs/common';
import { HealthOfficeModule } from '../../../healthOffice/healthOffice.module';
import { TravelReturnEmailWorkflow } from './travelReturnEmail.service';
import { RegisterAsContactEmailWorkflow } from './registerAsContactEmail.service';
import { IndexCaseEmailWorkflow } from './indexCaseEmail.service';
import { TemplatesModule } from '../../../util/templates/templates.module';

@Module({
  imports: [HealthOfficeModule, TemplatesModule],
  providers: [TravelReturnEmailWorkflow, RegisterAsContactEmailWorkflow, IndexCaseEmailWorkflow],
  exports: [TravelReturnEmailWorkflow, RegisterAsContactEmailWorkflow, IndexCaseEmailWorkflow],
})
export class EmailWorkflowModule {}
