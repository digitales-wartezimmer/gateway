import { Injectable } from '@nestjs/common';
import { MailTemplate } from '../../interfaces/mailTemplate';
import { ModeOfEntry, TestResult, TravelReturnDTO } from '../../travelReturn/interfaces/travelReturnDTO';
import { EmailWorkflow } from './emailWorkflow';
import * as Sentry from '@sentry/minimal';

@Injectable()
export class TravelReturnEmailWorkflow extends EmailWorkflow {
  private async getModeOfEntryText (travelReturn: TravelReturnDTO): Promise<string> {
    const ModeOfEntryText: Record<ModeOfEntry, string> = {
      [ModeOfEntry.BUS]: await this.i18n.t('services.email.travel_return.entry.mode_text.bus'),
      [ModeOfEntry.CAR]: await this.i18n.t('services.email.travel_return.entry.mode_text.car'),
      [ModeOfEntry.PLANE]: await this.i18n.t('services.email.travel_return.entry.mode_text.plane'),
      [ModeOfEntry.SHIP]: await this.i18n.t('services.email.travel_return.entry.mode_text.ship'),
      [ModeOfEntry.TRAIN]: await this.i18n.t('services.email.travel_return.entry.mode_text.train'),
    };

    return travelReturn.travel.modeOfEntry.map((mode) => {
      return ModeOfEntryText[mode];
    }).join(', ');
  }

  private async getTestResultText (travelReturn: TravelReturnDTO): Promise<string> {
    const test = travelReturn.test;
    const TestResultText: Record<TestResult, string> = {
      [TestResult.POSITIVE]: await this.i18n.t('services.email.travel_return.test.result_text.positive'),
      [TestResult.NEGATIVE]: await this.i18n.t('services.email.travel_return.test.result_text.negative'),
      [TestResult.UNKNOWN]: await this.i18n.t('services.email.travel_return.test.result_text.pending'),
    };

    return TestResultText[test.result]
  }

  public async getMailTemplate (travelReturn: TravelReturnDTO, lang: string): Promise<MailTemplate> {
    const templateText = await this.templateService.getTemplate('email/travel-return', lang, {
      ...travelReturn,
      contact: {
        ...travelReturn.contact,
        genderText: await this.getGenderText(travelReturn.contact.gender),
      },
      travel: {
        ...travelReturn.travel,
        modeOfEntryText: await this.getModeOfEntryText(travelReturn),
      },
      test: {
        ...travelReturn.test,
        testResultText: await this.getTestResultText(travelReturn),
      },
    });
    Sentry.captureMessage('TravelReturnForm (E-Mail) completed');
    return {
      'subject': await this.i18n.t('services.email.travel_return.subject'),
      'body': templateText,
    } as MailTemplate;
  }
}
