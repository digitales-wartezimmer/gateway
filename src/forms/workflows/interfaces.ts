import { RegisterAsContactDTO } from '../registerAsContact/interfaces/registerAsContactDTO';
import { TravelReturnDTO } from '../travelReturn/interfaces/travelReturnDTO';
import { MailTemplate } from '../interfaces/mailTemplate';
import { WORKFLOW_TYPES } from './configuration/workflowConfiguration.constants';
import { ApiProperty, getSchemaPath } from '@nestjs/swagger';
import { IndexCaseDTO } from '../indexCase/interfaces/indexCaseDTO';
import { SormasResponse } from '../interfaces/sormasResponse';

export type SubmissionDTO =
  | RegisterAsContactDTO
  | TravelReturnDTO
  | IndexCaseDTO;

export type ResponseDataDTO = MailTemplate | SormasResponse;

export class ResponseDTO<T extends ResponseDataDTO> {
  @ApiProperty({
    description: 'Workflow type that was used to process the submission',
    enum: WORKFLOW_TYPES,
  })
  workflow: WORKFLOW_TYPES;
  @ApiProperty({
    description:
      'States whether the submission has been automatically processed or whether the user has to do additional steps (e.g. send an email)',
  })
  submitted: boolean;
  @ApiProperty({
    description:
      'States whether the submission did cause an error. However, this valu ebeing truthy does not necessarily mean, that data has not been processed. In case of an error there may be some fallback handling',
  })
  errored: boolean;
  @ApiProperty({
    description: 'Data Payload of the Response',
    type: 'object',
    oneOf: [
      { $ref: getSchemaPath(MailTemplate) },
      { $ref: getSchemaPath(SormasResponse) },
    ],
    examples: [{
      userEmail: 'test@test.de',
      healthOfficeEmail: 'health@office.de',
      subject: 'My Subject',
      body: 'My Email body with text',
      csvAttachment: {
        fileName: 'export.csv',
        contentData: [],
      },
    }, { error: 'CASE_ID_INVALID' }, {}],
  })
  data: ResponseDataDTO;
}
