import { BadRequestException, Injectable } from '@nestjs/common';
import { I18nRequestScopeService } from 'nestjs-i18n';
import { HealthOfficeService } from '../../healthOffice/healthOffice.service';
import { HealthOffice } from '../../healthOffice/interfaces/healthOffice';
import { ResponseDataDTO, ResponseDTO, SubmissionDTO } from './interfaces';
import { HealthOfficeNotFoundException } from '../../healthOffice/healthOffice.exception';
import { TemplateService } from '../../util/templates/templates.service';

@Injectable()
export abstract class Workflow {
  protected i18n: I18nRequestScopeService;
  protected healthOfficeService: HealthOfficeService;
  protected templateService: TemplateService;

  public constructor(
    protected readonly i18nService: I18nRequestScopeService,
    protected readonly healthService: HealthOfficeService,
    protected readonly templateCompilationService: TemplateService,
  ) {
    this.i18n = i18nService;
    this.healthOfficeService = healthService;
    this.templateService = templateCompilationService;
  }

  public async execute(
    postCode: string,
    submission: SubmissionDTO,
    lang: string,
  ): Promise<ResponseDTO<ResponseDataDTO>> {
    const healthOffice: Array<HealthOffice> = await this.healthOfficeService.findByPostcode(
      postCode,
    );
    if (!healthOffice || healthOffice.length === 0) {
      throw new HealthOfficeNotFoundException();
    }
    return await this.process(healthOffice[0], submission, lang);
  }
  public abstract process(
    healthOffice: HealthOffice,
    submission: SubmissionDTO,
    lang: string,
  ): Promise<ResponseDTO<ResponseDataDTO>>;
}
