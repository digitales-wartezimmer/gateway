import { Test } from '@nestjs/testing';
import { I18nModule } from '../../../i18n.module';
import { getModelToken } from '@nestjs/mongoose';
import { HealthOffice } from '../../../healthOffice/interfaces/healthOffice';
import { HealthOfficeModule } from '../../../healthOffice/healthOffice.module';
import { BadRequestException, Logger, NotFoundException } from '@nestjs/common';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { IndexCaseSormasWorkflow } from '../sormas/indexCaseSormas.service';
import { SormasWorkflowModule } from '../sormas/sormasWorkflow.module';
import { Connector } from '../../../connectors/connector.schema';
import { ConnectorService } from '../../../connectors/connector.service';
import { ConnectorModule } from '../../../connectors/connector.module';
import { IndexCaseDTO } from '../../../forms/indexCase/interfaces/indexCaseDTO';
import { SERVICE_KEYS } from '../../../connectors/connector.constants';
import { WORKFLOW_TYPES } from '../configuration/workflowConfiguration.constants';
import { SormasModule } from '../../../sormas/sormas.module';
import { SormasService } from '../../../sormas/sormas.service';
import { WorkflowProcessingException } from '../configuration/workflowConfiguration.exception';
import { Person, PersonDTO } from '../../../sormas/interfaces/person';
import { I18nRequestScopeService, I18nService } from 'nestjs-i18n';

const exampleHealthOfficeData: HealthOffice = {
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  code: '1.08.4.25.',
  address: { street: 'Schillerstr. 30', postCode: '89077', place: 'Ulm' },
  contact: {
    phone: '0731 185-1730',
    fax: '0731 185-1738',
    mail: 'Gesundheitsamt@alb-donau-kreis.de',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

const exampleRequestBody: IndexCaseDTO = {
  idempotencyToken: '5f88ac88-b068-4aa0-8ec2-90970f5a145a',
  timestamp: '2020-09-11T12:52:53.128Z',
  contact: {
    firstName: 'Andreas',
    lastName: 'Fiedler',
    email: 'test@test.com',
    caseId: '123abc',
  },
  zip: '89077',
  contacts: [
    {
      firstName: 'Manuela',
      lastName: 'Fiedler',
      email: 'test1@test.com',
      address: {},
    },
    {
      firstName: 'Jochen',
      lastName: 'Bauer',
      phone: '+4915112345678',
      address: {
        city: 'Berlin',
      },
    },
    {
      firstName: 'Markus',
      lastName: 'Müller',
      phone: '+4915187654321',
      email: 'markus.mueller@t-online.de',
      address: {
        street: 'Schwalbenweg',
        zip: '10115',
        city: 'Berlin',
        houseNumber: '15',
      },
    },
  ],
} as IndexCaseDTO;

const persons: Array<Person> = exampleRequestBody.contacts.map(person => {
  return {
    firstName: person.firstName,
    lastName: person.lastName,
    phone: person.phone,
    email: person.email,
    address: {
      postalCode: person.address.zip,
      city: person.address.city,
      street: person.address.street,
      houseNumber: person.address.houseNumber,
    },
  } as Person;
});

const personDTOs: Array<PersonDTO> = [
  {
    address: {
      city: undefined,
      houseNumber: undefined,
      postalCode: undefined,
      street: undefined,
      uuid: expect.any(String),
    },
    emailAddress: undefined,
    firstName: 'Erste',
    lastName: 'Person',
    phone: undefined,
    uuid: expect.any(String),
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: undefined,
      postalCode: '10115',
      street: undefined,
      uuid: expect.any(String),
    },
    emailAddress: undefined,
    firstName: 'Zweite',
    lastName: 'Person',
    phone: '+49151123456789',
    uuid: expect.any(String),
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: undefined,
      postalCode: undefined,
      street: 'Friedrichstraße',
      uuid: expect.any(String),
    },
    emailAddress: 'test@test.de',
    firstName: 'Dritte',
    lastName: 'Persona',
    phone: undefined,
    uuid: expect.any(String),
  },
  {
    address: {
      city: 'Berlin',
      houseNumber: '12',
      postalCode: '10115',
      street: 'Friedrichstraße',
      uuid: expect.any(String),
    },
    emailAddress: 'test@test.de',
    firstName: 'Vierte',
    lastName: 'Person!',
    phone: '+49151123456789',
    uuid: expect.any(String),
  },
];

class MockedConnector {
  data: any;

  findOne() {
    return this;
  }
}

class MockedConnectorService {
  getConnector(): Promise<Connector> {
    return Promise.resolve({
      code: 'demo',
      name: 'test',
      service: 'sormas',
      url: 'https://sormas.de',
      username: 'foo',
      password: 'bar',
      userUuid: '123-abc',
    } as Connector);
  }
}

class MockedSormasService {
  checkCaseIdExists(): Promise<boolean> {
    return Promise.resolve(true);
  }
  createPersons(): Promise<Array<PersonDTO>> {
    return Promise.resolve([]);
  }
  createContacts(): Promise<Array<object>> {
    return Promise.resolve([]);
  }
}

class MockedI18nService {
  constructor(
    private readonly req: Request,
    private readonly i18nService: I18nService,
  ) {}

  t(arg: string): string {
    return arg;
  }
}

class CustomLogger extends TestingLogger {
  error(): void {}
}

describe('IndexCaseSormasWorkflow', () => {
  let indexCaseSormasService: IndexCaseSormasWorkflow;

  const connectorService = new MockedConnectorService();
  const connectorModel = new MockedConnector();
  const sormasService = new MockedSormasService();
  const logger = new CustomLogger();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        I18nModule,
        HealthOfficeModule,
        SormasWorkflowModule,
        ConnectorModule,
        SormasModule,
      ],
      providers: [],
    })
      .overrideProvider(getModelToken(Connector.name))
      .useValue(connectorModel)
      .overrideProvider(ConnectorService)
      .useValue(connectorService)
      .overrideProvider(SormasService)
      .useValue(sormasService)
      .overrideProvider(I18nRequestScopeService)
      .useClass(MockedI18nService)
      .compile();

    Logger.overrideLogger(logger);

    indexCaseSormasService = await moduleRef.resolve<IndexCaseSormasWorkflow>(
      IndexCaseSormasWorkflow,
    );
  });

  beforeEach(() => {
    jest.spyOn(logger, 'error');
    jest.spyOn(connectorService, 'getConnector').mockResolvedValue({
      code: 'demo',
      name: 'test',
      service: SERVICE_KEYS.SORMAS,
      url: 'https://sormas.de',
      username: 'foo',
      password: 'bar',
      userUuid: '123-abc',
    });
    jest.spyOn(sormasService, 'checkCaseIdExists').mockResolvedValue(true);
    jest.spyOn(sormasService, 'createPersons').mockResolvedValue(personDTOs);
    jest.spyOn(sormasService, 'createContacts').mockResolvedValue([]);
    jest.clearAllMocks();
  });

  describe('IndexCaseSormasService', () => {
    describe('process', () => {
      it('throws an error if no case id given', async () => {
        await expect(
          indexCaseSormasService.process(
            exampleHealthOfficeData,
            {
              ...exampleRequestBody,
              contact: { ...exampleRequestBody.contact, caseId: undefined },
            },
            'de',
          ),
        ).rejects.toThrow(
          new WorkflowProcessingException(
            { message: 'No caseId given' },
            'IndexCaseSormasWorkflow',
          ),
        );
      });
      it('throws an error if no connector can be found', async () => {
        jest.spyOn(connectorService, 'getConnector').mockImplementation(() => {
          throw new NotFoundException('Could not find connector');
        });
        await expect(
          indexCaseSormasService.process(
            exampleHealthOfficeData,
            exampleRequestBody,
            'de',
          ),
        ).rejects.toThrow(
          new WorkflowProcessingException(
            { message: 'Could not find connector' },
            'IndexCaseSormasWorkflow',
          ),
        );
      });
      it('throws an error if case id does not exist', async () => {
        jest.spyOn(sormasService, 'checkCaseIdExists').mockResolvedValue(false);
        await expect(
          indexCaseSormasService.process(
            exampleHealthOfficeData,
            exampleRequestBody,
            'de',
          ),
        ).rejects.toThrow(new BadRequestException('workflows.case_id_invalid'));
      });
      it('throws an error if case id check fails', async () => {
        jest
          .spyOn(sormasService, 'checkCaseIdExists')
          .mockImplementation(() => {
            throw new Error('Not found');
          });
        await expect(
          indexCaseSormasService.process(
            exampleHealthOfficeData,
            exampleRequestBody,
            'de',
          ),
        ).rejects.toThrow(
          new WorkflowProcessingException(
            { message: 'Not found' },
            'IndexCaseSormasWorkflow',
          ),
        );
      });
      it('throws an error if creating persons fails', async () => {
        jest.spyOn(sormasService, 'createPersons').mockImplementation(() => {
          throw new Error('Failed to create Person');
        });
        await expect(
          indexCaseSormasService.process(
            exampleHealthOfficeData,
            exampleRequestBody,
            'de',
          ),
        ).rejects.toThrow(
          new WorkflowProcessingException(
            { message: 'Failed to create Person' },
            'IndexCaseSormasWorkflow',
          ),
        );
      });
      it('throws an error if creating contacts fails', async () => {
        jest.spyOn(sormasService, 'createContacts').mockImplementation(() => {
          throw new Error('Failed to create Contact');
        });
        await expect(
          indexCaseSormasService.process(
            exampleHealthOfficeData,
            exampleRequestBody,
            'de',
          ),
        ).rejects.toThrow(
          new WorkflowProcessingException(
            { message: 'Failed to create Contact' },
            'IndexCaseSormasWorkflow',
          ),
        );
      });
      it('returns successful response body', async () => {
        const result = await indexCaseSormasService.process(
          exampleHealthOfficeData,
          exampleRequestBody,
          'de',
        );
        expect(result).toEqual({
          workflow: WORKFLOW_TYPES.SORMAS,
          submitted: true,
          errored: false,
          data: {},
        });
        expect(sormasService.checkCaseIdExists).toHaveBeenCalledWith(
          {
            code: 'demo',
            name: 'test',
            password: 'bar',
            service: 'sormas',
            url: 'https://sormas.de',
            userUuid: '123-abc',
            username: 'foo',
          },
          '123abc',
        );
        // TODO adjust object
        expect(sormasService.createPersons).toHaveBeenCalledWith(
          {
            code: 'demo',
            name: 'test',
            password: 'bar',
            service: 'sormas',
            url: 'https://sormas.de',
            userUuid: '123-abc',
            username: 'foo',
          },
          '123abc',
          persons,
        );
        // TODO adjust object
        expect(sormasService.createContacts).toHaveBeenCalledWith(
          {
            code: 'demo',
            name: 'test',
            password: 'bar',
            service: 'sormas',
            url: 'https://sormas.de',
            userUuid: '123-abc',
            username: 'foo',
          },
          '123abc',
          personDTOs,
        );
      });
    });
  });
});
