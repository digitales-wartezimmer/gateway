import { Injectable } from '@nestjs/common';
import { I18nRequestScopeService } from 'nestjs-i18n';
import { SormasService } from '../../../sormas/sormas.service';
import { ConnectorService } from '../../../connectors/connector.service';
import { HealthOfficeService } from '../../../healthOffice/healthOffice.service';
import { TemplateService } from '../../../util/templates/templates.service';
import { Workflow } from '../workflow';

@Injectable()
export abstract class SormasWorkflow extends Workflow {
  protected connectorService: ConnectorService;
  protected sormasService: SormasService;
  public constructor(
    protected readonly connectorSrvc: ConnectorService,
    protected readonly i18nService: I18nRequestScopeService,
    protected readonly healthService: HealthOfficeService,
    protected readonly templateCompilationService: TemplateService,
    protected readonly sormasSrvc: SormasService,
  ) {
    super(i18nService, healthService, templateCompilationService);
    this.connectorService = connectorSrvc;
    this.sormasService = sormasSrvc;
  }
}
