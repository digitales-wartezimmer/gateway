import { Module } from '@nestjs/common';
import { ConnectorModule } from '../../../connectors/connector.module';
import { HealthOfficeModule } from '../../../healthOffice/healthOffice.module';

import { TemplatesModule } from '../../../util/templates/templates.module';
import { IndexCaseSormasWorkflow } from '../sormas/indexCaseSormas.service';
import { SormasModule } from '../../../sormas/sormas.module';

@Module({
  imports: [HealthOfficeModule, TemplatesModule, ConnectorModule, SormasModule],
  providers: [IndexCaseSormasWorkflow],
  exports: [IndexCaseSormasWorkflow],
})
export class SormasWorkflowModule {}
