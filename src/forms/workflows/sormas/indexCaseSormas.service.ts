import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import * as Sentry from '@sentry/minimal';
import { SERVICE_KEYS } from '../../../connectors/connector.constants';
import { Connector } from '../../..//connectors/connector.schema';
import { IndexCaseDTO } from '../../../forms/indexCase/interfaces/indexCaseDTO';
import { SormasResponse } from '../../../forms/interfaces/sormasResponse';
import { HealthOffice } from '../../../healthOffice/interfaces/healthOffice';
import { WORKFLOW_TYPES } from '../configuration/workflowConfiguration.constants';
import { WorkflowProcessingException } from '../configuration/workflowConfiguration.exception';
import { ResponseDTO } from '../interfaces';
import { SormasWorkflow } from './sormasWorkflow';
import { Person } from '../../../sormas/interfaces/person';

@Injectable()
export class IndexCaseSormasWorkflow extends SormasWorkflow {
  private readonly logger = new Logger(IndexCaseSormasWorkflow.name);

  public async process(
    healthOffice: HealthOffice,
    submission: IndexCaseDTO,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    lang: string,
  ): Promise<ResponseDTO<SormasResponse>> {
    if (!submission.contact.caseId) {
      Sentry.captureException({
        message: 'Sormas Workflow errored due to missing case id',
      });
      throw new WorkflowProcessingException(
        new Error('No caseId given'),
        IndexCaseSormasWorkflow.name,
      );
    }
    let connector: Connector;
    try {
      connector = await this.connectorService.getConnector(
        healthOffice.code,
        SERVICE_KEYS.SORMAS,
      );
    } catch (e) {
      Sentry.captureException({
        message: 'Sormas Workflow errored during retrieval of connector',
        error: e,
      });
      this.logger.error(e);
      throw new WorkflowProcessingException(e, IndexCaseSormasWorkflow.name);
    }

    let caseIdValid = false;
    const caseId = submission.contact.caseId;
    try {
      caseIdValid = await this.sormasService.checkCaseIdExists(
        connector,
        caseId,
      );
    } catch (e) {
      Sentry.captureException({
        message: 'Sormas Workflow errored during checking of case id',
        error: e,
      });
      this.logger.error(e);
      throw new WorkflowProcessingException(e, IndexCaseSormasWorkflow.name);
    }
    if (!caseIdValid) {
      throw new BadRequestException(
        await this.i18n.t('workflows.case_id_invalid'),
      );
    }

    try {
      const persons = await this.sormasService.createPersons(
        connector,
        caseId,
        submission.contacts.map(person => {
          return {
            firstName: person.firstName,
            lastName: person.lastName,
            phone: person.phone,
            email: person.email,
            address: {
              postalCode: person.address.zip,
              city: person.address.city,
              street: person.address.street,
              houseNumber: person.address.houseNumber,
            },
          } as Person;
        }),
      );
      await this.sormasService.createContacts(connector, caseId, persons);
    } catch (e) {
      Sentry.captureException({
        message: 'Sormas Workflow errored',
        error: e,
      });
      this.logger.error(e);
      throw new WorkflowProcessingException(e, IndexCaseSormasWorkflow.name);
    }
    Sentry.captureMessage('IndexCaseForm (SORMAS) completed');
    return {
      workflow: WORKFLOW_TYPES.SORMAS,
      submitted: true,
      errored: false,
      data: {},
    };
  }
}
