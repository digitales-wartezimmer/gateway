import {
  NotFoundException,
  NotImplementedException,
  ServiceUnavailableException,
} from '@nestjs/common';
import * as Sentry from '@sentry/minimal';

export class WorkflowConfigurationNotFoundException extends NotFoundException {
  constructor() {
    super('Workflow Configuration Not Found');
  }
}

export class ConfiguredWorkflowNotImplementedException extends NotImplementedException {
  constructor(workflowName: string) {
    super(
      'Could not find Workflow "' +
        workflowName +
        '" matching the configured Workflow',
    );
    Sentry.captureException(this);
  }
}

export class FormNotConfiguredException extends NotImplementedException {
  constructor(formName: string) {
    super(
      'Could not find form "' + formName + '" matching the configured Workflow',
    );
    Sentry.captureException(this);
  }
}

export class WorkflowProcessingException extends ServiceUnavailableException {
  constructor(
    objectOrError: Error | (object & { message: string }),
    workflowName: string,
  ) {
    objectOrError.message = objectOrError.message
      ? `${workflowName}: ${objectOrError.message}`
      : `${workflowName}: Unknown error`;
    super(
      objectOrError,
      'Workflow "' + workflowName + '" errored during processing',
    );
    Sentry.captureException(objectOrError);
  }
}
