import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WorkflowConfigurationService } from './workflowConfiguration.service';
import {
  WorkflowConfiguration,
  WorkflowConfigurationSchema,
} from './workflowConfiguration.schema';
import { WorkflowConfigurationController } from './workflowConfiguration.controller';
import { HealthOfficeModule } from '../../../healthOffice/healthOffice.module';
import { EmailWorkflowModule } from '../email/emailWorkflow.module';
import { SormasWorkflowModule } from '../sormas/sormasWorkflow.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: WorkflowConfiguration.name, schema: WorkflowConfigurationSchema },
    ]),
    HealthOfficeModule,
    EmailWorkflowModule,
    SormasWorkflowModule,
  ],
  controllers: [WorkflowConfigurationController],
  providers: [WorkflowConfigurationService],
  exports: [WorkflowConfigurationService, MongooseModule],
})
export class WorkflowConfigurationModule {}
