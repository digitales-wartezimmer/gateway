export enum FORM_KEYS {
  REGISTER_AS_CONTACT = 'registerAsContact',
  TRAVEL_RETURN = 'travelReturn',
  INDEX_CASE = 'indexCase',
}

export enum WORKFLOW_TYPES {
  EMAIL = 'email',
  SORMAS = 'sormas',
}
