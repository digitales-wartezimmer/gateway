import { Workflow } from '../workflow';
import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import {
  WorkflowConfiguration,
  WorkflowConfigurationDocument,
} from './workflowConfiguration.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { FORM_KEYS, WORKFLOW_TYPES } from './workflowConfiguration.constants';
import { I18nRequestScopeService } from 'nestjs-i18n';
import * as Sentry from '@sentry/minimal';
import { HealthOfficeService } from '../../../healthOffice/healthOffice.service';
import {
  ConfiguredWorkflowNotImplementedException,
  FormNotConfiguredException,
  WorkflowConfigurationNotFoundException,
  WorkflowProcessingException,
} from './workflowConfiguration.exception';
import { ResponseDataDTO, ResponseDTO, SubmissionDTO } from '../interfaces';
import { HealthOfficeNotFoundException } from '../../../healthOffice/healthOffice.exception';
import { RegisterAsContactEmailWorkflow } from '../email/registerAsContactEmail.service';
import { TravelReturnEmailWorkflow } from '../email/travelReturnEmail.service';
import { IndexCaseEmailWorkflow } from '../email/indexCaseEmail.service';
import assert = require('assert');
import { IndexCaseSormasWorkflow } from '../sormas/indexCaseSormas.service';

type WorkflowTypeMap = Record<FORM_KEYS, Record<WORKFLOW_TYPES, Workflow>>;

@Injectable()
export class WorkflowConfigurationService {
  private readonly logger = new Logger(WorkflowConfigurationService.name);

  public constructor(
    @InjectModel(WorkflowConfiguration.name)
    private workflowConfigurationModel: Model<WorkflowConfigurationDocument>,
    private readonly i18n: I18nRequestScopeService,
    private readonly healthOfficeService: HealthOfficeService,
    private readonly registerAsContactEmailWorkflowService: RegisterAsContactEmailWorkflow,
    private readonly travelReturnEmailWorkflowService: TravelReturnEmailWorkflow,
    private readonly indexCaseEmailWorkflowService: IndexCaseEmailWorkflow,
    private readonly indexCaseSormasWorkflowService: IndexCaseSormasWorkflow,
  ) {}

  private get workflowTypeMap(): WorkflowTypeMap {
    return {
      [FORM_KEYS.REGISTER_AS_CONTACT]: {
        [WORKFLOW_TYPES.EMAIL]: this.registerAsContactEmailWorkflowService,
        [WORKFLOW_TYPES.SORMAS]: this.registerAsContactEmailWorkflowService, // SORMAS not implemented yet, therefore fallback to email
      },
      [FORM_KEYS.TRAVEL_RETURN]: {
        [WORKFLOW_TYPES.EMAIL]: this.travelReturnEmailWorkflowService,
        [WORKFLOW_TYPES.SORMAS]: this.travelReturnEmailWorkflowService, // SORMAS not implemented yet, therefore fallback to email
      },
      [FORM_KEYS.INDEX_CASE]: {
        [WORKFLOW_TYPES.EMAIL]: this.indexCaseEmailWorkflowService,
        [WORKFLOW_TYPES.SORMAS]: this.indexCaseSormasWorkflowService,
      },
    };
  }

  public async findAllConfigurations(): Promise<WorkflowConfiguration[]> {
    return this.workflowConfigurationModel.find().exec();
  }

  public async getConfigurationByCode(
    code: string,
  ): Promise<WorkflowConfiguration> {
    return await this.workflowConfigurationModel.findOne({ code }).exec();
  }

  public async getConfigurationByPostCode(
    postCode: string,
  ): Promise<WorkflowConfiguration> {
    const healthOffice = await this.healthOfficeService.findByPostcode(
      postCode,
    );
    if (healthOffice.length === 0) {
      throw new HealthOfficeNotFoundException();
    }
    try {
      const configuration = await this.getConfigurationByCode(
        healthOffice[0].code,
      );
      assert(configuration);
      return configuration;
    } catch (e) {
      Sentry.captureException({
        message: 'Workflow Configuration not found',
        error: e,
      });
      throw new WorkflowConfigurationNotFoundException();
    }
  }

  public async processSubmission(
    formKey: FORM_KEYS,
    postCode: string,
    submission: SubmissionDTO,
    lang: string,
  ): Promise<ResponseDTO<ResponseDataDTO>> {
    try {
      const service: Workflow = await this.getWorkflowByPostCodeAndForm(
        postCode,
        formKey,
      );
      assert(service);
      return await service.execute(postCode, submission, lang);
    } catch (e) {
      if (e instanceof HealthOfficeNotFoundException) {
        throw new NotFoundException(
          await this.i18n.t('health_office.none_found'),
        );
      } else if (e instanceof WorkflowProcessingException) {
        Sentry.captureException({
          message: 'Workflow errored, but is being picked up by Fallback E-Mail Workflow',
          error: e,
        });
        this.logger.error(
          e.message + ' But it was picked up by Fallback E-Mail workflow',
        );
        const service: Workflow = this.getWorkflowByType(
          formKey,
          WORKFLOW_TYPES.EMAIL,
        );
        assert(service);
        return await service.execute(postCode, submission, lang);
      } else {
        Sentry.captureException({
          message: 'Workflow Error',
          error: e,
        });
        throw e;
      }
    }
  }

  private getWorkflowByType(
    formKey: FORM_KEYS,
    type: WORKFLOW_TYPES,
  ): Workflow {
    const formConfiguration = this.workflowTypeMap[formKey];
    if (!formConfiguration) {
      throw new FormNotConfiguredException(formKey);
    }
    const workflow = formConfiguration[type];
    if (!workflow) {
      throw new ConfiguredWorkflowNotImplementedException(type);
    }
    return workflow;
  }

  private async getWorkflowByPostCodeAndForm(
    postCode: string,
    formKey: FORM_KEYS,
  ): Promise<Workflow> {
    let configuration: WorkflowConfiguration;
    try {
      configuration = await this.getConfigurationByPostCode(postCode);
    } catch (e) {
      if (e instanceof WorkflowConfigurationNotFoundException) {
        configuration = null;
      } else {
        throw e;
      }
    }
    try {
      assert(configuration);
      const serviceType = configuration!.workflows[formKey];
      return this.getWorkflowByType(formKey, serviceType);
    } catch (e) {
      if (
        !configuration ||
        e instanceof WorkflowConfigurationNotFoundException
      ) {
      } else if (e instanceof FormNotConfiguredException) {
        throw e;
      } else {
        this.logger.error(
          e.message + ' But it was handled with Fallback to E-Mail workflow',
        );
      }
      return this.getWorkflowByType(formKey, WORKFLOW_TYPES.EMAIL);
    }
  }
}
