import {
  CacheInterceptor,
  CallHandler,
  INestApplication,
  NestInterceptor,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { WorkflowConfigurationModule } from './workflowConfiguration.module';
import { I18nModule } from '../../../i18n.module';
import { getModelToken } from '@nestjs/mongoose';
import { WorkflowConfiguration } from './workflowConfiguration.schema';
import * as request from 'supertest';
import { HealthOffice } from '../../../healthOffice/interfaces/healthOffice';
import { HealthOfficeService } from '../../../healthOffice/healthOffice.service';
import { ConnectorModule } from '../../../connectors/connector.module';
import { Observable } from 'rxjs';
import { Connector } from '../../../connectors/connector.schema';
import { User } from '../../../users/users.schema';
import { AuthModule } from '../../../auth/auth.module';
import { UsersService } from '../../../users/users.service';
import { Role } from '../../../auth/roles.enum';

const exampleHealthOfficeData: HealthOffice = {
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  code: '1.08.4.25.',
  address: { street: 'Schillerstr. 30', postCode: '89077', place: 'Ulm' },
  contact: {
    phone: '0731 185-1730',
    fax: '0731 185-1738',
    mail: 'Gesundheitsamt@alb-donau-kreis.de',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

class MockedHealthOfficeService {
  findByPostcode(): Promise<Array<HealthOffice>> {
    return new Promise(resolve => {
      resolve(this.getHealthOfficeData());
    });
  }

  getHealthOfficeData(): Array<HealthOffice> {
    return [];
  }
}

class MockedWorkflowConfiguration {
  data: any;

  getData() {
    return undefined;
  }

  findOne() {
    return this;
  }

  find() {
    return this;
  }

  exec() {
    return this.getData();
  }
}

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}

class MockedConnector {
  data: any;

  findOne() {
    return this;
  }
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('WorkflowConfiguration', () => {
  let app: INestApplication;
  const workflowConfigurationModel = new MockedWorkflowConfiguration();
  const healthOfficeService = new MockedHealthOfficeService();
  const connectorModel = new MockedConnector();
  const userService = new MockedUserService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [WorkflowConfigurationModule, I18nModule, ConnectorModule, AuthModule],
    })
      .overrideProvider(HealthOfficeService)
      .useValue(healthOfficeService)
      .overrideProvider(getModelToken(WorkflowConfiguration.name))
      .useValue(workflowConfigurationModel)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(getModelToken(Connector.name))
      .useValue(connectorModel)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  beforeEach(() => {
    jest
      .spyOn(userService, 'getData')
      .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
  });

  describe('GET /workflowConfigurations/all', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .expect(401);
    });
    it('only allows admins to access this resource', async () => {
      jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue([]);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(200);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'User', role: Role.User, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Monitor', role: Role.Monitor, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'HealthOffice', role: Role.HealthOffice, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(401);
    });

    it(`returns all workflow configurations`, () => {
      const data = [
        {
          name: 'Test 2',
          code: '1234',
          workflows: {
            travelReturn: 'email',
            registerAsContact: 'other',
          },
        },
        {
          name: 'Test 1',
          code: '5678',
          workflows: {
            travelReturn: 'log',
            registerAsContact: 'email',
          },
        },
      ];
      jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);
      return request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect(data);
    });

    it(`returns empty array if no workflow configurations exist`, () => {
      const data = [];
      jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);
      return request(app.getHttpServer())
        .get('/workflowConfigurations/all')
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([]);
    });
  });

  describe('GET /workflowConfigurations', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .get('/workflowConfigurations')
        .query({ postCode: '89077' })
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      const data = {
        name: 'Test 2',
        code: '1234',
        workflows: {
          travelReturn: 'email',
          registerAsContact: 'other',
        },
      };
      jest.spyOn(workflowConfigurationModel, 'findOne');
      jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Admin', role: Role.Admin, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(200);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'User', role: Role.User, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(403);
        jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'HealthOffice', role: Role.HealthOffice, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(403);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ _id: '1', id: '1', name: 'Monitor', role: Role.Monitor, token: '1' });
      await request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(401);
    });

    it('requires a postCode query', () => {
      return request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .expect(400)
        .expect({
          error: 'Bad Request',
          message: [
            'postCode must be a postal code',
            'postCode must be a string',
          ],
          statusCode: 400,
        });
    });
    it('validates the postCode query', () => {
      return request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: 'invalid' })
        .expect(400)
        .expect({
          error: 'Bad Request',
          message: ['postCode must be a postal code'],
          statusCode: 400,
        });
    });
    it('returns an error if no matching health office found', () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([]);
      return request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '12345' })
        .expect(404)
        .expect({
          statusCode: 404,
          message:
            'Es konnte kein Gesundheitsamt für die angegebene Postleitzahl gefunden werden',
          error: 'Not Found',
        });
    });
    it(`returns the workflow configurations`, async () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      const data = {
        name: 'Test 2',
        code: '1234',
        workflows: {
          travelReturn: 'email',
          registerAsContact: 'other',
        },
      };
      jest.spyOn(workflowConfigurationModel, 'findOne');
      jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);
      await request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(200)
        .expect(data);
      expect(workflowConfigurationModel.findOne).toHaveBeenCalledWith({
        code: exampleHealthOfficeData.code,
      });
    });

    it(`returns an error if no workflow configurations exist`, () => {
      jest
        .spyOn(healthOfficeService, 'getHealthOfficeData')
        .mockReturnValue([exampleHealthOfficeData]);
      const data = undefined;
      jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);
      return request(app.getHttpServer())
        .get('/workflowConfigurations')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(404)
        .expect({
          statusCode: 404,
          message:
            'Es konnte keine spezifische Workflow Konfiguration für das zuständige Gesundheitsamt gefunden werden',
          error: 'Not Found',
        });
    });
  });
});
