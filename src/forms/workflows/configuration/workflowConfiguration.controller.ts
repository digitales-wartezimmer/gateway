import {
  Controller,
  Get,
  Header,
  NotFoundException,
  Query,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { SentryInterceptor } from '../../../interceptors/sentry.interceptor';
import {
  ApiBadRequestResponse,
  ApiHeader, ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { WorkflowConfigurationService } from './workflowConfiguration.service';
import { PostCode } from '../../../healthOffice/interfaces/postCode';
import { WorkflowConfiguration } from './workflowConfiguration.schema';
import { I18n, I18nContext } from 'nestjs-i18n';
import {
  WorkflowConfigurationNotFoundException,
} from './workflowConfiguration.exception';
import { HealthOfficeNotFoundException } from '../../../healthOffice/healthOffice.exception';
import { Roles } from '../../../auth/roles.decorator';
import { Role } from '../../../auth/roles.enum';


@UseInterceptors(SentryInterceptor)
@Controller('workflowConfigurations')
@ApiTags('WorkflowConfigurations')
export class WorkflowConfigurationController {
  constructor (private workflowConfigurationService: WorkflowConfigurationService) {
  }

  @Get('all')
  @ApiOperation({ summary: 'Get All existing Workflow configurations' })
  @ApiOkResponse({ description: 'Response contains array of all existing workflow configurations', type: [WorkflowConfiguration] })
  @ApiHeader({
    name: 'X-Custom-Lang',
    description: 'Set Language of Response or errors by two letter language code',
    example: 'de',
  })
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  @Header('Cache-Control', 'no-cache, max-age=0')
  @Roles(Role.Admin)
  async getAllWorkflows (): Promise<Array<WorkflowConfiguration>> {
    return this.workflowConfigurationService.findAllConfigurations();
  }

  @Get('')
  @ApiOperation({ summary: 'Get existing Workflow configurations by postal code' })
  @ApiOkResponse({ description: 'Response contains existing workflow configuration for given postal code', type: WorkflowConfiguration })
  @ApiBadRequestResponse({ description: 'Given Postal Code did not pass validation' })
  @ApiNotFoundResponse({ description: 'No Health Office or Workflow configuration was found for given postal Code' })
  @ApiHeader({
    name: 'X-Custom-Lang',
    description: 'Set Language of Response or errors by two letter language code',
    example: 'de',
  })
  @Header('Cache-Control', 'no-cache, max-age=0')
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  @Roles(Role.Admin)
  async getWorkflowByPostCode (@I18n() i18n: I18nContext, @Query() postCode: PostCode): Promise<WorkflowConfiguration> {
    try {
      return await this.workflowConfigurationService.getConfigurationByPostCode(postCode.postCode);
    } catch (e) {
      if (e instanceof WorkflowConfigurationNotFoundException) {
        throw new NotFoundException(await i18n.t('workflows.none_found'));
      } else if (e instanceof HealthOfficeNotFoundException) {
        throw new NotFoundException(await i18n.t('workflows.no_health_office_found'))
      } else {
        throw e;
      }
    }
  }
}
