import { Schema, Prop, SchemaFactory, raw } from '@nestjs/mongoose'
import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { FORM_KEYS, WORKFLOW_TYPES } from './workflowConfiguration.constants';


export type WorkflowConfigurationDocument = WorkflowConfiguration & Document;

class WorkflowConfigurationTypeMap implements Record<FORM_KEYS, WORKFLOW_TYPES> {
  @ApiProperty({
    description: 'Configured Workflow for RegisterAsContactForm',
    examples: ['email', 'sormas'],
    enum: WORKFLOW_TYPES,
  })
  registerAsContact: WORKFLOW_TYPES
  @ApiProperty({
    description: 'Configured Workflow for TravelReturnForm',
    examples: ['email', 'sormas'],
    enum: WORKFLOW_TYPES,
  })
  travelReturn: WORKFLOW_TYPES
  @ApiProperty({
    description: 'Configured Workflow for IndexCaseForm',
    examples: ['email', 'sormas'],
    enum: WORKFLOW_TYPES,
  })
  indexCase: WORKFLOW_TYPES
}

@Schema()
export class WorkflowConfiguration {
  @ApiProperty({
    description: 'Code of the Health office',
    example: '1.08.4.25.',
  })
  @Prop({ required: true, index: true, unique: true })
  code: string;

  @ApiProperty({
    description: 'Descriptive Name for the Health office for easier recognition',
    example: 'Gesundheitsamt Alb Donau Kreis',
  })
  @Prop()
  name: string;

  @ApiProperty({
    description: 'Configured Workflows by Form',
    type: WorkflowConfigurationTypeMap,
  })
  @Prop(raw({
    [FORM_KEYS.REGISTER_AS_CONTACT]: { type: WORKFLOW_TYPES },
    [FORM_KEYS.TRAVEL_RETURN]: { type: WORKFLOW_TYPES },
    [FORM_KEYS.INDEX_CASE]: { type: WORKFLOW_TYPES },
  }))
  workflows: WorkflowConfigurationTypeMap;
}

export const WorkflowConfigurationSchema = SchemaFactory.createForClass(WorkflowConfiguration);
