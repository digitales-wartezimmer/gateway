import { Test } from '@nestjs/testing';
import { WorkflowConfigurationModule } from './workflowConfiguration.module';
import { I18nModule } from '../../../i18n.module';
import { getModelToken } from '@nestjs/mongoose';
import { WorkflowConfiguration } from './workflowConfiguration.schema';
import { HealthOffice } from '../../../healthOffice/interfaces/healthOffice';
import { HealthOfficeService } from '../../../healthOffice/healthOffice.service';
import { WorkflowConfigurationService } from './workflowConfiguration.service';
import { HealthOfficeModule } from '../../../healthOffice/healthOffice.module';
import { RegisterAsContactEmailWorkflow } from '../email/registerAsContactEmail.service';
import { TravelReturnEmailWorkflow } from '../email/travelReturnEmail.service';
import { EmailWorkflowModule } from '../email/emailWorkflow.module';
import { FORM_KEYS } from './workflowConfiguration.constants';
import {
  CacheInterceptor,
  CallHandler,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { Observable } from 'rxjs';
import { IndexCaseEmailWorkflow } from '../email/indexCaseEmail.service';
import { IndexCaseSormasWorkflow } from '../sormas/indexCaseSormas.service';
import { SormasWorkflowModule } from '../sormas/sormasWorkflow.module';
import { WorkflowProcessingException } from './workflowConfiguration.exception';
import { Connector } from '../../../connectors/connector.schema';
import { ConnectorService } from '../../../connectors/connector.service';
import { ConnectorModule } from '../../../connectors/connector.module';

const exampleHealthOfficeData: HealthOffice = {
  name: 'Landratsamt Alb-Donau-Kreis',
  department: 'Fachdienst Gesundheit',
  code: '1.08.4.25.',
  address: { street: 'Schillerstr. 30', postCode: '89077', place: 'Ulm' },
  contact: {
    phone: '0731 185-1730',
    fax: '0731 185-1738',
    mail: 'Gesundheitsamt@alb-donau-kreis.de',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}
class MockedHealthOfficeService {
  findByPostcode(): Promise<Array<HealthOffice>> {
    return new Promise(resolve => {
      resolve(this.getHealthOfficeData());
    });
  }

  getHealthOfficeData(): Array<HealthOffice> {
    return [];
  }
}

class MockedConnector {
  data: any;

  findOne() {
    return this;
  }
}

class MockedConnectorService {
  getConnector(): Promise<Connector> {
    return Promise.resolve({
      code: 'demo',
      name: 'test',
      service: 'sormas',
      url: 'https://sormas.de',
      username: 'foo',
      password: 'bar',
      userUuid: '123-abc',
    } as Connector);
  }
}

class MockedWorkflowConfigurationModel {
  data: any;

  getData() {
    return undefined;
  }

  findOne() {
    return this;
  }

  find() {
    return this;
  }

  exec() {
    return this.getData();
  }
}

class MockedWorkflow {
  execute(): any {}

  process() {}
}

class CustomLogger extends TestingLogger {
  error(): void {}
}

describe('WorkflowConfigurationService', () => {
  const workflowConfigurationModel = new MockedWorkflowConfigurationModel();
  const healthOfficeService = new MockedHealthOfficeService();
  let workflowConfigurationService: WorkflowConfigurationService;
  const registerAsContactEmailWorkflow = new MockedWorkflow();
  const travelReturnEmailWorkflow = new MockedWorkflow();
  const indexCaseEmailWorkflow = new MockedWorkflow();
  const indexCaseSormasWorkflow = new MockedWorkflow();
  const connectorService = new MockedConnectorService();
  const connectorModel = new MockedConnector();
  const logger = new CustomLogger();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        WorkflowConfigurationModule,
        I18nModule,
        HealthOfficeModule,
        EmailWorkflowModule,
        SormasWorkflowModule,
        ConnectorModule,
      ],
      providers: [WorkflowConfigurationService],
    })
      .overrideProvider(getModelToken(WorkflowConfiguration.name))
      .useValue(workflowConfigurationModel)
      .overrideProvider(HealthOfficeService)
      .useValue(healthOfficeService)
      .overrideProvider(RegisterAsContactEmailWorkflow)
      .useValue(registerAsContactEmailWorkflow)
      .overrideProvider(TravelReturnEmailWorkflow)
      .useValue(travelReturnEmailWorkflow)
      .overrideProvider(IndexCaseEmailWorkflow)
      .useValue(indexCaseEmailWorkflow)
      .overrideProvider(IndexCaseSormasWorkflow)
      .useValue(indexCaseSormasWorkflow)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(getModelToken(Connector.name))
      .useValue(connectorModel)
      .overrideProvider(ConnectorService)
      .useValue(connectorService)
      .compile();

    Logger.overrideLogger(logger);

    workflowConfigurationService = await moduleRef.resolve<
      WorkflowConfigurationService
    >(WorkflowConfigurationService);
  });

  beforeEach(() => {
    jest.spyOn(logger, 'error');
  });

  describe('WorkflowConfigurations', () => {
    describe('findAllConfigurations', () => {
      it('returns all workflow configurations', async () => {
        const data = [
          {
            name: 'Test 2',
            code: '1234',
            workflows: {
              travelReturn: 'email',
              registerAsContact: 'other',
            },
          },
          {
            name: 'Test 1',
            code: '5678',
            workflows: {
              travelReturn: 'log',
              registerAsContact: 'email',
            },
          },
        ];
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);

        expect(
          await workflowConfigurationService.findAllConfigurations(),
        ).toEqual(data);
      });
      it('returns empty array if no workflow configurations exist', async () => {
        const data = [];
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);

        expect(
          await workflowConfigurationService.findAllConfigurations(),
        ).toEqual([]);
      });
    });
    describe('getConfigurationByCode', () => {
      it('returns the workflow configuration by health office code', async () => {
        const data = {
          name: 'Test 2',
          code: '1234',
          workflows: {
            travelReturn: 'email',
            registerAsContact: 'other',
          },
        };
        jest.spyOn(workflowConfigurationModel, 'findOne');
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);

        expect(
          await workflowConfigurationService.getConfigurationByCode('1.0.4.5'),
        ).toEqual(data);
        expect(workflowConfigurationModel.findOne).toHaveBeenCalledWith({
          code: '1.0.4.5',
        });
      });
      it('returns undefined if the given health office code does not exist', async () => {
        const data = undefined;
        jest.spyOn(workflowConfigurationModel, 'findOne');
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);

        expect(
          await workflowConfigurationService.getConfigurationByCode('1.0.4.5'),
        ).toEqual(undefined);
        expect(workflowConfigurationModel.findOne).toHaveBeenCalledWith({
          code: '1.0.4.5',
        });
      });
    });
    describe('getConfigurationByPostCode', () => {
      it('returns the workflow configuration by postal code', async () => {
        const data = {
          name: 'Test 2',
          code: '1234',
          workflows: {
            travelReturn: 'email',
            registerAsContact: 'other',
          },
        };
        jest
          .spyOn(healthOfficeService, 'getHealthOfficeData')
          .mockReturnValue([exampleHealthOfficeData]);
        jest.spyOn(workflowConfigurationModel, 'findOne');
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);

        expect(
          await workflowConfigurationService.getConfigurationByPostCode(
            '89077',
          ),
        ).toEqual(data);
        expect(workflowConfigurationModel.findOne).toHaveBeenCalledWith({
          code: exampleHealthOfficeData.code,
        });
      });
      it('throws an error if no health office is found for the given postal code', async () => {
        const data = {
          name: 'Test 2',
          code: '1234',
          workflows: {
            travelReturn: 'email',
            registerAsContact: 'other',
          },
        };
        jest
          .spyOn(healthOfficeService, 'getHealthOfficeData')
          .mockReturnValue([]);
        jest.spyOn(workflowConfigurationModel, 'findOne');
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);

        await expect(
          workflowConfigurationService.getConfigurationByPostCode('89077'),
        ).rejects.toThrowError('Health Office Not Found for given post code');
      });
      it('throws an error if no workflow configuration is found', async () => {
        jest
          .spyOn(healthOfficeService, 'getHealthOfficeData')
          .mockReturnValue([exampleHealthOfficeData]);
        jest.spyOn(workflowConfigurationModel, 'findOne');
        jest
          .spyOn(workflowConfigurationModel, 'getData')
          .mockReturnValue(undefined);

        await expect(
          workflowConfigurationService.getConfigurationByPostCode('89077'),
        ).rejects.toThrowError('Workflow Configuration Not Found');
        expect(workflowConfigurationModel.findOne).toHaveBeenCalledWith({
          code: exampleHealthOfficeData.code,
        });
      });
    });
    describe('processSubmission', () => {
      const submission = {
        foo: 'bar',
      };
      beforeEach(() => {
        const data = {
          name: 'Test 2',
          code: '1234',
          workflows: {
            travelReturn: 'email',
            registerAsContact: 'email',
          },
        };
        jest
          .spyOn(registerAsContactEmailWorkflow, 'execute')
          .mockReturnValue({ message: 'success' });
        jest
          .spyOn(travelReturnEmailWorkflow, 'execute')
          .mockReturnValue({ message: 'success' });
        jest
          .spyOn(indexCaseEmailWorkflow, 'execute')
          .mockReturnValue({ message: 'success' });
        jest
          .spyOn(healthOfficeService, 'getHealthOfficeData')
          .mockReturnValue([exampleHealthOfficeData]);
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue(data);
        jest.clearAllMocks();
      });
      it('processes the service', async () => {
        expect(
          await workflowConfigurationService.processSubmission(
            FORM_KEYS.REGISTER_AS_CONTACT,
            '89077',
            submission as any,
            'de',
          ),
        ).toEqual({ message: 'success' });

        expect(registerAsContactEmailWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(travelReturnEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(logger.error).not.toHaveBeenCalled();
      });
      it('processes the service based on form key', async () => {
        expect(
          await workflowConfigurationService.processSubmission(
            FORM_KEYS.TRAVEL_RETURN,
            '89077',
            submission as any,
            'de',
          ),
        ).toEqual({ message: 'success' });

        expect(travelReturnEmailWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(registerAsContactEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(logger.error).not.toHaveBeenCalled();
      });
      it('processes the default service if no workflow configuration exists', async () => {
        jest
          .spyOn(workflowConfigurationModel, 'getData')
          .mockReturnValue(undefined);
        expect(
          await workflowConfigurationService.processSubmission(
            FORM_KEYS.TRAVEL_RETURN,
            '89077',
            submission as any,
            'de',
          ),
        ).toEqual({ message: 'success' });

        expect(travelReturnEmailWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(registerAsContactEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(logger.error).not.toHaveBeenCalled();
      });
      it('handles a not configured form', async () => {
        await expect(
          workflowConfigurationService.processSubmission(
            'otherForm' as any,
            '89077',
            submission as any,
            'de',
          ),
        ).rejects.toThrowError('Could not find form "otherForm"');

        expect(travelReturnEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(registerAsContactEmailWorkflow.execute).not.toHaveBeenCalled();
      });
      it('handles a not implemented workflow', async () => {
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue({
          name: 'Test 2',
          code: '1234',
          workflows: {
            travelReturn: 'otherWorkflow',
            registerAsContact: 'email',
          },
        });
        expect(
          await workflowConfigurationService.processSubmission(
            FORM_KEYS.TRAVEL_RETURN,
            '89077',
            submission as any,
            'de',
          ),
        ).toEqual({ message: 'success' });

        expect(travelReturnEmailWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(registerAsContactEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(logger.error).toHaveBeenCalledWith(
          'Could not find Workflow "otherWorkflow" matching the configured Workflow But it was handled with Fallback to E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
      it('handles malformed workflow configuration', async () => {
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue({
          name: 'Test 2',
          code: '1234',
          workflows: {},
        });
        expect(
          await workflowConfigurationService.processSubmission(
            FORM_KEYS.TRAVEL_RETURN,
            '89077',
            submission as any,
            'de',
          ),
        ).toEqual({ message: 'success' });

        expect(travelReturnEmailWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(registerAsContactEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(logger.error).toHaveBeenCalledWith(
          'Could not find Workflow "undefined" matching the configured Workflow But it was handled with Fallback to E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
      it('handles workflow processing exception', async () => {
        jest.spyOn(workflowConfigurationModel, 'getData').mockReturnValue({
          name: 'Test 2',
          code: '1234',
          workflows: {
            indexCase: 'sormas',
          },
        });
        jest
          .spyOn(indexCaseSormasWorkflow, 'execute')
          .mockImplementation(() => {
            throw new WorkflowProcessingException(
              { message: 'Something went wrong' },
              'IndexCaseSormas',
            );
          });
        expect(
          await workflowConfigurationService.processSubmission(
            FORM_KEYS.INDEX_CASE,
            '89077',
            submission as any,
            'de',
          ),
        ).toEqual({ message: 'success' });

        expect(indexCaseSormasWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(indexCaseEmailWorkflow.execute).toHaveBeenCalledWith(
          '89077',
          submission,
          'de',
        );
        expect(registerAsContactEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(travelReturnEmailWorkflow.execute).not.toHaveBeenCalled();
        expect(logger.error).toHaveBeenCalledWith(
          'IndexCaseSormas: Something went wrong But it was picked up by Fallback E-Mail workflow',
          '',
          'WorkflowConfigurationService',
        );
      });
    });
  });
});
