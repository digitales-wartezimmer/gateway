import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MailTemplate } from './forms/interfaces/mailTemplate';

export function setupSwaggerDocs(app) {
  const options = new DocumentBuilder()
    .setTitle('Digitales Wartezimmer')
    .setDescription(
      'API description for Digitales Wartezimmer Gateway API Services',
    )
    .setVersion('1.0')
    .setContact(
      'Digitales Wartezimmer',
      'https://digitales-wartezimmer.org',
      'info@digitales-wartezimmer.org',
    )
    .addServer(
      'https://staging.api.digitales-wartezimmer.org',
      'Staging Server for Testing Purpose and Beta Features',
    )
    .addServer(
      'https://api.digitales-wartezimmer.org',
      'Production Server for Live Data and Production Use',
    )
    .addTag('Health', 'Health Checks for the API')
    .addTag('HealthOffice', 'Get Information about Health Offices')
    .addTag('Forms', 'Send user data to the health offices')
    .addTag(
      'WorkflowConfigurations',
      'Get Information about available Workflows and the configurations',
    )
    .addTag(
      'Connectors',
      'Configure Connectors and authentication details to external services',
    )
    .addTag('Auth', 'Authenticate Clients and Users')
    .addApiKey({
      type: 'apiKey',
      description: 'API Key Authorization header',
      name: 'apiKey',
      in: 'Header',
      bearerFormat: 'Api-Key',
    })
    .addCookieAuth('connect.sid')
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    extraModels: [MailTemplate],
  });
  SwaggerModule.setup('docs', app, document);
}
