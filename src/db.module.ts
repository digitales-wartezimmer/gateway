import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('DB_URL'),
        useNewUrlParser: true,
        useUnifiedTopology: true,
        user: configService.get('DB_USER'),
        pass: configService.get('DB_PASSWORD'),
        dbName: configService.get('DB_NAME'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [],
  exports: [],
  providers: [],
})
export class DBModule {}
