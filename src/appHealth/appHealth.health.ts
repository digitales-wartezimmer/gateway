import { Injectable } from '@nestjs/common';
import { HealthCheckError, HealthIndicator, HealthIndicatorResult } from '@nestjs/terminus';
import { HealthOfficeService } from '../healthOffice/healthOffice.service';
import { PlzInfoService } from '../plzInfo/plzInfo.service';

@Injectable()
export class AppHealthIndicator extends HealthIndicator {
  constructor (private healthOfficeService: HealthOfficeService, private plzInfoService: PlzInfoService) {
    super();
  }

  async isHealthy (key: string): Promise<HealthIndicatorResult> {
    const isHealthy = true;
    const status = this.getStatus(key, isHealthy);
    if (isHealthy) {
      return status;
    }
    throw new HealthCheckError('App Health Check failed', status);
  }

  async isRKIIntegrationHealthy (key: string): Promise<HealthIndicatorResult> {
    let result
    let isHealthy = false
    try {
      result = { testResponse: await this.healthOfficeService.findByPostcode('10115') };
      isHealthy = result.testResponse.length > 0;
    } catch (e) {
      result = { error: e}
    }
    const status = this.getStatus(key, isHealthy, result);
    if (isHealthy) {
      return status;
    }
    throw new HealthCheckError('RKI Integration Check failed', status);
  }

  async isPlzInfoIntegrationHealthy (key: string): Promise<HealthIndicatorResult> {
    let result
    let isHealthy = false
    try {
      result = { testResponse: await this.plzInfoService.findByPostcode('10115') };
      isHealthy = result.testResponse.length > 0;
    } catch (e) {
      result = { error: e}
    }
    const status = this.getStatus(key, isHealthy, result);
    if (isHealthy) {
      return status;
    }
    throw new HealthCheckError('PLZ Info Integration Check failed', status);
  }
}
