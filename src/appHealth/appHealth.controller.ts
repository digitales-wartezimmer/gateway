import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
  MongooseHealthIndicator,
} from '@nestjs/terminus';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppHealthIndicator } from './appHealth.health';

@ApiTags('Health')
@Controller('health')
export class AppHealthController {
  constructor(
    private healthCheck: HealthCheckService,
    private mongooseHealth: MongooseHealthIndicator,
    private appHealthIndicator: AppHealthIndicator,
  ) {}

  @Get('live')
  @HealthCheck()
  @ApiOperation({ summary: 'Check whether App is live and functional' })
  @ApiResponse({ status: 200, description: 'App is live and functional' })
  @ApiResponse({ status: 503, description: 'App errored and is not working' })
  getLive(): Promise<HealthCheckResult> {
    return this.healthCheck.check([
      async () => this.appHealthIndicator.isHealthy('app'),
      async () => this.mongooseHealth.pingCheck('mongoDB'),
    ]);
  }

  @Get('integrations')
  @HealthCheck()
  @ApiOperation({
    summary:
      'Check whether App is correctly interfacing with external Integrations',
  })
  @ApiResponse({
    status: 200,
    description: 'App Integrations are live and functional',
  })
  @ApiResponse({
    status: 503,
    description: 'App Integrations errored and ar not working',
  })
  getIntegrations(): Promise<HealthCheckResult> {
    return this.healthCheck.check([
      async () =>
        this.appHealthIndicator.isRKIIntegrationHealthy('rkiIntegration'),
      async () =>
        this.appHealthIndicator.isPlzInfoIntegrationHealthy(
          'plzInfoIntegration',
        ),
    ]);
  }
}
