import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { AppHealthController } from './appHealth.controller';
import { AppHealthIndicator } from './appHealth.health';
import { HealthOfficeModule } from '../healthOffice/healthOffice.module';
import { PlzInfoModule } from '../plzInfo/plzInfo.module';

@Module({
  controllers: [AppHealthController],
  imports: [TerminusModule, HealthOfficeModule, PlzInfoModule],
  providers: [AppHealthIndicator],
})
export class AppHealthModule {}
