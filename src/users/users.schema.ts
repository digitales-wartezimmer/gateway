import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Role } from '../auth/roles.enum';

export type UserDocument = User & Document;

@Schema()
export class User {
  _id: string;
  @ApiProperty({
    description: 'Unique User id',
  })
  id: string;

  @ApiProperty({
    description: 'Descriptive Name for the user',
    example: 'Uptime Robot Monitor',
  })
  @Prop({ required: true, unique: true })
  name: string;

  @ApiProperty({
    description: 'Api Token used for authentication',
    example: '123abc456def789ghi',
  })
  @Prop({ required: true, index: true, unique: true })
  token: string;

  @ApiProperty({
    description: 'Role of user used for Authorization',
    type: Role,
  })
  @Prop({ required: true, type: String, enum: Object.values(Role) })
  role: Role;
  @ApiPropertyOptional({
    description: 'Hostname that is tied to the user',
    example: 'example.com',
  })
  @Prop({ type: String })
  host?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
