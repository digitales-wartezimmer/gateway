import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDocument, User } from './users.schema';

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);

  public constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  public async getUserByToken(token: string): Promise<User | undefined> {
    return await this.userModel.findOne({ token }).exec();
  }
  
  public async getUserById(id: string): Promise<User | undefined> {
    return await this.userModel.findById(id).exec();
  }
}
