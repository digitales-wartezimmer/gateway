import { Logger } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { Role } from '../auth/roles.enum';
import { UsersModule } from './users.module';
import { User } from './users.schema';
import { UsersService } from './users.service';

class MockedUserModel {
  data: any;

  getData(): User {
    return undefined;
  }

  findOne() {
    return this;
  }

  find() {
    return this;
  }

  exec() {
    return this.getData();
  }
}

class CustomLogger extends TestingLogger {
  error(): void {}
}

describe('UsersService', () => {
  const userModel = new MockedUserModel();
  let userService: UsersService;
  const logger = new CustomLogger();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [UsersModule],
      providers: [UsersService],
    })
      .overrideProvider(getModelToken(User.name))
      .useValue(userModel)
      .compile();

    Logger.overrideLogger(logger);

    userService = await moduleRef.resolve<UsersService>(UsersService);
  });

  beforeEach(() => {
    jest.spyOn(logger, 'error');
  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
  });

  describe('getUserByToken', () => {
    it('returns user', async () => {
      const data = {
        _id: '1',
        id: '1',
        name: 'Staging Client',
        token: '34930df305ec50638038150d429dabe9',
        role: Role.User,
      };
      jest.spyOn(userModel, 'findOne');
      jest.spyOn(userModel, 'getData').mockReturnValue(data);

      expect(
        await userService.getUserByToken('34930df305ec50638038150d429dabe9'),
      ).toEqual(data);
      expect(userModel.findOne).toHaveBeenCalledWith({
        token: '34930df305ec50638038150d429dabe9',
      });
    });
    it('throws error if user not found for given token', async () => {
      const data = undefined;
      jest.spyOn(userModel, 'findOne');
      jest.spyOn(userModel, 'getData').mockReturnValue(data);

      expect(await userService.getUserByToken('foo')).toEqual(undefined);
      expect(userModel.findOne).toHaveBeenCalledWith({ token: 'foo' });
    });
  });
});
