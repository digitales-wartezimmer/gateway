import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.schema';
import { AuthService } from './auth.service';
import { Role } from './roles.enum';
import { UsersModule } from '../users/users.module';
import { getModelToken } from '@nestjs/mongoose';

class MockedUsersService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getUserData());
    });
  }

  getUserData(): User | undefined {
    return undefined;
  }
}

class MockedUserModel {
  data: any;

  getData(): User {
    return undefined;
  }

  findOne() {
    return this;
  }

  find() {
    return this;
  }

  exec() {
    return this.getData();
  }
}

describe('AuthService', () => {
  const usersService = new MockedUsersService();
  const userModel = new MockedUserModel();
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UsersModule],
      providers: [AuthService],
    })
      .overrideProvider(UsersService)
      .useValue(usersService)
      .overrideProvider(getModelToken(User.name))
      .useValue(userModel)
      .compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('validateUser', () => {
    it('return user if found', async () => {
      const data = {
        _id: '1',
        id: '1',
        name: 'Staging Client',
        token: '34930df305ec50638038150d429dabe9',
        role: Role.User,
      };
      jest.spyOn(usersService, 'getUserByToken');
      jest.spyOn(usersService, 'getUserData').mockReturnValue(data);

      expect(
        await service.validateUser('34930df305ec50638038150d429dabe9'),
      ).toEqual({
        id: '1',
        name: 'Staging Client',
        role: Role.User,
      });
      expect(usersService.getUserByToken).toHaveBeenCalledWith(
        '34930df305ec50638038150d429dabe9',
      );
    });
    it('returns null if user not found', async () => {
      const data = undefined;
      jest.spyOn(usersService, 'getUserByToken');
      jest.spyOn(usersService, 'getUserData').mockReturnValue(data);

      expect(
        await service.validateUser('34930df305ec50638038150d429dabe9'),
      ).toEqual(null);
      expect(usersService.getUserByToken).toHaveBeenCalledWith(
        '34930df305ec50638038150d429dabe9',
      );
    });
    it('returns null if users token does not match', async () => {
      const data = {
        _id: '1',
        id: '1',
        name: 'Staging Client',
        token: 'otherToken',
        role: Role.User,
      };
      jest.spyOn(usersService, 'getUserByToken');
      jest.spyOn(usersService, 'getUserData').mockReturnValue(data);

      expect(
        await service.validateUser('34930df305ec50638038150d429dabe9'),
      ).toEqual(null);
      expect(usersService.getUserByToken).toHaveBeenCalledWith(
        '34930df305ec50638038150d429dabe9',
      );
    });
  });
});
