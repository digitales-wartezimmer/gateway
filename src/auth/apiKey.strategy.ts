import { HeaderAPIKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '@sentry/node';

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(HeaderAPIKeyStrategy) {
  constructor(private authService: AuthService) {
    super(
      {
        header: 'Authorization',
        prefix: 'Api-Key ',
      },
      false,
      async (token: string, done) => {
        try {
          const user = await this.validate(token);
          return done(null, user);
        } catch (e) {
          done(e);
        }
      },
    );
  }

  async validate(token: string): Promise<Omit<User, 'token'>> {
    return await this.authService.validateUser(token);
  }
}
