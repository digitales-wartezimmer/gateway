import { Injectable } from '@nestjs/common';
import { User } from 'src/users/users.schema';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async validateUser(token: string): Promise<Omit<User, 'token'>> {
    const user = await this.usersService.getUserByToken(token);
    if (user && user.token === token) {
      return {
        id: user._id,
        name: user.name,
        role: user.role,
        host: user.host,
      } as Omit<User, 'token'>;
    }
    return null;
  }
}
