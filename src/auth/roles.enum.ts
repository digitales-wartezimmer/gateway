export enum Role {
  User = 'user',
  Admin = 'admin',
  Monitor = 'monitor', // Role for monitoring/health checks
  HealthOffice = 'healthoffice', // Role for health offices to push connector data
}
