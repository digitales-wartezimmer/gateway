import {
  Body,
  Controller,
  Post,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger';
import { SentryInterceptor } from '../interceptors/sentry.interceptor';
import { AuthenticateDTO } from './interfaces/authenticateDTO';
import { LocalAuthGuard } from './local-auth.guard';

@UseInterceptors(SentryInterceptor)
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor() {}

  @Post('authenticate')
  @ApiOperation({
    summary: 'Authenticate a Client to create an authenticated session',
  })
  @ApiOkResponse({
    description: 'Text that authentication was successfully made.',
  })
  @UseGuards(LocalAuthGuard)
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async authenticateClient(@Body() body: AuthenticateDTO): Promise<string> {
    return 'Authenticated successfully';
  }
}
