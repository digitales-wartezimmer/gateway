import { ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RolesGuard } from './roles.guard';

describe('RolesGuard', () => {
  const reflector = new Reflector();
  const getUser = jest.fn().mockReturnValue(undefined);
  const getReferer = jest.fn().mockReturnValue(undefined);
  const context = {
    getHandler: () => {},
    getClass: () => {},
    switchToHttp: () => {},
  };
  class AuthService {
    validateUser() {}
    validateHost(): boolean {
      return true;
    }
  }
  const authService = new AuthService();
  const guard = new RolesGuard(reflector, authService as any);
  beforeEach(() => {
    jest.spyOn(reflector, 'getAllAndOverride').mockReturnValue(['admin']);
    jest.spyOn(context as any, 'switchToHttp').mockReturnValue({
      getRequest: () => {
        return { user: getUser(), headers: { referer: getReferer() } };
      },
    });
  });
  describe('validates whether user can activate resource', () => {
    it('if no roles are required it returns true', () => {
      jest.spyOn(reflector, 'getAllAndOverride').mockReturnValue([]);
      expect(
        guard.canActivate((context as unknown) as ExecutionContext),
      ).toBeTruthy();
      jest.spyOn(reflector, 'getAllAndOverride').mockReturnValue(undefined);
      expect(
        guard.canActivate((context as unknown) as ExecutionContext),
      ).toBeTruthy();
    });
    it('if no user could be found it throws an Unauthorized exception', () => {
      getUser.mockReturnValue(undefined);
      expect(() =>
        guard.canActivate((context as unknown) as ExecutionContext),
      ).toThrow('No user information given');
    });
    it('if user has specified role it returns true', () => {
      getUser.mockReturnValue({ role: 'admin', host: '*' });
      expect(
        guard.canActivate((context as unknown) as ExecutionContext),
      ).toBeTruthy();
    });
    it('if user does not have specified role it returns false', () => {
      getUser.mockReturnValue({ role: 'other', host: '*' });
      expect(() =>
        guard.canActivate((context as unknown) as ExecutionContext),
      ).toThrow('Forbidden');
    });
  });
});
