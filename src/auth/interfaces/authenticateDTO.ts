import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AuthenticateDTO {
  @IsString()
  @ApiProperty({
    description: 'API Token',
  })
  token: string;
  @IsString()
  @ApiProperty({
    description: 'Requesting Host/Referer',
  })
  host: string;
}
