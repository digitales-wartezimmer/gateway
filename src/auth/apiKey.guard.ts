import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { ROLES_KEY } from './roles.decorator';
import { Role } from './roles.enum';

@Injectable()
export class HeaderApiKeyAuthGuard extends AuthGuard('headerapikey') {
  constructor(private reflector: Reflector) {
    super();
  }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    // Skip Authentication strategy if authentication url or user already resolved
    if (req.url === '/auth/authenticate' || req.user) {
      return true;
    }
    const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    // Skip Authentication for not protected routes
    if (!requiredRoles || requiredRoles.length === 0) {
      return true;
    }
    return super.canActivate(context);
  }
}
