import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { User } from '../users/users.schema';
import { UsersService } from '../users/users.service';

@Injectable()
export class LocalSerializer extends PassportSerializer {
  constructor(private readonly usersService: UsersService) {
    super();
  }

  serializeUser(user: User, done: CallableFunction) {
    done(null, user.id);
  }

  async deserializeUser(userId: string, done: CallableFunction) {
    try {
      const user = await this.usersService.getUserById(userId);
      if (!user) {
        return done(null, null);
      }
      done(null, {
        id: user._id,
        name: user.name,
        role: user.role,
        host: user.host,
      } as Omit<User, 'token'>);
    } catch (err) {
      done(err);
    }
  }
}
