import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.schema';
import { LocalSerializer } from './local.serializer';
import { UsersModule } from '../users/users.module';
import { getModelToken } from '@nestjs/mongoose';
import { Role } from './roles.enum';

class MockedUsersService {
  getUserById(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getUserData());
    });
  }

  getUserData(): User | undefined {
    return undefined;
  }
}

class MockedUserModel {
  data: any;

  getData(): User {
    return undefined;
  }

  findOne() {
    return this;
  }

  find() {
    return this;
  }

  exec() {
    return this.getData();
  }
}

describe('LocalSerializer', () => {
  const usersService = new MockedUsersService();
  const userModel = new MockedUserModel();
  let service: LocalSerializer;

  const user = {
    _id: '1',
    id: '1',
    name: 'Admin',
    role: Role.Admin,
    token: '123abc',
    host: 'foo.bar',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UsersModule],
      providers: [LocalSerializer],
    })
      .overrideProvider(UsersService)
      .useValue(usersService)
      .overrideProvider(getModelToken(User.name))
      .useValue(userModel)
      .compile();

    service = module.get<LocalSerializer>(LocalSerializer);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('serializeUser', () => {
    it('serializes the user to the session', () => {
      const cb = jest.fn();
      service.serializeUser(user, cb);
      expect(cb).toHaveBeenCalledWith(null, '1');
    });
  });
  describe('deserializeUser', () => {
    it('deserializes the user from the session', async () => {
      jest.spyOn(usersService, 'getUserData').mockReturnValue(user);
      const cb = jest.fn();
      await service.deserializeUser('1', cb);
      expect(cb).toHaveBeenCalledWith(null, {
        id: user._id,
        name: user.name,
        role: user.role,
        host: user.host,
      });
    });
    it('does not deserialize if user not found', async () => {
      jest.spyOn(usersService, 'getUserData').mockReturnValue(null);
      const cb = jest.fn();
      await service.deserializeUser('1', cb);
      expect(cb).toHaveBeenCalledWith(null, null);
    });
    it('does not deserialize in case of error', async () => {
      const error = new Error('TEST');
      jest.spyOn(usersService, 'getUserData').mockImplementation(() => {
        throw error;
      });
      const cb = jest.fn();
      await service.deserializeUser('1', cb);
      expect(cb).toHaveBeenCalledWith(error);
    });
  });
});
