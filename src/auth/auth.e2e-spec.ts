import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {
  INestApplication,
  CacheInterceptor,
  CallHandler,
  NestInterceptor,
} from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import * as session from 'express-session';
import * as passport from 'passport';
import * as cookieParser from 'cookie-parser';
import * as rxjs from 'rxjs';

import { I18nModule } from '../i18n.module';
import { User } from '../users/users.schema';
import { UsersService } from '../users/users.service';
import { Role } from '../auth/roles.enum';
import { AuthModule } from '../auth/auth.module';
import { HealthOffice } from '../healthOffice/interfaces/healthOffice';
import { HealthOfficeModule } from '../healthOffice/healthOffice.module';
import { UsersModule } from '../users/users.module';
import { HealthOfficeService } from '../healthOffice/healthOffice.service';

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getUserById(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}
const exampleHealthOfficeData: HealthOffice = {
  name: 'Demo Gesundheitsamt',
  department: 'Fachdienst Gesundheit',
  code: 'demo',
  address: { street: 'Teststr. 1', postCode: '12345', place: 'Deutschland' },
  contact: {
    phone: '+49151123456789',
    fax: '49151123456789',
    mail: 'info@digitales-wartezimmer.org',
  },
  jurisdiction: [],
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
};

class MockedHealthOfficeService {
  findByPostcode(): Array<HealthOffice> {
    return this.getRKIReturnedData();
  }

  getRKIReturnedData(): Array<HealthOffice> {
    return [];
  }
}

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): rxjs.Observable<any> {
    return next.handle();
  }
}

describe('Auth', () => {
  let app: INestApplication;
  const userService = new MockedUserService();
  const healthOfficeService = new MockedHealthOfficeService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [I18nModule, AuthModule, HealthOfficeModule, UsersModule],
    })
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(HealthOfficeService)
      .useValue(healthOfficeService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .compile();

    app = moduleRef.createNestApplication();
    const sessionParams = {
      secret: '123',
      secure: false,
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        sameSite: true,
      },
    };
    app.use(session(sessionParams));

    app.use(cookieParser());

    app.use(passport.initialize());
    app.use(passport.session());
    await app.init();
  });
  const user = {
    _id: '1',
    id: '1',
    name: 'Admin',
    role: Role.Admin,
    token: '123abc',
    host: 'foo.bar',
  };

  beforeEach(() => {
    jest
      .spyOn(healthOfficeService, 'getRKIReturnedData')
      .mockReturnValue([exampleHealthOfficeData]);
    jest.spyOn(userService, 'getData').mockReturnValue(user);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('POST /auth/authenticate', () => {
    it('validates request body', async () => {
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .expect(401);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ email: 'foo', password: 'test' })
        .expect(401);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar' })
        .expect(401);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ token: '123abc' })
        .expect(401);
    });
    it('rejects invalid tokens', async () => {
      jest.spyOn(userService, 'getData').mockReturnValue(null);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar', token: '123abc' })
        .expect(401);
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ ...user, token: 'other' });
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar', token: '123abc' })
        .expect(401);
    });
    it('rejects invalid host names', async () => {
      jest.spyOn(userService, 'getData').mockReturnValue(user);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'other.de', token: '123abc' })
        .expect(401);
      jest.spyOn(userService, 'getData').mockReturnValue(user);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: '', token: '123abc' })
        .expect(401);
      jest.spyOn(userService, 'getData').mockReturnValue(user);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: '*', token: '123abc' })
        .expect(401);
    });
    it('lets wildcard hostnames pass', async () => {
      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ ...user, host: '*' });
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'other.de', token: '123abc' })
        .expect(201);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'test.de', token: '123abc' })
        .expect(201);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'a', token: '123abc' })
        .expect(201);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: '*', token: '123abc' })
        .expect(201);

      jest
        .spyOn(userService, 'getData')
        .mockReturnValue({ ...user, host: undefined });
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'other.de', token: '123abc' })
        .expect(201);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'test.de', token: '123abc' })
        .expect(201);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'a', token: '123abc' })
        .expect(201);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: '*', token: '123abc' })
        .expect(201);
    });
    it('authenticates a user', async () => {
      jest.spyOn(userService, 'getData').mockReturnValue(user);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar', token: '123abc' })
        .expect(201);
    });
    it('supplies a session cookie', async () => {
      jest.spyOn(userService, 'getData').mockReturnValue(user);
      await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar', token: '123abc' })
        .expect(201)
        .expect(res => {
          const cookie = res.header['set-cookie'];
          expect(cookie).toEqual([expect.any(String)]);
          const parts = cookie[0].split(';').map(str => str.trim());
          expect(parts[0].split('=')[0]).toEqual('connect.sid');
          expect(parts[0].split('=')[1]).toEqual(expect.any(String));
          expect(parts[1]).toEqual('Path=/');
          expect(parts[2]).toEqual('HttpOnly');
          expect(parts[3]).toEqual('SameSite=Strict');
        });
    });

    it('can authentiate with api key', async done => {
      jest.spyOn(userService, 'getData').mockReturnValue(user);

      await request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '00000' })
        .expect(401);

      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Referer', 'foo.bar')
        .set('Authorization', 'Api-Key 123abc')
        .query({ postCode: '00000' })
        .expect(200)
        .expect([
          {
            name: 'Demo Gesundheitsamt',
            department: 'Fachdienst Gesundheit',
            code: 'demo',
            address: {
              street: 'Teststr. 1',
              postCode: '12345',
              place: 'Deutschland',
            },
            contact: {
              phone: '+49151123456789',
              fax: '49151123456789',
              mail: 'info@digitales-wartezimmer.org',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
      done();
    });

    it('can authenticate with session cookie', async done => {
      jest.spyOn(userService, 'getData').mockReturnValue(user);

      await request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '00000' })
        .expect(401);

      const response = await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar', token: '123abc' })
        .expect(201);

      const cookie = response.header['set-cookie'];
      expect(cookie).toEqual([expect.any(String)]);

      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Cookie', cookie)
        .query({ postCode: '00000' })
        .expect(200)
        .expect([
          {
            name: 'Demo Gesundheitsamt',
            department: 'Fachdienst Gesundheit',
            code: 'demo',
            address: {
              street: 'Teststr. 1',
              postCode: '12345',
              place: 'Deutschland',
            },
            contact: {
              phone: '+49151123456789',
              fax: '49151123456789',
              mail: 'info@digitales-wartezimmer.org',
            },
            jurisdiction: [],
            transmissionType: 'MAIL',
            pdfServiceEnabled: false,
          },
        ]);
      done();
    });

    it('different host names and referer header are not blocked', async () => {
      jest.spyOn(userService, 'getData').mockReturnValue(user);

      await request(app.getHttpServer())
        .get('/healthOffice')
        .query({ postCode: '00000' })
        .expect(401);

      const response = await request(app.getHttpServer())
        .post('/auth/authenticate')
        .send({ host: 'foo.bar', token: '123abc' })
        .expect(201);
      const cookie = response.header['set-cookie'];
      expect(cookie).toEqual([expect.any(String)]);
      await request(app.getHttpServer())
        .get('/healthOffice')
        .set('Referer', 'other')
        .set('Cookie', cookie)
        .query({ postCode: '00000' })
        .expect(200);
    });
  });
});
