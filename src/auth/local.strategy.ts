import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../users/users.schema';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'host',
      passwordField: 'token',
    });
  }

  async validate(host: string, token: string): Promise<Omit<User, 'token'>> {
    const user = await this.authService.validateUser(token);
    if (!user) {
      return null;
    }
    // allow wildcard or unset users
    if (!user.host || user.host === '*') {
      return user;
    }
    if (user.host !== host) {
      return null;
    }
    return user;
  }
}
