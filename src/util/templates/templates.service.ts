import {
    Injectable,
    Logger,
    Scope,
} from '@nestjs/common';
import * as Sqrl from 'squirrelly';
import * as path from 'path';
import { setupHelpers } from './templates.helpers';

@Injectable({
    scope: Scope.DEFAULT,
})
export class TemplateService {
    private readonly logger = new Logger(TemplateService.name);
    private static readonly TEMPLATE_DIR = path.join(__dirname, '../../templates');

    constructor() {
        setupHelpers();
    }

    public async getTemplate(name: string, lang: string, data: object): Promise<string> {
        try {
            return await Sqrl.renderFile(path.join(TemplateService.TEMPLATE_DIR, lang) + '/' + name + '.sqrl', data);
        } catch (e) {
            this.logger.error(e.message, e.trace);
            throw new Error('Error during Template Rendering');
        }
    }
}