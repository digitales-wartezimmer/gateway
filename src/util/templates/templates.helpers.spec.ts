import { setupHelpers } from './templates.helpers';
import * as Sqrl from 'squirrelly';

jest.mock('squirrelly', () => ({
  defaultConfig: { autoTrim: true },
  helpers: { define: jest.fn() },
  renderFile: jest.fn().mockResolvedValue('Example Template'),
}));


describe('TemplateHelpers', () => {
  beforeAll(() => {
    jest.spyOn(Sqrl.helpers, 'define')
    jest.clearAllMocks();
    setupHelpers();
  })
  describe('withFallback', () => {
    let helper;
    beforeAll(() => {
      helper = (Sqrl.helpers.define as any).mock.calls[0][1]
    })
    it ('returns default string for undefined or empty value', () => {
      expect(helper({ params: [ undefined ] })).toEqual('-')
      expect(helper({ params: [ null ] })).toEqual('-')
      expect(helper({ params: [ '' ] })).toEqual('-')
      expect(helper({ params: [ ' ' ] })).toEqual('-')
      expect(helper({ params: [ '  ' ] })).toEqual('-')
    })
    it ('returns value', () => {
      expect(helper({ params: [ 'test' ] })).toEqual('test')
      expect(helper({ params: [ '  aha  ' ] })).toEqual('  aha  ')
      expect(helper({ params: [ 'my test 123' ] })).toEqual('my test 123')
    })
  })
  describe('toDate', () => {
    let helper;
    beforeAll(() => {
      helper = (Sqrl.helpers.define as any).mock.calls[1][1]
    })
    it ('returns date string', () => {
      expect(helper({ params: [ '1990-01-19T00:00:00' ] })).toEqual('19.01.1990')
      expect(helper({ params: [ '2020-12-31T23:59:59' ] })).toEqual('31.12.2020')
    })
  });
});
