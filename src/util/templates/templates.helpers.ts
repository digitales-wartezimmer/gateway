import { format } from 'date-fns';
import { de } from 'date-fns/locale';
import * as Sqrl from 'squirrelly';

export function setupHelpers() {
    Sqrl.defaultConfig.autoTrim = false;
    Sqrl.helpers.define('withFallback', function (content) {
        const str = content.params[0];
        return (!str || str.trim() === '') ? '-' : str
    });
    Sqrl.helpers.define('toDate', function (content) {
        const dateString = content.params[0];
        return format(new Date(dateString), 'dd.MM.yyyy', { locale: de });
    });
}

