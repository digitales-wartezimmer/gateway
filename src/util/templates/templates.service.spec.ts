import { Test } from '@nestjs/testing';
import { I18nModule } from '../../i18n.module';
import { Logger } from '@nestjs/common';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { TemplatesModule } from './templates.module';
import { TemplateService } from './templates.service';
import * as Sqrl from 'squirrelly';
import path = require('path');

jest.mock('squirrelly', () => ({
  defaultConfig: { autoTrim: true },
  helpers: { define: jest.fn() },
  renderFile: jest.fn().mockResolvedValue('Example Template'),
}));


class CustomLogger extends TestingLogger {
  error (): void {
  }
}

describe('TemplateService', () => {
  let templateService: TemplateService;
  const logger = new CustomLogger();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [TemplatesModule, I18nModule],
      providers: [
        TemplateService,
      ],
    })
      .compile();

    Logger.overrideLogger(logger);

    templateService = await moduleRef.resolve<TemplateService>(TemplateService);
  });

  beforeEach(() => {
    jest.spyOn(logger, 'error');
    jest.spyOn(Sqrl, 'renderFile').mockResolvedValue('Example Template')
    jest.spyOn(Sqrl.helpers, 'define')
    jest.clearAllMocks();
  });

  describe('TemplateService', () => {
    it('retrieves a template', async () => {
      const result = await templateService.getTemplate('my-template', 'de', { foo: 'bar' });
      expect(Sqrl.renderFile).toHaveBeenCalledWith(path.join(__dirname, '../../templates') + '/de/my-template.sqrl', { foo: 'bar' });
      expect(result).toEqual('Example Template');
    })
    it('retrieves a template with locale', async () => {
      const result = await templateService.getTemplate('test/my-template', 'en', { foo: 'bar' });
      expect(Sqrl.renderFile).toHaveBeenCalledWith(path.join(__dirname, '../../templates') + '/en/test/my-template.sqrl', { foo: 'bar' });
      expect(result).toEqual('Example Template');
    })
    it('handles error', async () => {
      jest.spyOn(Sqrl, 'renderFile').mockImplementationOnce(() => { throw new Error('File not found') })
      await expect(templateService.getTemplate('my-template', 'de', { foo: 'bar' })).rejects.toThrowError('Error during Template Rendering');
      expect(logger.error).toHaveBeenCalledWith('File not found', '', 'TemplateService');
    })
  });
});
