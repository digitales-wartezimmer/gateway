import { Module } from '@nestjs/common';
import { I18nModule } from 'nestjs-i18n';
import { TemplateService } from './templates.service';

@Module({
  imports: [I18nModule],
  exports: [TemplateService],
  providers: [TemplateService],
})
export class TemplatesModule {}
