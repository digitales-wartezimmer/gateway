import { Test } from '@nestjs/testing';
import { CACHE_MANAGER, CacheModule } from '@nestjs/common';
import { CacheService } from './cache.service';

class MockedCacheManager {
  get(key): any {}
  set(key, value, _options) {}
  del(key) {}
  reset() {}
}

describe('Cache Decorator', () => {
  const cacheManager = new MockedCacheManager();
  let cacheService: CacheService
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [CacheService],
      imports: [CacheModule],
    })
      .overrideProvider(CACHE_MANAGER)
      .useValue(cacheManager)
      .compile();

    cacheService = await moduleRef.resolve<CacheService>(CacheService);

    jest.clearAllMocks();
  })

  it('can retrieve a value from cache', async () => {
    jest.spyOn(cacheManager, 'get').mockReturnValue(JSON.stringify('My Cached Value'))
    expect(await cacheService.get('myKey')).toEqual('My Cached Value')
    expect(cacheManager.get).toHaveBeenCalledWith('myKey')

    jest.spyOn(cacheManager, 'get').mockReturnValue(JSON.stringify([{ foo: 'bar', test: 1 }]))
    expect(await cacheService.get('myOtherKey')).toEqual([{ foo: 'bar', test: 1 }])
    expect(cacheManager.get).toHaveBeenCalledWith('myOtherKey')
  })
  it('returns undefined for cache miss', async () => {
    jest.spyOn(cacheManager, 'get').mockReturnValue(undefined)
    expect(await cacheService.get('myKey')).toEqual(undefined)
    expect(cacheManager.get).toHaveBeenCalledWith('myKey')
  })
  it('can set a value', () => {
    jest.spyOn(cacheManager, 'set')
    cacheService.set('myKey', [{ foo: 'bar', test: 1 }], { ttl: 1234 })
    expect(cacheManager.set).toHaveBeenCalledWith('myKey', JSON.stringify([{ foo: 'bar', test: 1 }]), { ttl: 1234 })

    cacheService.set('otherKey', 'Test', {})
    expect(cacheManager.set).toHaveBeenCalledWith('otherKey', "\"Test\"", { ttl: 0 })

  })
  it('can delete a value', () => {
    jest.spyOn(cacheManager, 'del')
    cacheService.del('myKey')
    expect(cacheManager.del).toHaveBeenCalledWith('myKey')
  })
  it('can reset the cache', () => {
    jest.spyOn(cacheManager, 'reset')
    cacheService.reset()
    expect(cacheManager.reset).toHaveBeenCalled()
  })
})
