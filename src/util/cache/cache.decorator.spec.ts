import { Cache, TTL } from './cache.decorator';
import { Test } from '@nestjs/testing';
import { Injectable } from '@nestjs/common';
import { CacheService } from './cache.service';

const calculate = jest.fn().mockImplementation((a: number, b: number) => {
  return a + b;
});

@Injectable()
class TestService {
  @Cache({ key: 'myKey', ttl: TTL.DAY })
  public doSomething(a: number, b: number): number {
    return calculate(a, b);
  }
}

class MockedCacheService {
  private data: Map<string, any>

  constructor () {
    this.data = new Map<string, any>()
  }

  get(key) {
    return this.data.get(key)
  }
  set(key, value, _options) {
    return this.data.set(key, value)
  }
}

describe('Cache Decorator', () => {
  const cacheService = new MockedCacheService();
  let testService: TestService
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [CacheService, TestService],
    })
      .overrideProvider(CacheService)
      .useValue(cacheService)
      .compile();

    testService = await moduleRef.resolve<TestService>(TestService);

    jest.clearAllMocks();
  })

  it('executes and caches the function on cache miss', async () => {
    jest.spyOn(cacheService, 'get').mockReturnValue(undefined)
    jest.spyOn(cacheService, 'set')
    const result = await testService.doSomething(1, 2);
    expect(result).toEqual(3);
    expect(calculate).toHaveBeenCalled();
    expect(cacheService.set).toHaveBeenCalledWith('myKey[1,2]', 3, { ttl: 86400})
  })

  it('returns cached result of the function on cache hit', async () => {
    jest.spyOn(cacheService, 'get').mockReturnValue(3)
    jest.spyOn(cacheService, 'set')
    const result = await testService.doSomething(1, 2);
    expect(result).toEqual(3);
    expect(calculate).not.toHaveBeenCalled();
    expect(cacheService.set).not.toHaveBeenCalled()
  })
})
