import {
  CACHE_MANAGER,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class CacheService {
  constructor (@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  public async set(key: string, value: any, { ttl = 0 }: { ttl?: number }) {
    const stringifiedValue = JSON.stringify(value);

    await this.cacheManager.set(key, stringifiedValue, {
      ttl,
    });
  }

  public async get(key: string): Promise<any> {
    const data: any = await this.cacheManager.get(key);
    return data ? JSON.parse(data) : undefined;
  }

  public async del(key: string) {
    await this.cacheManager.del(key);
  }

  public async reset() {
    await this.cacheManager.reset();
  }
}
