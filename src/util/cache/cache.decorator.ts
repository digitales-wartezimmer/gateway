import { CacheService } from './cache.service';
import { Inject, Logger } from '@nestjs/common';

const logger = new Logger("Cache");

export enum TTL {
  MINUTE = 60,
  HOUR = 60 * 60,
  DAY = 60 * 60 * 24,
  WEEK = 60 * 60 * 24 * 7
}

export function Cache ({ key, ttl }: { key: string, ttl?: number }) {
  const cacheInjection = Inject(CacheService);

  return function(target: Record<string, any>, _, descriptor: PropertyDescriptor) {
    cacheInjection(target, 'cacheService');
    const method = descriptor.value;

    descriptor.value = async function(...args: Array<any>) {
      const cacheKey = `${key}[${args.map((res) => JSON.stringify(res)).join(',')}]`;

      const { cacheService } = this;
      const cacheResult = await cacheService.get(cacheKey);
      if (cacheResult) {
        return cacheResult;
      }
      const data = await method.apply(this, args);
      await cacheService.set(cacheKey, data, { ttl });
      return data;
    };
  };
}
