import { Module } from '@nestjs/common';
import { CacheModule } from './cache/cache.module';
import { TemplatesModule } from './templates/templates.module';

@Module({
  controllers: [],
  providers: [],
  exports: [],
  imports: [CacheModule, TemplatesModule],
})
export class UtilModule {}
