import {
  CacheInterceptor,
  Controller,
  Get,
  Header,
  Query,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { PlzInfoService } from './plzInfo.service';
import { PlzInfo } from './interfaces/plzInfo';
import { PostCode } from './interfaces/postCode';
import {
  ApiHeader,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { SentryInterceptor } from '../interceptors/sentry.interceptor';
import { TTL } from '../util/cache/cache.decorator';
import { Roles } from '../auth/roles.decorator';
import { Role } from '../auth/roles.enum';

@UseInterceptors(SentryInterceptor)
@UseInterceptors(CacheInterceptor)
@ApiTags('PlzInfo')
@Controller('plzInfo')
export class PlzInfoController {
  constructor(private healthOfficeService: PlzInfoService) {}

  @Get('')
  @ApiOperation({
    summary: 'Get City and Region Information based on postal code',
  })
  @ApiOkResponse({
    description: 'Array of City and Region information based on plz',
    type: [PlzInfo],
  })
  @ApiHeader({
    name: 'X-Custom-Lang',
    description:
      'Set Language of Response or errors by two letter language code',
    example: 'de',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Pass API Token for Authorization with prefix Api-Key',
    example: 'Api-Key 123abc456def',
  })
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  @Header('Cache-Control', `public, max-age=${TTL.WEEK}`)
  @Roles(Role.User, Role.Admin)
  async getPlzInfo(@Query() postCode: PostCode): Promise<Array<PlzInfo>> {
    return this.healthOfficeService.findByPostcode(postCode.postCode);
  }
}
