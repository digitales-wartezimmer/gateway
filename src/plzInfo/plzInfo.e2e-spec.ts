import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {
  CacheInterceptor,
  CallHandler,
  HttpService,
  INestApplication,
  NestInterceptor,
} from '@nestjs/common';
import * as rxjs from 'rxjs';

import { PlzInfoModule } from './plzInfo.module';
import { ExternalAPIDTO } from './interfaces/plzInfo';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { I18nModule } from '../i18n.module';
import { Observable } from 'rxjs';
import { UsersService } from '../users/users.service';
import { AuthModule } from '../auth/auth.module';
import { User } from '../users/users.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Role } from '../auth/roles.enum';

jest.mock('../util/cache/cache.decorator', () => ({
  Cache: (item: any) => {
    return (target, propertyKey, descriptor) => {
      return descriptor;
    };
  },
  TTL: {
    DAY: 1,
    WEEK: 1,
  },
}));

const exampleInfoData: ExternalAPIDTO = {
  // eslint-disable-next-line @typescript-eslint/camelcase
  geo_point_2d: [],
  geometry: {},
  name: '10115',
  // eslint-disable-next-line @typescript-eslint/camelcase
  lan_code: '11',
  // eslint-disable-next-line @typescript-eslint/camelcase
  lan_name: 'Berlin',
  // eslint-disable-next-line @typescript-eslint/camelcase
  krs_name: 'Kreisfreie Stadt Berlin',
  // eslint-disable-next-line @typescript-eslint/camelcase
  plz_code: '10115',
  // eslint-disable-next-line @typescript-eslint/camelcase
  krs_code: '11000',
  // eslint-disable-next-line @typescript-eslint/camelcase
  plz_name: 'Berlin Mitte',
  // eslint-disable-next-line @typescript-eslint/camelcase
  plz_name_long: '10115 Berlin Mitte',
};

class MockedHttpService {
  get(): rxjs.Observable<{
    data: { records: Array<{ fields: ExternalAPIDTO }> };
  }> {
    return rxjs.of({ data: { records: this.getAPIReturnedData() } });
  }

  getAPIReturnedData(): Array<{ fields: ExternalAPIDTO }> {
    return [];
  }
}

class MockedCacheInterceptor implements NestInterceptor {
  intercept(context: any, next: CallHandler): Observable<any> {
    return next.handle();
  }
}

export class CustomLogger extends TestingLogger {
  error(message: string, trace: string): any {}
}

class MockedUserService {
  getUserByToken(): Promise<User | undefined> {
    return new Promise(resolve => {
      resolve(this.getData());
    });
  }
  getData(): User | undefined {
    return undefined;
  }
}

describe('PLZ Info', () => {
  let app: INestApplication;
  const httpService = new MockedHttpService();
  const userService = new MockedUserService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [PlzInfoModule, I18nModule, AuthModule],
    })
      .overrideProvider(HttpService)
      .useValue(httpService)
      .overrideInterceptor(CacheInterceptor)
      .useValue(MockedCacheInterceptor)
      .overrideProvider(UsersService)
      .useValue(userService)
      .overrideProvider(getModelToken(User.name))
      .useValue({})
      .compile();

    app = moduleRef.createNestApplication();
    app.useLogger(new CustomLogger());
    await app.init();
  });

  beforeEach(() => {
    jest.spyOn(userService, 'getData').mockReturnValue({
      _id: '1',
      id: '1',
      name: 'Admin',
      role: Role.Admin,
      token: '1',
    });
  });

  describe('GET /plzInfo', () => {
    it('throws an unauthorized error if not authenticated', () => {
      return request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .expect(401);
    });
    it('only allows admins and users to access this resource', async () => {
      jest
        .spyOn(httpService, 'getAPIReturnedData')
        .mockReturnValue([{ fields: exampleInfoData }]);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'Admin',
        role: Role.Admin,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'User',
        role: Role.User,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'Monitor',
        role: Role.Monitor,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue({
        _id: '1',
        id: '1',
        name: 'HealthOffice',
        role: Role.HealthOffice,
        token: '1',
      });
      await request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(403);
      jest.spyOn(userService, 'getData').mockReturnValue(undefined);
      await request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(401);
    });

    it(`get plzInfo`, () => {
      jest
        .spyOn(httpService, 'getAPIReturnedData')
        .mockReturnValue([{ fields: exampleInfoData }]);
      return request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([
          {
            countyCode: '11000',
            countyName: 'Kreisfreie Stadt Berlin',
            plzCode: '10115',
            plzName: 'Berlin Mitte',
            stateCode: '11',
            stateName: 'Berlin',
          },
        ]);
    });

    it(`gets multiple results from api`, () => {
      jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([
        { fields: exampleInfoData },
        {
          fields: {
            ...exampleInfoData,
            // eslint-disable-next-line @typescript-eslint/camelcase
            plz_code: '9999',
          },
        },
      ]);
      return request(app.getHttpServer())
        .get('/plzInfo')
        .query({ postCode: '89077' })
        .set('Authorization', 'Api-Key 1')
        .expect(200)
        .expect([
          {
            countyCode: '11000',
            countyName: 'Kreisfreie Stadt Berlin',
            plzCode: '10115',
            plzName: 'Berlin Mitte',
            stateCode: '11',
            stateName: 'Berlin',
          },
          {
            countyCode: '11000',
            countyName: 'Kreisfreie Stadt Berlin',
            plzCode: '9999',
            plzName: 'Berlin Mitte',
            stateCode: '11',
            stateName: 'Berlin',
          },
        ]);
    });

    it(`gets no matching`, () => {
      jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue([]);
      return request(app.getHttpServer())
        .get('/plzInfo')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '12345' })
        .expect(200)
        .expect([]);
    });

    it(`handles invalid zip code`, () => {
      jest.spyOn(httpService, 'getAPIReturnedData').mockReturnValue(undefined);
      return request(app.getHttpServer())
        .get('/plzInfo')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: 'wrong' })
        .expect(400)
        .expect({
          statusCode: 400,
          message: ['postCode must be a postal code'],
          error: 'Bad Request',
        });
    });

    it(`handles error when retrieving data from API`, () => {
      jest.spyOn(httpService, 'getAPIReturnedData').mockImplementation(() => {
        throw new Error('Test-Error: API-Service unavailable');
      });
      return request(app.getHttpServer())
        .get('/plzInfo')
        .set('Authorization', 'Api-Key 1')
        .query({ postCode: '89077' })
        .expect(503)
        .expect({
          statusCode: 503,
          message:
            'Die externe API zum Abruf der Daten ist nicht erreichbar oder hat einen Fehler produziert',
          error: 'Service Unavailable',
        });
    });

    it(`translates the error when retrieving data from API`, () => {
      jest.spyOn(httpService, 'getAPIReturnedData').mockImplementation(() => {
        throw new Error('Test-Error: API-Service unavailable');
      });
      return request(app.getHttpServer())
        .get('/plzInfo')
        .set('Authorization', 'Api-Key 1')
        .set('x-custom-lang', 'en')
        .query({ postCode: '89077' })
        .expect(503)
        .expect({
          statusCode: 503,
          message:
            'The external API for the information retrieval is unavailable or errored',
          error: 'Service Unavailable',
        });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
