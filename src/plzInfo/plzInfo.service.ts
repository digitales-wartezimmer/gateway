import { HttpService, Injectable, Logger, ServiceUnavailableException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PlzInfo, ExternalAPIDTO } from './interfaces/plzInfo';
import * as Sentry from '@sentry/minimal';
import { I18nRequestScopeService } from 'nestjs-i18n';
import * as assert from 'assert';
import { Cache, TTL } from '../util/cache/cache.decorator';

@Injectable()
export class PlzInfoService {
  private readonly apiUrl: string;
  private readonly apiDatasetName: string;
  private readonly logger = new Logger(PlzInfoService.name);

  constructor (private configService: ConfigService, private httpService: HttpService, private readonly i18n: I18nRequestScopeService) {
    this.apiUrl = configService.get('PLZ_INFO_API_URL');
    this.apiDatasetName = configService.get('PLZ_INFO_API_DATASET_NAME');
  }

  @Cache({ key: 'plzInfo', ttl: TTL.WEEK })
  async findByPostcode (postCode: string): Promise<Array<PlzInfo>> {
    let records: Array<ExternalAPIDTO> = [];
    try {
      const response = await this.httpService.get(this.apiUrl, {
        method: 'GET',
        params: {
          q: postCode,
          dataset: this.apiDatasetName,
          facet: 'plz',
        },
        headers: {
          'User-Agent': 'Digitales Wartezimmer (https://digitales-wartezimmer.org)',
        },
      }).toPromise();
      assert(Array.isArray(response.data.records));
      records = response.data.records.map(record => record.fields);
    } catch (error) {
      Sentry.captureException({
        message: 'PLZ Info Tool Endpoint errored!',
        error,
      });
      this.logger.error('PLZ Info Tool Endpoint errored!', error);
      throw new ServiceUnavailableException(await this.i18n.t('plz_info.api_tool_error'));
    }
    return records.map(PlzInfoService.parseAPIDTOtoPlzInfo)
  }

  private static parseAPIDTOtoPlzInfo (dto: ExternalAPIDTO): PlzInfo {
    return {
      countyCode: dto.krs_code,
      countyName: dto.krs_name,
      plzCode: dto.plz_code,
      plzName: dto.plz_name,
      stateCode: dto.lan_code,
      stateName: dto.lan_name,
    };
  }
}
