import { ApiProperty } from '@nestjs/swagger';

export class PlzInfo {
  @ApiProperty({
    description: 'Code for the state',
  })
  stateCode: string
  @ApiProperty({
    description: 'Name of the state',
    examples: ['Bayern', 'Berlin', 'Sachsen'],
  })
  stateName: string
  @ApiProperty({
    description: 'Code for the county',
  })
  countyCode: string
  @ApiProperty({
    description: 'Name of the county',
    examples: ['Stadtkreis Ulm', 'Landkreis München'],
  })
  countyName: string
  @ApiProperty({
    description: 'Zip/Postal Code',
  })
  plzCode: string
  @ApiProperty({
    description: 'Name of the city or town',
    examples: ['Ismaning', 'Berlin Mitte'],
  })
  plzName: string
}

export interface ExternalAPIDTO {
  plz_name_long: string,
  lan_code: string,
  name: string,
  geometry: Record<string, any>,
  lan_name: string,
  geo_point_2d: Array<any>,
  krs_name: string,
  plz_code: string,
  krs_code: string,
  plz_name: string

}
