import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PlzInfoService } from './plzInfo.service';
import { PlzInfoController } from './plzInfo.controller';
import { CacheModule } from '../util/cache/cache.module';

@Module({
  controllers: [PlzInfoController],
  providers: [PlzInfoService],
  exports: [PlzInfoService],
  imports: [ConfigModule, HttpModule, CacheModule],
})
export class PlzInfoModule {}
