<p align="center" class="text-center">
  <a href="https://digitales-wartezimmer.org/" target="blank"><img src="https://i.imgur.com/z4zeQQO.png" width="320" alt="Digitales Wartezimmer Logo" /></a>
</p>
  
<p align="center" class="text-center">A digital solution for simple and efficient exchange of information between health authorities and citizens for rapid containment of infectious diseases.</p>
<p align="center" class="text-center">
<a href="https://gitlab.com/digitales-wartezimmer/gateway/-/pipelines" target="_blank"><img alt="Gitlab pipeline status Production" src="https://img.shields.io/gitlab/pipeline/digitales-wartezimmer/gateway/main?label=build%20production"></a>
<a href="https://gitlab.com/digitales-wartezimmer/gateway/-/pipelines" target="_blank"><img alt="Gitlab pipeline status Staging" src="https://img.shields.io/gitlab/pipeline/digitales-wartezimmer/gateway/staging?label=build%20staging"></a>
<a href="https://status.digitales-wartezimmer.org" target="_blank"><img alt="Uptime Robot status Production" src="https://img.shields.io/uptimerobot/status/m786427046-00770a7245d443df83effd1a?label=status%20production"></a>
<a href="https://status.digitales-wartezimmer.org" target="_blank"><img alt="Uptime Robot status Staging" src="https://img.shields.io/uptimerobot/status/m786427050-9d1cb37b234488bcd8a373c2?label=status%20staging"></a>
<a href="https://gitlab.com/digitales-wartezimmer/gateway/-/pipelines" target="_blank"><img alt="Gitlab code coverage" src="https://img.shields.io/gitlab/coverage/digitales-wartezimmer/gateway/main"></a>
<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank"><img src="https://img.shields.io/badge/License-GPLv3-brightgreen.svg" alt="License GPLv3"></a>
</p>

> :warning: This project is sadly not active anymore and unmaintained. For more info visit https://digitales-wartezimmer.org. Should you have questions regarding the codebase feel free to send an [E-mail](mailto:digitales-wartezimmer@bernhardwittmann.com).

# Digitales Wartezimmer - Gateway

#### :book: [API - Docs](https://api.digitales-wartezimmer.org/docs) 
#### :book: [Staging - API - Docs](https://staging.api.digitales-wartezimmer.org/docs)

With our digital solution, all relevant information of COVID-19 contact persons will be digitally captured and transferred to the systems used by the responsible health authorities to track contact persons.

By digitizing the transmission of COVID-19 contact person data, we want to help health authorities break the chain of infection quickly. At the same time, the COVID-19 contact person solution should provide a fast and user-friendly way to contact his/her health authority. 

This repository contains the Backend or Gateway of the Digitales Wartezimmer.

## Installation

You should have [node](https://nodejs.org/en/) and npm or [yarn](https://yarnpkg.com) installed.

```bash
$ npm install
```

### Database Setup

This app requires a [MongoDB](https://www.mongodb.com/) database and [Redis](https://redis.io/) storage.

Copy example env file:

```bash
$ cp .env .development.env
```

Add the connection details of both of them to your `development.env`

## Running the app

For more Information see [NestJs Docs](https://docs.nestjs.com/).

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Authentication

Interacting with the API requires an API token. This API token has to be passed with all your requests in an http header (`Authorization: Api-Key your-api-key-here`).

Create a somewhat random API token (please dont use simple phrases like test etc.)

```bash
$ openssl rand -hex 16
```

Create a user in the DB with this token and the desired role. First connect to your MongoDB instance and then run:

```bash
$ db.users.insert({ name: 'User', token: 'your-api-token-here', role: 'user' })
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Digitales Wartezimmer is a GPLv3 open source project. It is powered by our awesome [Team](https://digitales-wartezimmer.org/project). If you'd like to join us, please contact [info@digitales-wartezimmer.org](mailto:digitales-wartezimmer.org)

## Stay in touch

- Website - [https://digitales-wartezimmer.org](https://digitales-wartezimmer.org)
- Twitter - [@Dig_Wartezimmer](https://twitter.com/Dig_Wartezimmer)
- Facebook - [DigitalesWartezimmer](https://www.facebook.com/DigitalesWartezimmer/)
- LinkedIn - [digitales-wartezimmer](https://www.linkedin.com/company/digitales-wartezimmer/)

## License

  Digitales Wartezimmer is [GPLv3 licensed](LICENSE).
